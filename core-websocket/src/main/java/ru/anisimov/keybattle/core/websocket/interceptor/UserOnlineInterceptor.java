package ru.anisimov.keybattle.core.websocket.interceptor;

import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptorAdapter;

import java.security.Principal;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         10/22/14
 */
public class UserOnlineInterceptor extends ChannelInterceptorAdapter {
	/*private static final Map<String, UsersOnlineManager.PersistentConnection> INTERCEPT_URLS =
			new HashMap<String, UsersOnlineManager.PersistentConnection>() {{
		put(Destinations.BATTLE_ROOM, BATTLE_ROOM);
		put(Destinations.BATTLE_ROOM_CHAT, BATTLE_ROOM);
		put(Destinations.BATTLE_ROOM_USERS, BATTLE_ROOM);
	}};*/

	private static final Map<String, String> sessionUser = new ConcurrentHashMap<>();

	//private UsersOnlineManager usersOnlineManager;

	@Override
	public void postSend(Message<?> message, MessageChannel channel, boolean sent) {
		if (!sent) {
			return;
		}

		StompHeaderAccessor accessor = StompHeaderAccessor.wrap(message);
		Principal user = accessor.getUser();
		StompCommand command = accessor.getCommand();
		String sessionId = accessor.getSessionId();
		String url = accessor.getDestination();

		switch(command.getMessageType()) {
			case DISCONNECT:
				if (sessionUser.containsKey(sessionId)) {
					String userName = sessionUser.get(sessionId);
					//usersOnlineManager.removeUserSession(userName, sessionId);
					sessionUser.remove(sessionId);
				}
				break;
			case CONNECT:
			case SUBSCRIBE:
			case MESSAGE:
			case HEARTBEAT:
				/*if (user != null && !sessionUser.containsKey(sessionId) && INTERCEPT_URLS.containsKey(url)) {
					sessionUser.put(sessionId, user.getName());
					usersOnlineManager.addUserSession((User)user, sessionId, INTERCEPT_URLS.get(url));
				}*/
				break;
		}
	}

	/*public void setUsersOnlineManager(UsersOnlineManager usersOnlineManager) {
		this.usersOnlineManager = usersOnlineManager;
	}*/
}
