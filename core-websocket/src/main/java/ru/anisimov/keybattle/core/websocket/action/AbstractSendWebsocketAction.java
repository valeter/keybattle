package ru.anisimov.keybattle.core.websocket.action;

import org.springframework.messaging.simp.SimpMessagingTemplate;

import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         10/28/14
 */
public abstract class AbstractSendWebsocketAction implements Consumer<Void> {
	protected SimpMessagingTemplate simpMessagingTemplate;
	protected String destination;
	protected Supplier messageBuilder;

	public AbstractSendWebsocketAction(SimpMessagingTemplate simpMessagingTemplate, String destination, Supplier messageBuilder) {
		this.simpMessagingTemplate = simpMessagingTemplate;
		this.destination = destination;
		this.messageBuilder = messageBuilder;
	}
}

