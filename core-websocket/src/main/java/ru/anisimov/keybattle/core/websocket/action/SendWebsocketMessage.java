package ru.anisimov.keybattle.core.websocket.action;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import java.util.function.Supplier;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         10/22/14
 */
public class SendWebsocketMessage extends AbstractSendWebsocketAction {
	private static final Logger log = LogManager.getLogger(SendWebsocketMessage.class);

	public SendWebsocketMessage(SimpMessagingTemplate simpMessagingTemplate, String destination, Supplier messageBuilder) {
		super(simpMessagingTemplate, destination, messageBuilder);
	}

	@Override
	public void accept(Void Void) {
		log.debug("Sending message to " + destination);
		simpMessagingTemplate.convertAndSend(destination, messageBuilder.get());
	}
}
