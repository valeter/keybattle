package ru.anisimov.keybattle.core.websocket.action;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import java.util.function.Supplier;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         10/28/14
 */
public class SendToUserWebsocketMessage extends AbstractSendWebsocketAction {
	private static final Logger log = LogManager.getLogger(SendToUserWebsocketMessage.class);

	private String userName;

	public SendToUserWebsocketMessage(SimpMessagingTemplate simpMessagingTemplate, String destination, String userName, Supplier messageBuilder) {
		super(simpMessagingTemplate, destination, messageBuilder);
		this.userName = userName;
	}

	@Override
	public void accept(Void Void) {
		log.debug("Sending message to " + destination + " to user " + userName);
		simpMessagingTemplate.convertAndSendToUser(userName, destination, messageBuilder.get());
	}
}
