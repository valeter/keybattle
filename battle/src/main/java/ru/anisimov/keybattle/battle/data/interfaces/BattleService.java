package ru.anisimov.keybattle.battle.data.interfaces;

import ru.anisimov.keybattle.core.model.battle.BattleInfo;
import ru.anisimov.keybattle.core.model.battle.BattleSource;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         19.09.15
 */
public interface BattleService {
	BattleInfo createBattle(BattleSource battleSource);
	
	boolean storeBattleInfo(BattleInfo info);
}
