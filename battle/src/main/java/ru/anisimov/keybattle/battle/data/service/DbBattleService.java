package ru.anisimov.keybattle.battle.data.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import ru.anisimov.keybattle.battle.data.interfaces.BattleService;
import ru.anisimov.keybattle.core.model.battle.BattleInfo;
import ru.anisimov.keybattle.core.model.battle.BattleSource;
import ru.anisimov.keybattle.core.model.user.User;

import java.util.UUID;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         19.09.15
 */
@Service
public class DbBattleService implements BattleService {
	@Autowired
	private JdbcTemplate battleJdbcTemplate;

	@Override
	public BattleInfo createBattle(BattleSource battleSource) {
		return new BattleInfo(UUID.randomUUID(), text, users);
	}

	@Override
	public boolean storeBattleInfo(BattleInfo info) {
		return false;
	}
}
