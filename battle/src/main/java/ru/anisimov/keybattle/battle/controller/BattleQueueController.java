package ru.anisimov.keybattle.battle.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.anisimov.keybattle.battle.manager.BattleManager;
import ru.anisimov.keybattle.battle.manager.BattleRepository;
import ru.anisimov.keybattle.core.manager.UserManager;
import ru.anisimov.keybattle.core.model.battle.BattleInfo;
import ru.anisimov.keybattle.core.model.user.User;
import ru.anisimov.keybattle.core.model.websocket.WebsocketEvent;
import ru.anisimov.keybattle.core.model.websocket.WebsocketEventType;

import java.util.UUID;

import static ru.anisimov.keybattle.core.config.constants.Destinations.*;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         03.10.15
 */
@Controller
public class BattleQueueController {
	@Autowired
	private UserManager userManager;
	@Autowired
	private BattleManager battleManager;
	@Autowired
	private BattleRepository battleRepository;
	
	@RequestMapping(name = BATTLE_JOIN, method = RequestMethod.POST)
	@ResponseBody
	public String joinBattle(@RequestBody String userId) {
		User user = userManager.getUserById(Long.parseLong(userId));
		return battleManager.userJoinBattle(user).toString();
	}
	
	@MessageMapping(BATTLE_QUEUE_SYSTEM_STATE)
	@SendToUser(value = BATTLE_QUEUE_SYSTEM, broadcast = false)
	public WebsocketEvent systemState(String battleId) {
		return new WebsocketEvent(null, null);
	}

	@RequestMapping(value = BATTLE_QUEUE_USER_LIST, method = RequestMethod.GET)
	@SendToUser(value = BATTLE_QUEUE_USERS, broadcast = false)
	public WebsocketEvent userList(String battleId) {
		BattleInfo info = battleManager.getBattle(UUID.fromString(battleId));
		return new WebsocketEvent(WebsocketEventType.USER_LIST, info.users());
	}
}
