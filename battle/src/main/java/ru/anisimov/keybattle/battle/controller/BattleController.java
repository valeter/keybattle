package ru.anisimov.keybattle.battle.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.anisimov.keybattle.battle.manager.BattleRepository;
import ru.anisimov.keybattle.core.config.constants.ModelAttributes;
import ru.anisimov.keybattle.core.exception.ApplicationError;
import ru.anisimov.keybattle.core.exception.ApplicationErrorType;
import ru.anisimov.keybattle.core.model.battle.BattleInfo;
import ru.anisimov.keybattle.core.model.user.User;
import ru.anisimov.keybattle.core.websocket.action.SendToUserWebsocketMessage;
import ru.anisimov.keybattle.core.websocket.action.SendWebsocketMessage;

import java.util.List;
import java.util.UUID;

import static ru.anisimov.keybattle.core.config.constants.Destinations.*;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         26.09.15
 */
@Controller
public class BattleController {
	@Autowired
	private SimpMessagingTemplate messagingTemplate;
	@Autowired
	private BattleRepository battleRepository;

	@RequestMapping(value = BATTLE_ID, method = RequestMethod.GET)
	public String showBattle(@PathVariable("id") String battleId, User user, Model model) {
		model.addAttribute(ModelAttributes.BATTLE_ID, battleId);
		model.addAttribute(ModelAttributes.BATTLE, getBattle(battleId));
		return BATTLE;
	}

	@MessageMapping(BATTLE_ID_STATE)
	@SendTo(BATTLE_ID)
	public BattleInfo getState(@DestinationVariable("id") String id) throws Exception {
		return getBattle(id);
	}

	@MessageMapping(BATTLE_ID_WORDS_SEND)
	public void addWord(@DestinationVariable("id") String id, String word, User user) throws Exception {
		getBattle(id).addWord(user.getId(), word);
	}

	@MessageExceptionHandler
	@SendToUser(value= BATTLE_ID_ERRORS, broadcast=false)
	public ApplicationError handleException(@DestinationVariable("id") String id, Exception exception) {
		return new ApplicationError("", ApplicationErrorType.ALERT);
	}

	private void sendMessage(Object event, String destination, List<String> userNames) {
		if (userNames == null || userNames.size() == 0) {
			new SendWebsocketMessage(messagingTemplate, destination, () -> event).make();
		} else {
			for (String userName : userNames) {
				new SendToUserWebsocketMessage(messagingTemplate, destination, userName, () -> event).make();
			}
		}
	}

	private BattleInfo getBattle(String id) {
		return battleRepository.get(UUID.fromString(id));
	}
}
