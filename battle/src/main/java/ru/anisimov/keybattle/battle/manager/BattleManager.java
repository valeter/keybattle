package ru.anisimov.keybattle.battle.manager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.anisimov.keybattle.battle.data.interfaces.BattleService;
import ru.anisimov.keybattle.core.model.battle.BattleInfo;
import ru.anisimov.keybattle.core.model.battle.BattleSource;
import ru.anisimov.keybattle.core.model.locale.DictionaryLocale;
import ru.anisimov.keybattle.core.model.user.FakeUsers;
import ru.anisimov.keybattle.core.model.user.User;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         19.09.15
 */
@Component("battleManager")
public class BattleManager {
	@Autowired
	private BattleService battleService;
	@Autowired
	private BattleRepository battleRepository;
	@Value("battle.default.size")
	private int battleDefaultSize;

	private LinkedHashMap<UUID, BattleInfo> battles = new LinkedHashMap<>();
	
	
	public synchronized BattleInfo getBattle(UUID id) {
		return battles.get(id);
	}

	private UUID createNewBattle(BattleSource source) {
		int size = source.getSize() == null ? battleDefaultSize : source.getSize();
		User[] users = new User[size];
		users[0] = source.getUser();
		
		BattleInfo battle = battleService.startBattle(user, text, users);
		battles.put(battle.getId(), battle);

		return battle.getId();
	}
	
	public synchronized UUID userJoinBattle(User user) {
		return userJoinBattle(user, DictionaryLocale.values());
	}
	
	public synchronized UUID userJoinBattle(User user, DictionaryLocale... locales) {
		ThreadLocalRandom random = ThreadLocalRandom.current();
		DictionaryLocale locale = locales[random.nextInt(locales.length)];
		
		BattleInfo info = battleQueues.get(locale).peek();
		if (info == null) {
			return createNewBattle(user, locale);
		}
		if (info.botCount() > 0) {
			info.changeUser(info.botIds().get(0), user);
		}
		return info.getId();
	}
	
	public synchronized void userLeaveBattle(User user, UUID id) {
		BattleInfo info = battles.get(id);
		if (info == null) {
			return;
		}
		
		List<Long> botIds = info.botIds();
		info.changeUser(user.getId(), new User(FakeUsers.BATTLE_BOTS.stream()
				.filter(botId -> !botIds.contains(botId))
				.findFirst().get()));
		
		if (info.onlyBots()) {
			battles.remove(id);
			battleQueues.get(info.getText().getLocale()).remove(info);
		}
	}
}
