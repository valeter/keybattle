package ru.anisimov.keybattle.battle.manager;

import org.springframework.stereotype.Service;
import ru.anisimov.keybattle.core.model.battle.BattleInfo;

import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         20.09.15
 */
@Service
public class BattleRepository {
	private ConcurrentHashMap<UUID, BattleInfo> battles = new ConcurrentHashMap<>();

	public int size() {
		return battles.size();
	}

	public BattleInfo get(UUID key) {
		return battles.get(key);
	}

	public boolean containsKey(UUID key) {
		return battles.containsKey(key);
	}

	public void put(BattleInfo value) {
		battles.put(value.getId(), value);
	}

	public BattleInfo remove(UUID key) {
		return battles.remove(key);
	}
}
