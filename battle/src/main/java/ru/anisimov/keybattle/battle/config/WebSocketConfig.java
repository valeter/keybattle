package ru.anisimov.keybattle.battle.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.messaging.simp.config.ChannelRegistration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import ru.anisimov.keybattle.core.config.CoreConfig;
import ru.anisimov.keybattle.core.config.ManagerConfig;
import ru.anisimov.keybattle.core.config.PersistenceConfig;

import static ru.anisimov.keybattle.core.config.constants.Destinations.BATTLE_QUEUE;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         10/20/14
 */
@Configuration
@EnableWebSocketMessageBroker
@Import({PersistenceConfig.class, CoreConfig.class, ManagerConfig.class, PropertyPlaceholderConfig.class})
public class WebSocketConfig extends AbstractWebSocketMessageBrokerConfigurer {
	private static final String DESTINATION_PREFIX = "/app";
	
	@Override
	public void configureMessageBroker(MessageBrokerRegistry config) {
		config.enableSimpleBroker(
				BATTLE_QUEUE + "/"
		);
		config.setApplicationDestinationPrefixes(DESTINATION_PREFIX);
	}

	@Override
	public void registerStompEndpoints(StompEndpointRegistry registry) {
		registry.addEndpoint(BATTLE_QUEUE).withSockJS();
	}

	@Override
	public void configureClientInboundChannel(ChannelRegistration registration) {
	}
}
