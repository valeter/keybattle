package ru.anisimov.keybattle.tms.data;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.anisimov.keybattle.core.config.PersistenceConfig;
import ru.anisimov.keybattle.tms.data.dao.SchedulerInfoDAO;
import ru.anisimov.keybattle.tms.data.model.SchedulerInfo;

@Transactional
@TransactionConfiguration(defaultRollback = true)
@ContextConfiguration(classes = PersistenceConfig.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class SchedulerInfoDAOTest {
	@Autowired
	private SchedulerInfoDAO schedulerInfoDAO;
	
	@Test
	public void testGetLastSchedulers() throws Exception {
		Page<SchedulerInfo> infos = schedulerInfoDAO.getLastSchedulers(new PageRequest(0, 10));
		System.out.println(infos.getNumberOfElements());
	}
}
