package ru.anisimov.keybattle.tms.data;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.anisimov.keybattle.core.config.PersistenceConfig;
import ru.anisimov.keybattle.tms.data.dao.JobInfoDAO;
import ru.anisimov.keybattle.tms.data.dao.SchedulerInfoDAO;
import ru.anisimov.keybattle.tms.data.model.JobInfo;
import ru.anisimov.keybattle.tms.data.model.SchedulerInfo;

import java.util.List;

@Transactional
@TransactionConfiguration(defaultRollback = true)
@ContextConfiguration(classes = PersistenceConfig.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class JobInfoDAOTest {
	@Autowired
	private SchedulerInfoDAO schedulerInfoDAO;
	@Autowired
	private JobInfoDAO jobInfoDAO;

	@Test
	public void testGetForScheduler() throws Exception {
		List<JobInfo> jobInfosList = jobInfoDAO.getForScheduler("QuartzScheduler-1424199766256", null, null);
		
		Page<SchedulerInfo> schedulerInfos = schedulerInfoDAO.getLastSchedulers(new PageRequest(0, 10));
		for (SchedulerInfo schedulerInfo : schedulerInfos) {
			List<JobInfo> jobInfos = jobInfoDAO.getForScheduler(schedulerInfo.getSchedulerName(), 10, 0);
			System.out.println(jobInfos.size());
			jobInfos = jobInfoDAO.getForScheduler(schedulerInfo.getSchedulerName(), null, null);
			System.out.println(jobInfos.size());
		}
	}
}
