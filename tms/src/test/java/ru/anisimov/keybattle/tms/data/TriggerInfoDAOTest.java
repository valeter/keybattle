package ru.anisimov.keybattle.tms.data;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.anisimov.keybattle.core.config.PersistenceConfig;
import ru.anisimov.keybattle.tms.data.dao.JobInfoDAO;
import ru.anisimov.keybattle.tms.data.dao.SchedulerInfoDAO;
import ru.anisimov.keybattle.tms.data.model.JobInfo;
import ru.anisimov.keybattle.tms.data.model.SchedulerInfo;
import ru.anisimov.keybattle.tms.data.dao.TriggerInfoDAO;
import ru.anisimov.keybattle.tms.data.model.TriggerInfo;

import java.util.List;

@Transactional
@TransactionConfiguration(defaultRollback = true)
@ContextConfiguration(classes = PersistenceConfig.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class TriggerInfoDAOTest {
	@Autowired
	private SchedulerInfoDAO schedulerInfoDAO;
	@Autowired
	private JobInfoDAO jobInfoDAO;
	@Autowired
	private TriggerInfoDAO triggerInfoDAO;

	@Test
	public void testGetTriggerInfos() throws Exception {
		Page<SchedulerInfo> schedulerInfos = schedulerInfoDAO.getLastSchedulers(new PageRequest(0, 10));
		for (SchedulerInfo schedulerInfo : schedulerInfos) {
			List<JobInfo> jobInfos = jobInfoDAO.getForScheduler(schedulerInfo.getSchedulerName(), 1, 0);
			for (JobInfo jobInfo : jobInfos) {
				List<TriggerInfo> infos = triggerInfoDAO.getTriggerInfos(schedulerInfo.getSchedulerName(), jobInfo.getFullName());
				System.out.println(infos.size());
			}
		}
	}
}
