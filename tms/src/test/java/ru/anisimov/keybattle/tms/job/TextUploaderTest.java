package ru.anisimov.keybattle.tms.job;

import org.junit.Test;
import ru.anisimov.keybattle.core.Pair;
import ru.anisimov.keybattle.core.model.locale.DictionaryLocale;
import ru.anisimov.keybattle.core.model.text.TextDetails;

import java.io.File;
import java.util.*;

import static org.junit.Assert.*;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         28.10.15
 */
public class TextUploaderTest {
	@Test
	public void testGetTexts() throws Exception {
		TextUploader textUploader = new TextUploader();
		Pair<TextDetails, List<String>> result = textUploader.getTexts(
				this.getClass().getClassLoader().getResourceAsStream("ru.fb2" ), DictionaryLocale.RU);
		
		assertNotNull(result);
		assertNotNull(result.getFirst());
		assertNotNull(result.getSecond());
		assertEquals("Аркадий", result.getFirst().getAuthorFirstName());
		assertEquals("Аверченко", result.getFirst().getAuthorLastName());
		assertEquals("Дюжина ножей в спину революции", result.getFirst().getBookTitle());
		assertFalse(result.getSecond().size() == 0);
		
		result = textUploader.getTexts(
				this.getClass().getClassLoader().getResourceAsStream("en.fb2"), DictionaryLocale.EN);
		assertNotNull(result);
		assertNotNull(result.getFirst());
		assertNotNull(result.getSecond());
		assertEquals("Gilbert", result.getFirst().getAuthorFirstName());
		assertEquals("Chesterton", result.getFirst().getAuthorLastName());
		assertEquals("Manalive", result.getFirst().getBookTitle());
		assertFalse(result.getSecond().size() == 0);
	}
	
	@Test
	public void testFindTexts() throws Exception {
		TextUploader textUploader = new TextUploader();
		textUploader.findTexts(new File("/Users/valter/tmp/fb2_ru"), DictionaryLocale.RU);
	}
	
	@Test
	public void testCleanupTexts() throws Exception {
		TextUploader textUploader = new TextUploader();
		Random rnd = new Random();
		int testCount = 10;
		for (int k = 0; k < testCount; k++) {
			Map<TextDetails, List<String>> texts = new HashMap<>();
			int textCount = TextUploader.MAX_TEXTS_BY_AUTHOR * 3 + rnd.nextInt(100);
			for (int i = 0; i < textCount; i++) {
				String s = String.valueOf(i);
				List<String> strings = new ArrayList<>();
				int stringsCount = rnd.nextInt(TextUploader.MAX_TEXTS_BY_AUTHOR * 3  + rnd.nextInt(100));
				for (int j = 0; j < stringsCount; j++) {
					strings.add(String.valueOf(j));
				}
				texts.put(new TextDetails(s, s, s), strings);
			}
			assertEquals(TextUploader.MAX_TEXTS_BY_AUTHOR, textUploader.cleanupTexts(texts));
			int realCount = 0;
			for (List<String> strings : texts.values()) {
				realCount += strings.size();
			}
			assertEquals(TextUploader.MAX_TEXTS_BY_AUTHOR, realCount);
		}
	}
}
