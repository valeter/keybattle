package ru.anisimov.keybattle.tms.listener;

import org.quartz.JobKey;
import org.quartz.JobListener;
import org.quartz.Matcher;
import org.quartz.impl.matchers.EverythingMatcher;

import java.util.Collections;
import java.util.List;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         12.02.15
 */
public interface SpringJobListener extends JobListener {
	default String getName() {
		return this.getClass().getSimpleName(); 
	}
	
	default String[] jobsToListen() {
		return new String[0];
	}
	
	default List<Matcher<JobKey>> customMatchers() {
        return Collections.singletonList(
                EverythingMatcher.allJobs()
        );
	}
}
