package ru.anisimov.keybattle.tms.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.support.TransactionTemplate;
import ru.anisimov.keybattle.tms.data.dao.JobInfoDAO;
import ru.anisimov.keybattle.tms.data.dao.SchedulerInfoDAO;
import ru.anisimov.keybattle.tms.data.dao.TriggerInfoDAO;
import ru.anisimov.keybattle.tms.data.model.JobInfo;
import ru.anisimov.keybattle.tms.data.model.SchedulerInfo;
import ru.anisimov.keybattle.tms.data.model.TriggerInfo;

import java.util.List;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         17.02.15
 */
@Service
public class QuartzInfoManager {
	@Autowired
	private SchedulerInfoDAO schedulerInfoDAO;
	@Autowired
	private JobInfoDAO jobInfoDAO;
	@Autowired
	private TriggerInfoDAO triggerInfoDAO;
	@Autowired
	private TransactionTemplate transactionTemplate;

	public SchedulerInfo getCurrentScheduler() {
		Page<SchedulerInfo> result = getSchedulers(new PageRequest(0, 1, new Sort(Sort.Direction.DESC, "SCHED_NAME")));
		return result.getNumberOfElements() == 0 ? null : result.getContent().get(0);
	}
	
	public Page<SchedulerInfo> getSchedulers(Pageable pageable) {
		Page<SchedulerInfo> result = schedulerInfoDAO.getLastSchedulers(pageable);
		for (SchedulerInfo schedulerInfo : result) {
			List<JobInfo> jobInfos = jobInfoDAO.getForScheduler(schedulerInfo.getSchedulerName(), null, null);
			for (JobInfo jobInfo : jobInfos) {
				List<TriggerInfo> triggerInfos = triggerInfoDAO.getTriggerInfos(
						schedulerInfo.getSchedulerName(),
						jobInfo.getFullName());
				jobInfo.setTriggerInfos(triggerInfos);
			}
			schedulerInfo.setJobInfos(jobInfos);
		}
		return result;
	}

	public int getSchedulersCount() {
		return schedulerInfoDAO.getSchedulersCount();
	}
}
