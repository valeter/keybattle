package ru.anisimov.keybattle.tms.data.model;


import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         17.02.15
 */
public abstract class TriggerInfo {
	private String triggerName;
	private String triggerGroup;

	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
	private Date nextFireTime;

	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
	private Date prevFireTime;
	private String triggerState;
	private String triggerType;

	public TriggerInfo() {
	}

	public TriggerInfo(String triggerName, String triggerGroup, Date nextFireTime, Date prevFireTime, String triggerState, String triggerType) {
		this.triggerName = triggerName;
		this.triggerGroup = triggerGroup;
		this.nextFireTime = nextFireTime;
		this.prevFireTime = prevFireTime;
		this.triggerState = triggerState;
		this.triggerType = triggerType;
	}

	public String getTriggerName() {
		return triggerName;
	}

	public void setTriggerName(String triggerName) {
		this.triggerName = triggerName;
	}

	public String getTriggerGroup() {
		return triggerGroup;
	}

	public void setTriggerGroup(String triggerGroup) {
		this.triggerGroup = triggerGroup;
	}

	public Date getNextFireTime() {
		return nextFireTime;
	}

	public void setNextFireTime(Date nextFireTime) {
		this.nextFireTime = nextFireTime;
	}

	public Date getPrevFireTime() {
		return prevFireTime;
	}

	public void setPrevFireTime(Date prevFireTime) {
		this.prevFireTime = prevFireTime;
	}

	public String getTriggerState() {
		return triggerState;
	}

	public void setTriggerState(String triggerState) {
		this.triggerState = triggerState;
	}

	public String getTriggerType() {
		return triggerType;
	}

	public void setTriggerType(String triggerType) {
		this.triggerType = triggerType;
	}
}
