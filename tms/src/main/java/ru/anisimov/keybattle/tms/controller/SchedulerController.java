package ru.anisimov.keybattle.tms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.anisimov.keybattle.core.model.web.WebActionResult;
import ru.anisimov.keybattle.tms.data.QuartzInfoManager;
import ru.anisimov.keybattle.tms.data.model.SchedulerInfo;
import ru.anisimov.keybattle.tms.scheduler.SchedulerManager;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         06.05.15
 */
@RestController
public class SchedulerController {
	@Autowired
	private QuartzInfoManager quartzInfoManager;
	
	@Autowired
	private SchedulerManager schedulerManager;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public Page<SchedulerInfo> schedulerInfo(
			@PageableDefault(page = 0, size = 1, sort = "SCHED_NAME", direction = Sort.Direction.DESC) Pageable pageable) {
		return quartzInfoManager.getSchedulers(pageable);
	}
	
	@RequestMapping(value = "/{jobName}/run", method = RequestMethod.POST)
	public WebActionResult runJob(@PathVariable("jobName") String jobName) {
		return schedulerManager.runJob(jobName) ? WebActionResult.SUCCESS : WebActionResult.FAIL;
	}

	@RequestMapping(value = "/{jobName}/pause", method = RequestMethod.POST)
	public WebActionResult pauseJob(@PathVariable("jobName") String jobName) {
		return schedulerManager.pauseJob(jobName) ? WebActionResult.SUCCESS : WebActionResult.FAIL;
	}

	@RequestMapping(value = "/{jobName}/resume", method = RequestMethod.POST)
	public WebActionResult resumeJob(@PathVariable("jobName") String jobName) {
		return schedulerManager.resumeJob(jobName) ? WebActionResult.SUCCESS : WebActionResult.FAIL;
	}

	@RequestMapping(value = "/{jobName}", method = RequestMethod.DELETE)
	public WebActionResult deleteJob(@PathVariable("jobName") String jobName) {
		return schedulerManager.deleteJob(jobName) ? WebActionResult.SUCCESS : WebActionResult.FAIL;
	}
}
