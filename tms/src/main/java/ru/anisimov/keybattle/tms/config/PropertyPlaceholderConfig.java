package ru.anisimov.keybattle.tms.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import ru.anisimov.keybattle.core.config.CorePropertyPlaceholderConfig;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         16.02.15
 */
@Configuration
@PropertySource({
		"classpath:jdbc.properties",
		"classpath:application.properties"
})
public class PropertyPlaceholderConfig {
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}
}
