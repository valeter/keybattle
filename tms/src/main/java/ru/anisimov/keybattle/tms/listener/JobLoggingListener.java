package ru.anisimov.keybattle.tms.listener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         12.02.15
 */
public class JobLoggingListener implements SpringJobListener {
	private static final Logger log = LogManager.getLogger(JobLoggingListener.class);

	@Override
	public void jobToBeExecuted(JobExecutionContext context) {
		try {
			log.debug(context.getJobDetail().getKey() + " starting " 
					+ context.getScheduler().getMetaData().getSchedulerName());
		} catch (SchedulerException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void jobExecutionVetoed(JobExecutionContext context) {
		try {
			log.debug(context.getJobDetail().getKey() + " vetoed " 
					+ context.getScheduler().getMetaData().getSchedulerName());
		} catch (SchedulerException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void jobWasExecuted(JobExecutionContext context, JobExecutionException jobException) {
		try {
			String message = context.getJobDetail().getKey() + " finished " 
					+ context.getScheduler().getMetaData().getSchedulerName();
			if (jobException != null) {
				log.error(message, jobException);
			} else {
				log.debug(message);
			}
		} catch (SchedulerException e) {
			throw new RuntimeException(e);
		}
	}
}
