package ru.anisimov.keybattle.tms.data.model;

import java.util.Date;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         07.05.15
 */
public class SimpleTriggerInfo extends TriggerInfo {
	private int repeatCount;
	private int repeatInterval;
	private int timesTriggered;

	public SimpleTriggerInfo() {
	}

	public SimpleTriggerInfo(String triggerName, String triggerGroup, Date nextFireTime, Date prevFireTime, String triggerState, String triggerType, int repeatCount, int repeatInterval, int timesTriggered) {
		super(triggerName, triggerGroup, nextFireTime, prevFireTime, triggerState, triggerType);
		this.repeatCount = repeatCount;
		this.repeatInterval = repeatInterval;
		this.timesTriggered = timesTriggered;
	}

	public int getRepeatCount() {
		return repeatCount;
	}

	public void setRepeatCount(int repeatCount) {
		this.repeatCount = repeatCount;
	}

	public int getRepeatInterval() {
		return repeatInterval;
	}

	public void setRepeatInterval(int repeatInterval) {
		this.repeatInterval = repeatInterval;
	}

	public int getTimesTriggered() {
		return timesTriggered;
	}

	public void setTimesTriggered(int timesTriggered) {
		this.timesTriggered = timesTriggered;
	}
}
