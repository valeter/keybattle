package ru.anisimov.keybattle.tms.data.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;
import ru.anisimov.keybattle.tms.data.model.CronTriggerInfo;
import ru.anisimov.keybattle.tms.data.model.SimpleTriggerInfo;
import ru.anisimov.keybattle.tms.data.model.TriggerInfo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         17.02.15
 */
@Service
public class TriggerInfoDAO {
	private RowMapper<TriggerInfo> ROW_MAPPER = (rs, rowNum) -> {
		TriggerInfo result = null;
		String type = rs.getString("trigger_type");
		switch (type) {
			case "CRON":
				result = new BeanPropertyRowMapper<>(CronTriggerInfo.class).mapRow(rs, rowNum);
				break;
			case "SIMPLE":
				result = new BeanPropertyRowMapper<>(SimpleTriggerInfo.class).mapRow(rs, rowNum);
				break;
			default:
				throw new RuntimeException("Unknown trigger type " + type);
		}
		return result;
	};
	
	@Autowired
	private NamedParameterJdbcTemplate tmsJdbcTemplateNamed;
	
	public List<TriggerInfo> getTriggerInfos(String schedulerName, String fullJobName) {
		Map<String, Object> params = new HashMap<>();
		params.put("scheduler_name", schedulerName);
		params.put("full_job_name", fullJobName);
		return tmsJdbcTemplateNamed.query(
				"select t.trigger_name, t.trigger_group, t.trigger_type, t.trigger_state, " +
				"from_unixtime(t.next_fire_time / 1000) next_fire_time, " +
				"from_unixtime(t.prev_fire_time / 1000) prev_fire_time, " +
				"c.CRON_EXPRESSION, s.REPEAT_COUNT, s.REPEAT_INTERVAL, s.TIMES_TRIGGERED from  " +
				"qrtz_triggers t " +
				"left join qrtz_cron_triggers c  " +
				"on (t.sched_name = c.SCHED_NAME  " +
				" and t.TRIGGER_NAME = c.TRIGGER_NAME  " +
				"    and t.trigger_group = c.trigger_group) " +
				"left join qrtz_simple_triggers s " +
				"on (t.sched_name = s.SCHED_NAME  " +
				" and t.TRIGGER_NAME = s.TRIGGER_NAME  " +
				"    and t.trigger_group = s.trigger_group)" +
				"where t.sched_name = :scheduler_name and concat(t.job_group, '.', t.job_name) = :full_job_name",
				params,
				ROW_MAPPER
		);
	}
}
