package ru.anisimov.keybattle.tms.data;

import org.apache.commons.dbcp2.BasicDataSource;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         06.05.15
 */
public class ConnectionFactory {
	private interface Singleton {
		ConnectionFactory INSTANCE = new ConnectionFactory();
	}

	private DataSource dataSource;

	private ConnectionFactory() {
		Properties properties = new Properties();
		try {
			properties.load(this.getClass().getClassLoader().getResourceAsStream("jdbc.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}

		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName(properties.getProperty("tms.driver"));
		dataSource.setUrl(properties.getProperty("tms.url"));
		dataSource.setUsername(properties.getProperty("tms.username"));
		dataSource.setPassword(properties.getProperty("tms.password"));
		
		this.dataSource = dataSource;
	}

	public static DataSource getDataSource() {
		return Singleton.INSTANCE.dataSource;
	}
	
	public static Connection getDatabaseConnection() throws SQLException {
		return Singleton.INSTANCE.dataSource.getConnection();
	}
}
