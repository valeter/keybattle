package ru.anisimov.keybattle.tms.config;

import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         16.02.15
 */
@Configuration
@ComponentScan("ru.anisimov.keybattle.tms.scheduler")
public class SchedulerConfig {
	@Bean(name = "schedulerFactory")
	public StdSchedulerFactory schedulerFactory() throws IOException, SchedulerException {
		return new StdSchedulerFactory("quartz.properties");
	}
	
	@Bean(name = "jobConfiguration") 
	public Class jobConfiguration() {
		return JobConfig.class;
	}
}
