package ru.anisimov.keybattle.tms.config;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.anisimov.keybattle.core.manager.SecurityKeyManager;
import ru.anisimov.keybattle.tms.data.JobLogManager;
import ru.anisimov.keybattle.tms.job.DummyJob;
import ru.anisimov.keybattle.tms.job.SpringJob;
import ru.anisimov.keybattle.tms.job.TextUploader;
import ru.anisimov.keybattle.tms.listener.JobLoggingListener;
import ru.anisimov.keybattle.tms.listener.SpringJobListener;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         12.02.15
 */
@Configuration
public class JobConfig {
	@Autowired
	private SecurityKeyManager securityKeyManager;
	@Autowired
	private JobLogManager jobLogManager;
	
	@Bean(name = "_dummyJob")
	public SpringJob dummyJob() {
		return new DummyJob();
	}
	
	@Bean(name = "textUploaderJob") 
	public SpringJob textUploaderJob(@Value("${fb2.directory}") String directory) {
		return new TextUploader() {
			@Override
			public String[] cronTriggers() {
				return new String[] {"0 0/30 * * * ?"};
			}

			@Override
			public String description() {
				return "upload fb2 text to db from directory " + directory;
			}
		};
	}

	@Bean(name = "cleanSk")
	public SpringJob cleanSk() {
		return new SpringJob() {
			@Override
			public void execute(JobExecutionContext context) throws JobExecutionException {
				securityKeyManager.clear();
			}

			@Override
			public String[] cronTriggers() {
				return new String[] {"0 0/10 * * * ?"};
			}

			@Override
			public String description() {
				return "clean old and used secret keys";
			}
		};
	}

	@Bean(name = "cleanJobLogs")
	public SpringJob cleanJobLogs() {
		return new SpringJob() {
			@Override
			public void execute(JobExecutionContext context) throws JobExecutionException {
				jobLogManager.clear();
			}

			@Override
			public String[] cronTriggers() {
				return new String[] {"0 0 0/8 * * ?"};
			}

			@Override
			public String description() {
				return "clean logs older than 7 days";
			}
		};
	}

	@Bean(name = "jobExecutionListener") 
	public SpringJobListener jobExecutionListener() {
		return new JobLoggingListener();
	}
}
