package ru.anisimov.keybattle.tms.job;

import com.google.common.base.Strings;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import ru.anisimov.keybattle.core.Pair;
import ru.anisimov.keybattle.core.manager.TextManager;
import ru.anisimov.keybattle.core.model.locale.DictionaryLocale;
import ru.anisimov.keybattle.core.model.text.TextDetails;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.*;
import java.util.*;
import java.util.function.BiFunction;
import java.util.regex.Pattern;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         28.10.15
 */
public class TextUploader implements SpringJob {
	private static final Logger log = LogManager.getLogger(TextUploader.class);
	
	static final int MAX_TEXTS_BY_AUTHOR = 200;
	
	@Value("${fb2.directory}")
	private String root;
	
	@Autowired
	private TextManager textManager;
	
	private SAXParser saxParser;
	private TextFilter filter = new TextFilter();
	
	{
		try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			saxParser = factory.newSAXParser();
		} catch (SAXException | ParserConfigurationException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		for (DictionaryLocale locale : DictionaryLocale.values()) {
			String currentRoot = root + locale.getLanguage();
			try {
				findTexts(new File(currentRoot), locale);
			} catch (Exception e) {
				log.error("Could not upload texts from directory " + currentRoot, e);
			}
		}
	}
	
	protected void findTexts(File root, DictionaryLocale locale) {
		File[] files = root.listFiles();
		if (files == null || files.length == 0) {
			return;
		}

		TextDetails details = null;
		Map<TextDetails, List<String>> texts = new HashMap<>();
		for (File file : files) {
			if (!file.isDirectory() && file.getName().endsWith("fb2")) {
				try (BufferedInputStream in = new BufferedInputStream(new FileInputStream(file))) {
					Pair<TextDetails, List<String>> fileTexts = getTexts(in, locale);
					
					String fullName = fileTexts.getFirst().getAuthorLastName() + " " + fileTexts.getFirst().getAuthorFirstName();
					if (!root.getName().trim().equals(fullName.trim())) {
						log.warn("Wrong author name in root " + root.getAbsolutePath() + " file " + file.getName() + " name " + fullName);
						continue;
					}
					
					if (Strings.isNullOrEmpty(fileTexts.getFirst().getAuthorFirstName()) 
							|| Strings.isNullOrEmpty(fileTexts.getFirst().getAuthorLastName())
							|| Strings.isNullOrEmpty(fileTexts.getFirst().getBookTitle())) {
						log.error("Could not load author info from file " + file.getAbsolutePath());
						continue;
					}
					if (fileTexts.getSecond().size() == 0) {
						log.debug("Could not found any texts in file " + file.getAbsolutePath());
						continue;
					}
					
					if (details == null) {
						details = fileTexts.getFirst();
					} else if (!Objects.equals(details.getAuthorFirstName(), fileTexts.getFirst().getAuthorFirstName()) 
							|| !Objects.equals(details.getAuthorLastName(), fileTexts.getFirst().getAuthorLastName())) {
						log.error("Two different authors in the same root " + root.getAbsolutePath() + " file " + file.getName());
						continue;
					} else if (texts.keySet().stream()
							.anyMatch(d -> Objects.equals(d.getBookTitle(), fileTexts.getFirst().getBookTitle()))) {
						log.error("Two same books in the same root " + root.getAbsolutePath() + " file " + file.getName());
						continue;
					}
					texts.put(fileTexts.getFirst(), fileTexts.getSecond());
				} catch (SAXException e) {
					log.error("SAXException while processing file " + file.getAbsolutePath());
				} catch (Exception e) {
					log.error("Could not process file " + file.getAbsolutePath(), e);
				}
			}
		}
		
		if (details != null && texts.size() > 0) {
			int textCount = cleanupTexts(texts);
			log.debug("For author " + details.getAuthorLastName() + " " + 
					details.getAuthorFirstName() + " found " + textCount + " texts");
		}
		
		Arrays.stream(files)
				.filter(File::isDirectory)
				.forEach(f -> findTexts(f, locale));
	}
	
	protected int cleanupTexts(Map<TextDetails, List<String>> texts) {
		int textCount = 0;
		for (List<String> strings : texts.values()) {
			textCount += strings.size();
		}
		if (textCount <= MAX_TEXTS_BY_AUTHOR) {
			return textCount;
		}

		int avgSize = MAX_TEXTS_BY_AUTHOR / texts.size();
		while (avgSize < 2) {
			int minCount = Integer.MAX_VALUE;
			TextDetails minDetails = null;
			for (TextDetails details : texts.keySet()) {
				if (texts.get(details).size() < minCount) {
					minCount = texts.get(details).size();
					minDetails = details;
				}
			}
			textCount -= minCount;
			texts.remove(minDetails);
			if (textCount <= MAX_TEXTS_BY_AUTHOR) {
				return textCount;
			}
			avgSize = MAX_TEXTS_BY_AUTHOR / texts.size();
		}

		Random rnd = new Random();
		while (true) {
			for (List<String> strings : texts.values()) {
				if (strings.size() > avgSize) {
					strings.remove(rnd.nextInt(strings.size()));
					textCount--;
					if (textCount <= MAX_TEXTS_BY_AUTHOR) {
						return textCount;
					}
				}
			}
		}
	}
	
	protected Pair<TextDetails, List<String>> getTexts(InputStream in, DictionaryLocale locale) throws IOException, SAXException {
		TextDetails details = new TextDetails();
		List<String> texts = new ArrayList<>();
		
		DefaultHandler handler = new DefaultHandler() {
			private Set<Tag> tags = new HashSet<>();
			private Random rnd = new Random();

			@Override
			public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
				Tag tag = Tag.getTag(qName);
				if (tag != null) {
					tags.add(tag);
				}
			}

			@Override
			public void characters(char[] ch, int start, int length) throws SAXException {
				if (tags.contains(Tag.DOCUMENT_INFO)) {
					return;
				}
				if (tags.contains(Tag.AUTHOR) && tags.contains(Tag.FIRST_NAME)) {
					details.setAuthorFirstName(new String(ch, start, length).trim());
					return;
				}
				if (tags.contains(Tag.AUTHOR) && tags.contains(Tag.LAST_NAME)) {
					details.setAuthorLastName(new String(ch, start, length).trim());
					return;
				}
				if (tags.contains(Tag.BOOK_TITLE)) {
					details.setBookTitle(new String(ch, start, length).trim());
					return;
				}
				if (tags.contains(Tag.BODY) && tags.contains(Tag.P)) {
					String text = filter.apply(new String(ch, start, length), TextFilterSettings.LOCALE_SETTINGS.get(locale));
					if (text == null) {
						return;
					}
					if (texts.size() < MAX_TEXTS_BY_AUTHOR) {
						texts.add(text);
					} else if (rnd.nextBoolean()) {
						// add some shuffle
						texts.set(rnd.nextInt(texts.size()), text);
					} 
				}
			}

			@Override
			public void endElement(String uri, String localName, String qName) throws SAXException {
				Tag tag = Tag.getTag(qName);
				if (tag != null) {
					tags.remove(tag);
				}
			}
		};
		
		saxParser.parse(in, handler);

		return new Pair<>(details, texts);
	}
	
	private class TextFilter implements BiFunction<String, TextFilterSettings, String> {
		@Override
		public String apply(String text, TextFilterSettings settings) {
			if (text == null || text.length() == 0) {
				return null;
			}
			
			if (text.length() < settings.minCharactersCount 
					|| text.length() > settings.maxCharactersCount) {
				return null;
			}
			
			String normalizedText = TextNormalizer.normalize(text);

			if (normalizedText.length() < settings.minCharactersCount 
					|| normalizedText.length() > settings.maxCharactersCount) {
				return null;
			}
			for (int i = 0; i < normalizedText.length(); i++) {
				if (!settings.allowedCharacters.contains(normalizedText.charAt(i))) {
					boolean not = false;
					if (normalizedText.charAt(i) >= 'a' && normalizedText.charAt(i) <= 'z') {
						not = true;
					}
					if (normalizedText.charAt(i) >= 'A' && normalizedText.charAt(i) <= 'Z') {
						not = true;
					}
					if (!not)
						System.out.println("Bad character '" + normalizedText.charAt(i) + "' code " + (int)normalizedText.charAt(i));
					return null;
				}
			}
			
			String[] words = normalizedText.split("\\s+");
			if (words.length < settings.minWordsCount || words.length > settings.maxWordsCount) {
				return null;
			}
			if (Arrays.stream(words)
					.map(w -> w.replaceAll("[^\\w]", ""))
					.anyMatch(w -> w.length() > settings.maxWordLength)) {
				return null;
			}
			
			if (settings.localeCustomFilters != null &&
					settings.localeCustomFilters.stream().anyMatch(f -> f.apply(normalizedText, settings) == null)) {
				return null;
			}
			
			return normalizedText;
		}
	}
	
	private static class TextNormalizer {
		public static String normalize(String text) {
			text = text
					.replace('«', '"')
					.replace('»', '"')
					.replace('“', '"')
					.replace('”', '"')
					.replace('„', '"')
					.replace('‘', '\'')
					.replace('_', ' ')
					.replace((char) 8211, '-')
					.replace((char) 8212, '-')
					.replace((char) 8217, '\'')
					.replace((char) 160, ' ')
					.replace((char) 173, ' ')
					.replace((char) 769, ' ')
					.replaceAll("…", "...")
					.replaceAll("\\{[0-9]+\\}", " ")
					.replaceAll("[\\x00-\\x1F\\x7F]", "" );
			
			String[] parts = text.split("\\s+");
			StringBuilder result = new StringBuilder();
			Pattern punctuation = Pattern.compile("[\\p{Punct}]+" );
			for (int i = 0; i < parts.length; i++) {
				String part = parts[i];
				if (i > 0 && !punctuation.matcher(part).matches()) {
					result.append(" ");
				}
				result.append(part);
			}
			return result.toString().trim();
		}
	}
	
	private static class TextFilterSettings {
		private static final Set<Character> PUNCTUATION = new HashSet<>(Arrays.asList(
				'`', '~', '[', ']', '(', ')', ',', '.', '<', '>', '?', '/', '\'', '"', ';', ':', '\\', '|', 
				'{', '}', '-', '_', '=', '+', '!', '@', '#', '$', '%', '^', '&', '*', ';', ' '
		));

		private static final Set<Character> NUMBERS;
		
		private static final Set<Character> ALLOWED_CHARS_EN;
		private static final Set<Character> ALLOWED_CHARS_RU;
		
		static {
			NUMBERS = new HashSet<>();
			for (int i = 0; i <= 9; i++) {
				NUMBERS.add((char)('0' + i));
			}
					
			ALLOWED_CHARS_EN = new HashSet<>(PUNCTUATION);
			ALLOWED_CHARS_EN.addAll(NUMBERS);
			for (char c = 'a'; c <= 'z'; c++) {
				ALLOWED_CHARS_EN.add(c);
				ALLOWED_CHARS_EN.add((char)(c - 'a' + 'A'));
			}
			
			ALLOWED_CHARS_RU = new HashSet<>(PUNCTUATION);
			ALLOWED_CHARS_RU.addAll(NUMBERS);
			for (char c = 'а'; c <= 'я'; c++) {
				ALLOWED_CHARS_RU.add(c);
				ALLOWED_CHARS_RU.add((char)(c - 'а' + 'А'));
			}
			ALLOWED_CHARS_RU.add('Ё');
			ALLOWED_CHARS_RU.add('ё');
		}
		
		public static final Map<DictionaryLocale, TextFilterSettings> LOCALE_SETTINGS = new HashMap<DictionaryLocale, TextFilterSettings>() {{
			put(DictionaryLocale.RU, new TextFilterSettings(
					400, 2000, 80, 400, 16, ALLOWED_CHARS_RU, null
			));
			put(DictionaryLocale.EN, new TextFilterSettings(
					400, 2000, 80, 400, 14, ALLOWED_CHARS_EN, null
			));
		}};
		
		int minCharactersCount;
		int maxCharactersCount;
		int minWordsCount;
		int maxWordsCount;
		int maxWordLength;
		Set<Character> allowedCharacters;
		List<TextFilter> localeCustomFilters;

		public TextFilterSettings(int minCharactersCount, int maxCharactersCount, int minWordsCount, int maxWordsCount, 
								  int maxWordLength, Set<Character> allowedCharacters, List<TextFilter> localeCustomFilters) {
			this.minCharactersCount = minCharactersCount;
			this.maxCharactersCount = maxCharactersCount;
			this.minWordsCount = minWordsCount;
			this.maxWordsCount = maxWordsCount;
			this.maxWordLength = maxWordLength;
			this.allowedCharacters = allowedCharacters;
			this.localeCustomFilters = localeCustomFilters;
		}
	}

	enum Tag {
		DOCUMENT_INFO("document-info", 1),
		AUTHOR("author", 2),
		FIRST_NAME("first-name", 3),
		LAST_NAME("last-name", 3),
		BOOK_TITLE("book-title", 2),
		BODY("body", 1),
		P("p", 3);
		
		private String name;
		private int level;

		Tag(String name, int level) {
			this.name = name;
			this.level = level;
		}

		public String getName() {
			return name;
		}

		public int getLevel() {
			return level;
		}

		public static Tag getTag(String qName) {
			return Arrays.stream(Tag.values())
					.filter(t -> Objects.equals(t.name.toLowerCase(), qName.toLowerCase()))
					.findFirst().orElse(null);
		} 
	}
}
