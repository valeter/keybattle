package ru.anisimov.keybattle.tms.data.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;
import ru.anisimov.keybattle.core.util.db.Pagination;
import ru.anisimov.keybattle.core.util.db.SqlParams;
import ru.anisimov.keybattle.tms.data.model.SchedulerInfo;

import javax.annotation.PostConstruct;
import java.util.Collections;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         17.02.15
 */
@Service
public class SchedulerInfoDAO {
	private Pagination<SchedulerInfo> SCHEDULER_INFO_PAGINATION;
	
	@Autowired
	private NamedParameterJdbcTemplate tmsJdbcTemplateNamed;

	@PostConstruct
	private void init() {
		SCHEDULER_INFO_PAGINATION = new Pagination.Builder<SchedulerInfo>(tmsJdbcTemplateNamed.getJdbcOperations())
				.table("QRTZ_JOB_DETAILS GROUP BY SCHED_NAME")
				.columns(
						"SCHED_NAME SCHEDULER_NAME")
				.rowMapper(new BeanPropertyRowMapper<>(SchedulerInfo.class))
				.build();
	}
	
	public Page<SchedulerInfo> getLastSchedulers(Pageable pageable) {
		return SCHEDULER_INFO_PAGINATION.getPage(pageable, SqlParams.empty());
	}
	
	public int getSchedulersCount() {
		return tmsJdbcTemplateNamed.queryForObject("select count(distinct SCHED_NAME) from qrtz_job_details",
				Collections.EMPTY_MAP, Integer.class);
	}
}
