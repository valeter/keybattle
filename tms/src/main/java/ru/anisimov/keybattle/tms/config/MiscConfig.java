package ru.anisimov.keybattle.tms.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.anisimov.keybattle.core.manager.SecurityKeyManager;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         06.05.15
 */
@Configuration
public class MiscConfig {
	@Bean
	public SecurityKeyManager securityKeyManager() {
		return new SecurityKeyManager();
	}
}
