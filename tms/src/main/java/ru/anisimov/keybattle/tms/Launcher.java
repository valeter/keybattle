package ru.anisimov.keybattle.tms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         12.02.15
 */
@SpringBootApplication
@EnableAutoConfiguration
public class Launcher {
	public static void main(String[] args) throws InterruptedException {
		SpringApplication.run(Launcher.class, args);
	}
}
