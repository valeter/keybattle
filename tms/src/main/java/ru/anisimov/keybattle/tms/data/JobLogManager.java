package ru.anisimov.keybattle.tms.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         07.05.15
 */
@Component
public class JobLogManager {
	private static final int DAYS_TO_STORE_LOGS = 7;
	
	@Autowired
	private JdbcTemplate tmsJdbcTemplate;
	
	public void clear() {
		tmsJdbcTemplate.update("DELETE FROM TMS_LOG WHERE TIMESTAMPDIFF(DAY, EVENTTIME, NOW()) > ?", DAYS_TO_STORE_LOGS);
	}
}
