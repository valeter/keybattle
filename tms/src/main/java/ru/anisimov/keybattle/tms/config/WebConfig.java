package ru.anisimov.keybattle.tms.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import ru.anisimov.keybattle.core.config.CoreConfig;
import ru.anisimov.keybattle.core.config.ManagerConfig;
import ru.anisimov.keybattle.core.config.PersistenceConfig;
import ru.anisimov.keybattle.tms.data.ConnectionFactory;

import javax.sql.DataSource;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         16.02.15
 */
@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages = {
		"ru.anisimov.keybattle.tms.controller",
		"ru.anisimov.keybattle.tms.data",
})
@Import({PersistenceConfig.class, CoreConfig.class, ManagerConfig.class, PropertyPlaceholderConfig.class, MiscConfig.class})
public class WebConfig {
	// replace existing data source bean
	@Bean(name = "tmsDataSource")
	public DataSource tmsDataSource() {
		return ConnectionFactory.getDataSource();
	}

	@Bean
	public ObjectMapper jacksonObjectMapper() {
		return new ObjectMapper() {{
			this.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
		}};
	}

	@Bean
	public SerializationConfig serializationConfig() {
		return jacksonObjectMapper().getSerializationConfig();
	}
}
