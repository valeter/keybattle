package ru.anisimov.keybattle.tms.data.model;

import java.util.List;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         17.02.15
 */
public class SchedulerInfo {
	private String schedulerName;
	private List<JobInfo> jobInfos;

	public SchedulerInfo() {
	}

	public SchedulerInfo(String schedulerName, List<JobInfo> jobInfos) {
		this.schedulerName = schedulerName;
		this.jobInfos = jobInfos;
	}

	public String getSchedulerName() {
		return schedulerName;
	}

	public void setSchedulerName(String schedulerName) {
		this.schedulerName = schedulerName;
	}

	public List<JobInfo> getJobInfos() {
		return jobInfos;
	}

	public void setJobInfos(List<JobInfo> jobInfos) {
		this.jobInfos = jobInfos;
	}
}
