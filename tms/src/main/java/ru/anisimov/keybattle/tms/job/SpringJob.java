package ru.anisimov.keybattle.tms.job;

import org.quartz.*;

import java.io.Serializable;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         12.02.15
 */
@PersistJobDataAfterExecution
public interface SpringJob extends Job, Serializable {
	void execute(JobExecutionContext context) throws JobExecutionException;
	
	default long delaySeconds() {
		return 0;
	}

	default String[] cronTriggers() {
		return new String[0];
	}
	
	default ScheduleBuilder[] scheduleTriggers() {
		return new ScheduleBuilder[0];
	}
	
	default String description() {
		return this.getClass().getSimpleName();
	}
}
