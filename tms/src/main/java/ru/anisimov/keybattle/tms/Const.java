package ru.anisimov.keybattle.tms;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         13.02.15
 */
public interface Const {
	String JOB_BEAN = "_job_bean";
	String JOB_GROUP = "_job_group";
}
