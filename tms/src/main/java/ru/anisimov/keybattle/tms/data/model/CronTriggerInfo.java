package ru.anisimov.keybattle.tms.data.model;

import java.util.Date;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         07.05.15
 */
public class CronTriggerInfo extends TriggerInfo {
	private String cronExpression;

	public CronTriggerInfo() {
	}

	public CronTriggerInfo(String triggerName, String triggerGroup, Date nextFireTime, Date prevFireTime, String triggerState, String triggerType, String cronExpression) {
		super(triggerName, triggerGroup, nextFireTime, prevFireTime, triggerState, triggerType);
		this.cronExpression = cronExpression;
	}

	public String getCronExpression() {
		return cronExpression;
	}

	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}
}
