package ru.anisimov.keybattle.tms.data.model;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         17.02.15
 */
public class JobInfo {
	private String fullName;
	private String jobName;
	private String groupName;
	private String description;
	private String className;

	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
	private Date nextTriggerFired;
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
	private Date lastTriggerFired;
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
	private Date lastFiredStart;
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
	private Date lastFiredEnd;
	
	private boolean lastFiredSuccess;
	private String lastFiredException;
	
	private List<TriggerInfo> triggerInfos;

	public JobInfo() {
	}

	public JobInfo(String fullName, String jobName, String groupName, String description, String className, Date nextTriggerFired, Date lastTriggerFired, Date lastFiredStart, Date lastFiredEnd, boolean lastFiredSuccess, String lastFiredException) {
		this.fullName = fullName;
		this.jobName = jobName;
		this.groupName = groupName;
		this.description = description;
		this.className = className;
		this.nextTriggerFired = nextTriggerFired;
		this.lastTriggerFired = lastTriggerFired;
		this.lastFiredStart = lastFiredStart;
		this.lastFiredEnd = lastFiredEnd;
		this.lastFiredSuccess = lastFiredSuccess;
		this.lastFiredException = lastFiredException;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public boolean isLastFiredSuccess() {
		return lastFiredSuccess;
	}

	public void setLastFiredSuccess(boolean lastFiredSuccess) {
		this.lastFiredSuccess = lastFiredSuccess;
	}

	public String getLastFiredException() {
		return lastFiredException;
	}

	public void setLastFiredException(String lastFiredException) {
		this.lastFiredException = lastFiredException;
	}

	public List<TriggerInfo> getTriggerInfos() {
		return triggerInfos;
	}

	public void setTriggerInfos(List<TriggerInfo> triggerInfos) {
		this.triggerInfos = triggerInfos;
	}

	public Date getNextTriggerFired() {
		return nextTriggerFired;
	}

	public void setNextTriggerFired(Date nextTriggerFired) {
		this.nextTriggerFired = nextTriggerFired;
	}

	public Date getLastTriggerFired() {
		return lastTriggerFired;
	}

	public void setLastTriggerFired(Date lastTriggerFired) {
		this.lastTriggerFired = lastTriggerFired;
	}

	public Date getLastFiredStart() {
		return lastFiredStart;
	}

	public void setLastFiredStart(Date lastFiredStart) {
		this.lastFiredStart = lastFiredStart;
	}

	public Date getLastFiredEnd() {
		return lastFiredEnd;
	}

	public void setLastFiredEnd(Date lastFiredEnd) {
		this.lastFiredEnd = lastFiredEnd;
	}
}
