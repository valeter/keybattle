package ru.anisimov.keybattle.tms.job;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import ru.anisimov.keybattle.tms.scheduler.SchedulerManager;
import ru.anisimov.keybattle.tms.Const;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         25.02.15
 */
public class SpringJobWrapper implements SpringJob {
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		String contextName = (String) context.getJobDetail().getJobDataMap().get(Const.JOB_GROUP);
		String jobName = (String) context.getJobDetail().getJobDataMap().get(Const.JOB_BEAN);
		SpringJob springJob = SchedulerManager.getContext(contextName).getBean(jobName, SpringJob.class);
		springJob.execute(context);
	}
}
