package ru.anisimov.keybattle.tms.job;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.*;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         12.02.15
 */
public class DummyJob implements SpringJob {
	private static final Logger log = LogManager.getLogger(DummyJob.class);
	
	private static final int INTERVAL_IN_SECONDS = 5;
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		log.debug("Dummy job done");
	}

	@Override
	public ScheduleBuilder[] scheduleTriggers() {
		return new ScheduleBuilder[] {
				SimpleScheduleBuilder
						.simpleSchedule()
						.withIntervalInSeconds(INTERVAL_IN_SECONDS)
						.repeatForever()
						.withMisfireHandlingInstructionFireNow()
		};
	}

	@Override
	public String description() {
		return "executing every 5 sec";
	}
}
