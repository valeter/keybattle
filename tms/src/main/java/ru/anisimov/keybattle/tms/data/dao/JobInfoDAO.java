package ru.anisimov.keybattle.tms.data.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;
import ru.anisimov.keybattle.tms.data.model.JobInfo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         17.02.15
 */
@Service
public class JobInfoDAO {
	@Autowired
	private NamedParameterJdbcTemplate tmsJdbcTemplateNamed;

	public List<JobInfo> getForScheduler(String schedulerName, Integer count, Integer offset) {
		String sql = 
				"SELECT j.full_job_name full_name, " +
				"  j.job_name, " +
				"  j.job_group group_name , " +
				"  j.description, " +
				"  j.JOB_CLASS_NAME class_name, " +
				"  from_unixtime(MAX(t.NEXT_FIRE_TIME) / 1000) next_trigger_fired, " +
				"  from_unixtime(MAX(t.PREV_FIRE_TIME) / 1000) last_trigger_fired, " +
				"  l.EVENTTIME_START last_fired_start, " +
				"  l.EVENTTIME_END last_fired_end, " +
				"  l.EXCEPTION last_fired_exception, " +
				"  CASE " +
				"    WHEN l.EXCEPTION             IS NOT NULL " +
				"    AND char_length(l.EXCEPTION) != 0 " +
				"    THEN 0 " +
				"    ELSE 1 " +
				"  END last_fired_success " +
				"FROM " +
				"  (SELECT concat(job_group, '.', job_name) full_job_name, " +
				"    job_name, " +
				"    job_group, " +
				"    description, " +
				"    JOB_CLASS_NAME " +
				"  FROM qrtz_job_details " +
				"  WHERE sched_name = :scheduler_name " +
				"  ) j " +
				"LEFT JOIN " +
				"  (SELECT concat(job_group, '.', job_name) full_job_name, " +
				"    NEXT_FIRE_TIME, " +
				"    PREV_FIRE_TIME " +
				"  FROM qrtz_triggers " +
				"  WHERE sched_name = :scheduler_name " +
				"  ) t " +
				"ON (j.full_job_name = t.full_job_name) " +
				"LEFT JOIN " +
				"  (SELECT EVENTTIME_START, EVENTTIME_END, EXCEPTION, JOB " +
				"  FROM v_tms_log " +
				"  ) l " +
				"ON (j.full_job_name = l.JOB) " +
				"GROUP BY j.full_job_name, " +
				"  j.job_name, " +
				"  j.job_group, " +
				"  j.description, " +
				"  j.JOB_CLASS_NAME, " +
				"  l.EXCEPTION " +
				"ORDER BY j.full_job_name";
		
		Map<String, Object> params = new HashMap<>();
		params.put("scheduler_name", schedulerName);
		if (count != null) {
			sql += " limit :count offset :offset";
			params.put("count", Long.valueOf(count));
			params.put("offset", Long.valueOf(offset));
		}
		return tmsJdbcTemplateNamed.query(sql, params, new BeanPropertyRowMapper<>(JobInfo.class));
	}
}
