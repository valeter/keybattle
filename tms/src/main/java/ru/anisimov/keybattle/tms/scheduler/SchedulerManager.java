package ru.anisimov.keybattle.tms.scheduler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.matchers.KeyMatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;
import ru.anisimov.keybattle.tms.Const;
import ru.anisimov.keybattle.tms.config.JobConfig;
import ru.anisimov.keybattle.tms.job.SpringJobWrapper;
import ru.anisimov.keybattle.tms.listener.SpringJobListener;
import ru.anisimov.keybattle.tms.job.SpringJob;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         12.02.15
 */
@Component
public class SchedulerManager {
	private static final Logger log = LogManager.getLogger(SchedulerManager.class);
	
	private static final Object SUCCESS = new Object();
	
	private Scheduler scheduler;
	
	@Autowired
	private ApplicationContext applicationContext;
	@Autowired
	private StdSchedulerFactory schedulerFactory;
	@Autowired
	private Class defaultConfiguration;
	
	private static ConcurrentMap<String, ApplicationContext> initializedContexts = new ConcurrentHashMap<>();
	
	public SchedulerManager() {
		this(null);
	}

	public SchedulerManager(Scheduler scheduler) {
		this.scheduler = scheduler;
	}
	
	public boolean runJob(String jobName) {
		return runJob(defaultConfiguration.getSimpleName(), jobName);
	}

	private boolean runJob(String groupName, String jobName) {
		JobKey key = JobKey.jobKey(jobName, groupName);
		return makeIfExists(key, () -> scheduler.triggerJob(key));
	}

	public boolean pauseJob(String jobName) {
		return pauseJob(defaultConfiguration.getSimpleName(), jobName);
	}

	private boolean pauseJob(String groupName, String jobName) {
		JobKey key = JobKey.jobKey(jobName, groupName);
		return makeIfExists(key, () -> scheduler.pauseJob(key));
	}

	public boolean resumeJob(String jobName) {
		return resumeJob(defaultConfiguration.getSimpleName(), jobName);
	}

	private boolean resumeJob(String groupName, String jobName) {
		JobKey key = JobKey.jobKey(jobName, groupName);
		return makeIfExists(key, () -> scheduler.resumeJob(key));
	}

	public boolean deleteJob(String jobName) {
		return deleteJob(defaultConfiguration.getSimpleName(), jobName);
	}

	private boolean deleteJob(String groupName, String jobName) {
		JobKey key = JobKey.jobKey(jobName, groupName);
		return makeIfExists(key, () -> scheduler.deleteJob(key));
	}
	
	private boolean makeIfExists(JobKey key, VoidSchedulerAction action) {
		if (makeSchedulerAction(() -> scheduler.getJobDetail(key)) != null) {
			makeSchedulerAction(action);
			return true;
		}
		return false;
	}
	
	@PostConstruct
	public void initialize() {
		initialize(defaultConfiguration);
	}
	
	private void initialize(Class jobConfiguration) {
		AnnotationConfigApplicationContext jobContext = new AnnotationConfigApplicationContext();
		jobContext.setParent(applicationContext);
		jobContext.register(JobConfig.class);
		jobContext.refresh();
		initialize(jobConfiguration.getSimpleName(), jobContext);
	}
	
	private void initialize(String groupName, ApplicationContext context) {
		if (scheduler == null) {
			scheduler = makeSchedulerAction((SchedulerAction<Scheduler>) schedulerFactory::getScheduler);
		}
		
		if (initializedContexts.containsKey(groupName)) {
			throw new RuntimeException("Context " + groupName + " have already been initialized");
		}
		
		scheduleJobs(groupName, context);
		addJobListeners(groupName, context);
		initializedContexts.putIfAbsent(groupName, context);
		
		if (!makeSchedulerAction(scheduler::isStarted)) {
			makeSchedulerAction(scheduler::start);
		}
	}
	
	public static ApplicationContext getContext(String groupName) {
		return initializedContexts.get(groupName);
	}
	
	private void scheduleJobs(String groupName, ApplicationContext context) {
		for (Map.Entry<String, SpringJob> jobEntry : context.getBeansOfType(SpringJob.class).entrySet()) {
			String jobName = jobEntry.getKey();
			SpringJob job = jobEntry.getValue();
			
			JobDetail jobDetail = buildJobDetail(jobName, groupName, job);
			Set<? extends Trigger> triggers = buildTriggers(jobName, groupName, job);

			if (SUCCESS == makeSchedulerAction(() -> {
				try {
					scheduler.scheduleJob(jobDetail, triggers, true);
				} catch (ObjectAlreadyExistsException e) {
					throw new RuntimeException(e);
				}
			})) {
				log.info("Job " + jobName + " scheduled successfully");
			}
		}
	}
	
	private JobDetail buildJobDetail(String jobName, String groupName, SpringJob job) {
		JobDataMap jobDataMap = new JobDataMap();
		jobDataMap.putIfAbsent(Const.JOB_BEAN, jobName);
		jobDataMap.putIfAbsent(Const.JOB_GROUP, groupName);
		
		return JobBuilder
				.newJob(SpringJobWrapper.class)
				.withDescription(job.description())
				.setJobData(jobDataMap)
				.withIdentity(jobName, groupName)
				.build();
	}
	
	private Set<? extends Trigger> buildTriggers(String jobName, String groupName, SpringJob job) {
		Set<Trigger> result = new HashSet<>();

		int scheduleTriggerCount = 0;
		for (ScheduleBuilder scheduleBuilder : job.scheduleTriggers()) {
			result.add(TriggerBuilder
					.newTrigger()
					.withSchedule(scheduleBuilder)
					.startAt(getTriggerStartDate(job.delaySeconds()))
					.withIdentity(jobName + " schedule trigger #" + (scheduleTriggerCount++),
							groupName + " schedule")
					.build());
		}
		
		int cronTriggerCount = 0;
		for (String cronExpression : job.cronTriggers()) {
			result.add(TriggerBuilder
					.newTrigger()
					.withSchedule(CronScheduleBuilder.cronSchedule(cronExpression))
					.startAt(getTriggerStartDate(job.delaySeconds()))
					.withIdentity(jobName + " cron trigger #" + (cronTriggerCount++),
							groupName + " cron")
					.build());
		}

		return result;
	}
	
	private Date getTriggerStartDate(long delayInSeconds) {
		return Date.from(LocalDateTime
				.now()
				.plus(delayInSeconds, ChronoUnit.SECONDS)
				.atZone(ZoneOffset.systemDefault())
				.toInstant());
	}

	private void addJobListeners(String groupName, ApplicationContext context) {
		ListenerManager listenerManager = makeSchedulerAction(scheduler::getListenerManager);
		for (Map.Entry<String, SpringJobListener> listenerEntry :
				context.getBeansOfType(SpringJobListener.class).entrySet()) {
			SpringJobListener listener = listenerEntry.getValue();

			List<Matcher<JobKey>> matchers = new ArrayList<>(listener.customMatchers());
			for (String jobName : listener.jobsToListen()) {
				matchers.add(KeyMatcher.keyEquals(JobKey.jobKey(jobName, groupName)));
			}

			listenerManager.addJobListener(listener, matchers);
		}
	}

	@PreDestroy
	public void shutdown() {
		shutdown(true);
	}
	
	private void shutdown(boolean waitForJobsToComplete) {
		makeSchedulerAction(() -> scheduler.shutdown(waitForJobsToComplete));
	}

	private Object makeSchedulerAction(VoidSchedulerAction action) {
		return makeSchedulerAction(() -> {
			action.make();
			return SUCCESS;
		});
	}

	private <T> T makeSchedulerAction(SchedulerAction<T> action) {
		try {
			return action.makeAndGet();
		} catch (SchedulerException e) {
			log.error("Exception while instantiating scheduler", e);
			throw new RuntimeException(e);
		} catch (Exception e) {
			log.error("Unknown exception", e);
			throw new RuntimeException(e);
		}
	}
	

	private interface SchedulerAction<T> {
		T makeAndGet() throws SchedulerException;
	}

	private interface VoidSchedulerAction {
		void make() throws SchedulerException;
	}
}
