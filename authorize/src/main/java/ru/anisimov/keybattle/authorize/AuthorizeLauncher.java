package ru.anisimov.keybattle.authorize;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         06.12.16
 */
@SpringBootApplication
@EnableAutoConfiguration
public class AuthorizeLauncher {
    public static void main(String[] args) throws InterruptedException {
        SpringApplication.run(AuthorizeLauncher.class, args);
    }
}
