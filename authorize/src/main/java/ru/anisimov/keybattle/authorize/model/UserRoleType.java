package ru.anisimov.keybattle.authorize.model;

import org.springframework.security.core.GrantedAuthority;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         11/2/14
 */
public enum UserRoleType implements GrantedAuthority {
    ADMINISTRATOR,
    USER;

    @Override
    public String getAuthority() {
        return this.name();
    }
}
