package ru.anisimov.keybattle.authorize.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.anisimov.keybattle.authorize.model.Registration;
import ru.anisimov.keybattle.authorize.model.User;
import ru.anisimov.keybattle.authorize.model.UserRole;
import ru.anisimov.keybattle.authorize.model.UserRoleType;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Collections.EMPTY_LIST;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         10/15/14
 */
@Service("userService")
public class DbUserService implements UserService {
	private static final RowMapper<User> USER_ROW_MAPPER = (rs, rowNum) -> new User(
			rs.getLong("ID"),
			rs.getString("USERNAME"),
			rs.getString("PASSWORD"),
			rs.getInt("LOCKED") == 1,
			rs.getInt("ENABLED") == 1,
			rs.getString("ROLES") != null ? Arrays.stream(rs.getString("ROLES").split(","))
					.peek(String::trim)
					.map(role -> UserRoleType.values()[Integer.parseInt(role)])
					.collect(Collectors.toList()) : Collections.EMPTY_LIST
	);

	private static final RowMapper<UserRole> USER_ROLE_ROW_MAPPER = (rs, rowNum) -> {
		UserRole result = new UserRole();
		result.setUserId(rs.getLong("USER_ID"));
		result.setUserName(rs.getString("USERNAME"));
		result.setType(UserRoleType.values()[rs.getInt("TYPE_ID")]);
		result.setEnabled(rs.getInt("ENABLED") == 1);
		Timestamp time = rs.getTimestamp("CREATION_TIME");
		result.setCreationTime(time == null ? null : time.toLocalDateTime());
		time = rs.getTimestamp("MODIFICATION_TIME");
		result.setModificationTime(time == null ? null : time.toLocalDateTime());
		return result;
	};

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User result = getUserByUserName(username);
		if (result == null) {
			throw  new UsernameNotFoundException(String.format("Username %s not found", username));
		}
		return result;
	}

	private static final String ADD_USER = "INSERT INTO USER(USERNAME, PASSWORD) VALUES (?,?)";

	private static final String ADD_USER_ROLE_BY_ID = "INSERT INTO USER_ROLE(USER_ID, TYPE_ID) VALUES (?,?) " +
			" ON DUPLICATE KEY UPDATE ENABLED = 1";

	private static final String CHANGE_PASSWORD = "UPDATE USER SET PASSWORD = ? WHERE ID = ?";

	private static final String REMOVE_USER_ROLE = "UPDATE USER_ROLE SET " +
			" ENABLED = 0 WHERE USER_ID = ? AND TYPE_ID = ?";

	private static final String LOCK_USER = "UPDATE USER SET LOCKED = ? WHERE ID = ?";

	private static final String ENABLE_USER = "UPDATE USER SET ENABLED = ? WHERE ID = ?";

	private static final String SELECT_USERS = "SELECT U.ID, U.USERNAME, U.PASSWORD, U.LOCKED, U.ENABLED, GROUP_CONCAT(R.TYPE_ID) ROLES " +
			" FROM USER U LEFT JOIN (SELECT * FROM USER_ROLE WHERE ENABLED = %s) R ON (U.ID = R.USER_ID) WHERE U.LOCKED = %s AND U.ENABLED = %s GROUP BY U.ID";

	private static final String SELECT_USERS_BY_ROLE = "SELECT U.ID, U.USERNAME, U.PASSWORD, U.LOCKED, U.ENABLED, GROUP_CONCAT(R.TYPE_ID) ROLES " +
			" FROM USER U LEFT JOIN (SELECT * FROM USER_ROLE WHERE ENABLED = 1) R ON (U.ID = R.USER_ID) " +
			" 	WHERE U.ID IN (SELECT DISTINCT USER_ID FROM USER_ROLE WHERE TYPE_ID = ? AND ENABLED = 1) GROUP BY U.ID";

	private static final String SELECT_USER_BY_ID = "SELECT U.ID, U.USERNAME, U.PASSWORD, U.LOCKED, U.ENABLED, GROUP_CONCAT(R.TYPE_ID) ROLES " +
			" FROM USER U LEFT JOIN (SELECT * FROM USER_ROLE WHERE ENABLED = 1) R ON (U.ID = R.USER_ID) WHERE U.ID = ? GROUP BY U.ID";

	private static final String SELECT_USER_BY_USERNAME = "SELECT U.ID, U.USERNAME, U.PASSWORD, U.LOCKED, U.ENABLED, GROUP_CONCAT(R.TYPE_ID) ROLES " +
			" FROM USER U LEFT JOIN (SELECT * FROM USER_ROLE WHERE ENABLED = 1) R ON (U.ID = R.USER_ID) WHERE U.USERNAME = ? GROUP BY U.ID";

	private static final String SELECT_USER_BY_EMAIL = "SELECT U.ID, U.USERNAME, U.PASSWORD, U.LOCKED, U.ENABLED, GROUP_CONCAT(R.TYPE_ID) ROLES " +
			" FROM USER U LEFT JOIN (SELECT * FROM USER_ROLE WHERE ENABLED = 1) R ON (U.ID = R.USER_ID) " +
			" WHERE U.ID = (SELECT USER_ID FROM USER_INFO WHERE EMAIL = ?) GROUP BY U.ID";

	private static final String SELECT_USER_ROLES_BY_ID = "SELECT * FROM USER_ROLE WHERE USER_ID = ?";

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public Long addUser(Registration user) {
		KeyHolder result = new GeneratedKeyHolder();
		if (jdbcTemplate.update(con -> {
			PreparedStatement query = con.prepareStatement(ADD_USER, Statement.RETURN_GENERATED_KEYS);
			query.setString(1, user.getUserName());
			query.setString(2, user.getPassword());
			return query;
		}, result) != 0) {
			return result.getKey().longValue();
		}
		return null;
	}

	@Override
	public boolean changeUserPassword(long userId, String password) {
		return jdbcTemplate.update(CHANGE_PASSWORD, password, userId) == 1;
	}

	@Override
	public boolean addRole(long userId, UserRoleType role) {
		return jdbcTemplate.update(ADD_USER_ROLE_BY_ID, userId, role.ordinal()) == 1;
	}

	@Override
	public boolean removeRole(long userId, UserRoleType role) {
		return jdbcTemplate.update(REMOVE_USER_ROLE, userId, role.ordinal()) == 1;
	}

	@Override
	public boolean lockUser(long userId, boolean locked) {
		return jdbcTemplate.update(LOCK_USER, locked ? 1: 0, userId) == 1;
	}

	@Override
	public boolean enableUser(long userId, boolean enabled) {
		return jdbcTemplate.update(ENABLE_USER, enabled ? 1: 0, userId) == 1;
	}

	@Override
	public List<User> getUsers(Boolean withActiveRoles, Boolean locked, Boolean enabled) {
		String sql = String.format(SELECT_USERS,
				withActiveRoles == null ? "ENABLED" : (withActiveRoles ? "1" : "0"),
				locked == null ? "U.LOCKED" : (locked ? "1" : "0"),
				enabled == null ? "U.ENABLED" : (enabled ? "1" : "0")
		);
		List<User> users = jdbcTemplate.query(sql, USER_ROW_MAPPER);
		return users == null ? EMPTY_LIST : users;
	}

	@Override
	public List<User> getUsers(UserRoleType role) {
		List<User> users = jdbcTemplate.query(SELECT_USERS_BY_ROLE, USER_ROW_MAPPER, role.ordinal());
		return users == null ? EMPTY_LIST : users;
	}

	@Override
	public User getUserById(long userId) {
		List<User> users = jdbcTemplate.query(SELECT_USER_BY_ID, USER_ROW_MAPPER, userId);
		return users != null && users.size() > 0 ? users.get(0) : null;
	}

	@Override
	public User getUserByUserName(String userName) {
		List<User> users = jdbcTemplate.query(SELECT_USER_BY_USERNAME, USER_ROW_MAPPER, userName);
		return users != null && users.size() > 0 ? users.get(0) : null;
	}

	@Override
	public User getUserByEmail(String email) {
		List<User> users = jdbcTemplate.query(SELECT_USER_BY_EMAIL, USER_ROW_MAPPER, email);
		return users != null && users.size() > 0 ? users.get(0) : null;
	}

	@Override
	public List<UserRole> getUserRoles(long userId, Boolean enabled) {
		String sql = SELECT_USER_ROLES_BY_ID;
		sql += enabled == null ? "" : (enabled ? " WHERE ENABLED = 1" : " WHERE ENABLED = 0");
		return jdbcTemplate.query(sql, USER_ROLE_ROW_MAPPER, userId);
	}
}
