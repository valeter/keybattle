package ru.anisimov.keybattle.authorize.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.anisimov.keybattle.authorize.controller.model.UserRoleRequest;
import ru.anisimov.keybattle.authorize.model.Registration;
import ru.anisimov.keybattle.authorize.model.User;
import ru.anisimov.keybattle.authorize.model.UserRole;
import ru.anisimov.keybattle.authorize.service.UserManager;
import ru.anisimov.keybattle.authorize.util.PasswordEncoding;
import ru.anisimov.keybattle.core.Pair;
import ru.anisimov.keybattle.core.manager.MailManager;
import ru.anisimov.keybattle.core.manager.SecurityKeyManager;

import javax.validation.Valid;
import java.util.List;
import java.util.Locale;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         11/2/14
 */
@RestController("/user")
public class UserController {
    @Autowired
    private UserManager userManager;
    @Autowired
    private SecurityKeyManager securityKeyManager;
    @Autowired
    private MailManager mailManager;

    @RequestMapping(method = RequestMethod.POST)
    public String processRegistration(@Valid Registration registration, BindingResult result, Locale locale) {
        if (result.hasErrors()) {
            return SIGN_UP;
        }

        registration.setPassword(PasswordEncoding.encodePassword(registration.getPassword()));
        registration.setConfirmPassword(null);
        Long userId = userManager.addUser(registration, FakeUsers.SYSTEM);
        if (userId == null) {
            throw new RuntimeException("Could not add user " + registration);
        }
        sendRegistrationConfirmLetter(userId, registration, locale);

        return "redirect:" + SIGN_UP_SUCCESS;
    }

    private void sendRegistrationConfirmLetter(long userId, Registration registration, Locale locale) {
        Pair<Long, String> sk = securityKeyManager.create(userId, SecurityKeyType.REGISTRATION);
        if (sk == null) {
            throw new RuntimeException("Could not generate sk for user " + registration);
        }
        if (!mailManager.sendRegistrationConfirm(registration.getEmail(), sk.getFirst(), sk.getSecond(), locale)) {
            throw new RuntimeException(String.format("Could not store registration confirm message userId: %d %s", userId, registration.toString()));
        }
    }

    @RequestMapping(value = SIGN_UP_CONFIRM, method = RequestMethod.GET, params = {Params.SK, Params.ENTITY_ID})
    public String confirmRegistration(
            @RequestParam(Params.SK) String sk,
            @RequestParam(Params.ENTITY_ID) Long id) {
        return securityKeyManager.check(SecurityKeyType.REGISTRATION, id, sk,
                (userId) -> userManager.enableUser(userId, FakeUsers.SYSTEM, "email confirmed"))
                ? "redirect:" + SIGN_UP_CONFIRM_SUCCESS
                : "redirect:" + SIGN_UP_CONFIRM_FAIL;
    }
    

    @RequestMapping(value = "/me", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public User getCurrentUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return auth == null || auth.getName() == null ? null : ((User) auth.getPrincipal());
    }

    @RequestMapping(value = "/roles/{userId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public List<UserRole> getUserRoles(
            @PathVariable("userId") Long userId,
            @RequestParam(value = "enabled", required = false) Boolean enabled) {
        return userManager.getUserRoles(userId, enabled);
    }

    @RequestMapping(value = "/roles/{userId}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity addUserRole(
            @PathVariable("userId") Long userId,
            @RequestBody UserRoleRequest request) {
        return userManager.addUserRole(userId, request.getRole(), request.getActorId(), request.getComment())
                ? ResponseEntity.ok().build()
                : ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    @RequestMapping(value = "/roles/{userId}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity removeUserRole(
            @PathVariable("userId") Long userId,
            @RequestBody UserRoleRequest request) {
        return userManager.removeUserRole(userId, request.getRole(), request.getActorId(), request.getComment())
                ? ResponseEntity.ok().build()
                : ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
}
