package ru.anisimov.keybattle.authorize.service;

import org.springframework.security.core.userdetails.UserDetailsService;
import ru.anisimov.keybattle.authorize.model.Registration;
import ru.anisimov.keybattle.authorize.model.User;
import ru.anisimov.keybattle.authorize.model.UserRole;
import ru.anisimov.keybattle.authorize.model.UserRoleType;

import java.util.List;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         10/14/14
 */
public interface UserService extends UserDetailsService {
	Long addUser(Registration user);
	
	boolean addUser(User newUser);

	boolean changeUserPassword(long userId, String password);

	boolean removeUser(long userId);

	boolean addRole(long userId, UserRoleType role);

	boolean removeRole(long userId, UserRoleType role);

	boolean lockUser(long userId, boolean locked);

	boolean enableUser(long userId, boolean enabled);

	List<User> getUsers(Boolean withActiveRoles, Boolean locked, Boolean enabled);

	List<User> getUsers(UserRoleType role);

	List<User> getUsers();

	List<UserRole> getUserRoles(long userId, Boolean enabled);

	User getUserById(long userId);

	User getUserByUserName(String userName);

	User getUserByEmail(String email);
}
