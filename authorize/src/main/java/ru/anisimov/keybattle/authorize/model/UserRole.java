package ru.anisimov.keybattle.authorize.model;

import java.time.LocalDateTime;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         11/2/14
 */
public class UserRole {
    private Long userId;
    private String userName;
    private UserRoleType type;
    private boolean enabled;
    private LocalDateTime creationTime;
    private LocalDateTime modificationTime;

    public UserRole() {
    }

    public UserRole(Long userId, String userName, UserRoleType type, boolean enabled, LocalDateTime creationTime, LocalDateTime modificationTime) {
        this.userId = userId;
        this.userName = userName;
        this.type = type;
        this.enabled = enabled;
        this.creationTime = creationTime;
        this.modificationTime = modificationTime;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public UserRoleType getType() {
        return type;
    }

    public void setType(UserRoleType type) {
        this.type = type;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public LocalDateTime getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(LocalDateTime creationTime) {
        this.creationTime = creationTime;
    }

    public LocalDateTime getModificationTime() {
        return modificationTime;
    }

    public void setModificationTime(LocalDateTime modificationTime) {
        this.modificationTime = modificationTime;
    }
}
