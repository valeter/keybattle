package ru.anisimov.keybattle.authorize.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         11/2/14
 */
public class PasswordEncoding {
    private static final int BCRYPT_PASSWORD_ENCODER_STRENGTH = 12;

    private static final PasswordEncoder PASSWORD_ENCODER = 
			new BCryptPasswordEncoder(BCRYPT_PASSWORD_ENCODER_STRENGTH);

    public static PasswordEncoder passwordEncoder() {
        return PASSWORD_ENCODER;
    }

    public static String encodePassword(String password) {
        return PASSWORD_ENCODER.encode(password);
    }
	
	public static boolean checkPassword(String password, String storedHash) {
		return PASSWORD_ENCODER.matches(password, storedHash);
	}
}
