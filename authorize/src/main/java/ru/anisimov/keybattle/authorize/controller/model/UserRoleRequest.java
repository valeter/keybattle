package ru.anisimov.keybattle.authorize.controller.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import ru.anisimov.keybattle.authorize.model.UserRoleType;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         06.12.16
 */
public class UserRoleRequest {
    @JsonProperty("actorId")
    private Long actorId;
    @JsonProperty("role")
    private UserRoleType role;
    @JsonProperty("comment")
    private String comment;

    public UserRoleRequest() {
    }

    public Long getActorId() {
        return actorId;
    }

    public void setActorId(Long actorId) {
        this.actorId = actorId;
    }

    public UserRoleType getRole() {
        return role;
    }

    public void setRole(UserRoleType role) {
        this.role = role;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
