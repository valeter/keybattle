package ru.anisimov.keybattle.authorize.config;

import net.tanesha.recaptcha.ReCaptcha;
import net.tanesha.recaptcha.ReCaptchaImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import ru.anisimov.keybattle.core.config.PersistenceConfig;
import ru.anisimov.keybattle.core.security.CredentialsLoader;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         06.12.16
 */
@Configuration
@ComponentScan(basePackages = {
        "ru.anisimov.keybattle.authorize.controller",
        "ru.anisimov.keybattle.authorize.service"
})
@Import(PersistenceConfig.class)
public class AuthorizeConfig {
    @Bean
    public CredentialsLoader credentialsLoader() {
        return new CredentialsLoader();
    }
    
    @Bean
    public ReCaptcha reCaptcha(CredentialsLoader credentialsLoader) {
        String recaptchaKey = credentialsLoader.recaptcha();
        ReCaptchaImpl result = new ReCaptchaImpl();
        result.setPrivateKey(recaptchaKey);
        return result;
    }
}
