package ru.anisimov.keybattle.authorize.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.anisimov.keybattle.authorize.model.Registration;
import ru.anisimov.keybattle.authorize.model.User;
import ru.anisimov.keybattle.authorize.model.UserRole;
import ru.anisimov.keybattle.authorize.model.UserRoleType;
import ru.anisimov.keybattle.core.data.service.interfaces.UserHistoryService;
import ru.anisimov.keybattle.core.model.history.HistoryAction;

import java.util.List;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         10/31/14
 */
@Component
public class UserManager {
	@Autowired
	private UserService userService;
	@Autowired
	private UserHistoryService userHistoryService;


	public List<UserRole> getUserRoles(Long userId, Boolean enabled) {
		return userService.getUserRoles(userId, enabled);
	}

	public Long addUser(Registration user, long actorId) {
        Long userId = userService.addUser(user);
        if (
                userId != null
                && userHistoryService.addRecord(userId, HistoryAction.USER_ADDED, actorId, "registration")
                && userService.addRole(userId, UserRoleType.USER)
                && userHistoryService.addRecord(userId, HistoryAction.USER_ROLE_ADDED, actorId, "registration")
            ) {
            return userId;
        }
        return null;
	}

	public boolean addUserRole(Long userId, UserRoleType role, long actorId, String comment) {
		return userService.addRole(userId, role)
                && userHistoryService.addRecord(userId, HistoryAction.USER_ROLE_ADDED, actorId, comment);
	}

	public boolean removeUserRole(long userId, UserRoleType role, long actorId, String comment) {
		return userService.removeRole(userId, role)
                && userHistoryService.addRecord(userId, HistoryAction.USER_ROLE_REMOVED, actorId, comment);
	}

	public boolean lockUser(long userId, long actorId, String comment) {
		return userService.lockUser(userId, true)
                && userHistoryService.addRecord(userId, HistoryAction.USER_LOCKED, actorId, comment);
	}

	public boolean unlockUser(long userId, long actorId, String comment) {
		return userService.lockUser(userId, false)
                && userHistoryService.addRecord(userId, HistoryAction.USER_UNLOCKED, actorId, comment);
	}

	public boolean disableUser(long userId, long actorId, String comment) {
		return userService.enableUser(userId, false)
                && userHistoryService.addRecord(userId, HistoryAction.USER_DISABLED, actorId, comment);
	}

	public boolean enableUser(long userId, long actorId, String comment) {
		return userService.enableUser(userId, true)
                && userHistoryService.addRecord(userId, HistoryAction.USER_ENABLED, actorId, comment);
	}

	public List<User> getUsers(Boolean withActiveRoles, Boolean locked, Boolean enabled) {
		return userService.getUsers(withActiveRoles, locked, enabled);
	}

	public List<User> getUsers(UserRoleType role) {
		return userService.getUsers(role);
	}

	public User getUserById(long userId) {
		return userService.getUserById(userId);
	}

	public User getUserByUserName(String userName) {
		return userService.getUserByUserName(userName);
	}

	public User getUserByEmail(String email) {
		return userService.getUserByEmail(email);
	}
}
