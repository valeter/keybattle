package ru.anisimov.keybattle.authorize.model;

import org.springframework.security.core.GrantedAuthority;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         10/10/14
 */
public class User extends org.springframework.security.core.userdetails.User {
	private long id;

	public User(long id) {
		super(String.valueOf(id), String.valueOf(id), Collections.emptyList());
		this.id = id;
	}

	public User(long id, String username, String password, boolean locked, boolean enabled, Collection<? extends GrantedAuthority> authorities) {
		super(username, password, enabled, true, true, !locked, authorities);
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	private Object[] keyArray() {
		return new Object[]{getId(), getUsername()};
	}
	
	@Override
	public int hashCode() {
		return Arrays.hashCode(keyArray());
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof User)) {
			return false;
		}
	
		User that = (User) obj;
		return Arrays.deepEquals(this.keyArray(), that.keyArray());
	}
	
	@Override
	public String toString() {
		return "User: " +
                Arrays.toString(keyArray());
	}
}
