package ru.anisimov.keybattle.core.model.text;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         20.09.15
 */
public class DamerauLevensteinTest {
	@Test
	public void testDistance() throws Exception {
		assertEquals(0, DamerauLevenstein.distance("a", "a"));
		assertEquals(1, DamerauLevenstein.distance("a", "b"));
		assertEquals(2, DamerauLevenstein.distance("", "aa"));
		assertEquals(2, DamerauLevenstein.distance("aa", ""));
		assertEquals(0, DamerauLevenstein.distance("", ""));
		assertEquals(2, DamerauLevenstein.distance("", "aa"));
		assertEquals(1, DamerauLevenstein.distance("abc", "adc"));
		assertEquals(1, DamerauLevenstein.distance("abc", "ac"));
		assertEquals(1, DamerauLevenstein.distance("bac", "abc"));
		assertEquals(1, DamerauLevenstein.distance("abc", "abdc"));
	}
}
