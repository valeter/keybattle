package ru.anisimov.keybattle.core.model.web;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         11/2/14
 */
public class WebActionResult {
    public static final WebActionResult SUCCESS = new WebActionResult(WebActionStatus.SUCCESS);
    public static final WebActionResult FAIL = new WebActionResult(WebActionStatus.FAIL);
    public static final WebActionResult ERROR = new WebActionResult(WebActionStatus.ERROR);
    
    private Integer status;
    private Map<Integer, String> params = new HashMap<>();

    public WebActionResult() {
    }

    public WebActionResult(WebActionStatus status) {
        this.status = status.ordinal();
    }
    
    public Integer getStatus() {
        return status;
    }

    public void setStatus(WebActionStatus status) {
        this.status = status.ordinal();
    }

    public Map<Integer, String> getParams() {
        return params;
    }

    public void setParams(Map<Integer, String> params) {
        this.params = params;
    }

    public WebActionResult addParam(WebActionParam name, WebActionParamValue value) {
        params.put(name.ordinal(), String.valueOf(value.ordinal()));
        return this;
    }

    public WebActionResult addParam(WebActionParam name, String value) {
        params.put(name.ordinal(), value);
        return this;
    }
}
