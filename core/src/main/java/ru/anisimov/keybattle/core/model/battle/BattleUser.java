package ru.anisimov.keybattle.core.model.battle;

import ru.anisimov.keybattle.core.model.user.User;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         24.02.16
 */
public class BattleUser {
	private User user;

	public BattleUser(User user) {
		this.user = user;
	}
	
	public void notify(BattleEvent event) {
		
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
