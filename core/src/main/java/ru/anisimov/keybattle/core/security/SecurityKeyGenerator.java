package ru.anisimov.keybattle.core.security;

import org.springframework.stereotype.Component;

import java.security.SecureRandom;
import java.util.*;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         14.04.15
 */
@Component
public class SecurityKeyGenerator {
	private static final Random RND = new SecureRandom();

	private static final int MIN_LENGTH = 20;
	private static final int MAX_LENGTH = 30;

	private static final String[] categories = new String[] {
			"abcdefghijklmnopqrstuvwxyz",
			"ABCDEFGHIJKLMNOPQRSTUVWXYZ",
			"0123456789",
			",/?<>\\|'\";:[{]}=+&^%$#@±§`"
	};

	private static final int MIN_CATEGORY_COUNT = MIN_LENGTH / (categories.length + 1);

	public String generate() {
		int length = RND.nextInt(MAX_LENGTH - MIN_LENGTH + 1) + MIN_LENGTH;

		List<Integer> positions = new ArrayList<>();
		for (int i = 0; i < length; i++) {
			positions.add(i);
		}
		Collections.shuffle(positions, RND);

		Map<Integer, Character> resultMap = new TreeMap<>();

		// first - static number of characters from each category
		int posInd = 0;
		for (String category : categories) {
			for (int i = 0; i < MIN_CATEGORY_COUNT; i++) {
				resultMap.put(positions.get(posInd), getRandomChar(category));
				posInd++;
			}
		}

		// after that - characters from random category
		for (; posInd < length; posInd++) {
			resultMap.put(positions.get(posInd),
					getRandomChar(categories[RND.nextInt(categories.length)]));
		}

		StringBuilder result = new StringBuilder();
		for (Integer position : resultMap.keySet()) {
			result.append(resultMap.get(position));
		}
		return result.toString();
	}

	private char getRandomChar(String string) {
		return string.charAt(RND.nextInt(string.length()));
	}
}
