package ru.anisimov.keybattle.core.model.history;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Arrays;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         10/31/14
 */
@Entity
@Table(name = "USER_HISTORY")
public class UserHistoryRecord {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private long id;
	
	@Column(name = "USER_ID", nullable = false)
	private long userId;
	
	@Column(name = "HISTORY_ACTION_ID", nullable = false)
	@Enumerated(EnumType.ORDINAL)
	private HistoryAction action;
	
	@Column(name = "CREATION_TIME", nullable = false)
	private LocalDateTime creationTime;

	@Column(name = "ACTOR_ID", nullable = false)
	private long actorId;

	@Column(name = "COMMENT", nullable = false, length = 300)
	private String comment;

	public UserHistoryRecord() {
	}

	public UserHistoryRecord(long userId, HistoryAction action, LocalDateTime creationTime, long actorId, String comment) {
		this.userId = userId;
		this.action = action;
		this.creationTime = creationTime;
		this.actorId = actorId;
		this.comment = comment;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public HistoryAction getAction() {
		return action;
	}

	public void setAction(HistoryAction action) {
		this.action = action;
	}

	public LocalDateTime getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(LocalDateTime creationTime) {
		this.creationTime = creationTime;
	}

	public long getActorId() {
		return actorId;
	}

	public void setActorId(long actorId) {
		this.actorId = actorId;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	private Object[] keyArray() {
		return new Object[]{id};
	}
	
	@Override
	public int hashCode() {
		return Arrays.hashCode(keyArray());
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof UserHistoryRecord)) {
			return false;
		}
	
		UserHistoryRecord that = (UserHistoryRecord) obj;
		return Arrays.equals(this.keyArray(), that.keyArray());
	}

	@Override
	public String toString() {
		return "UserHistoryRecord{" +
				"id=" + id +
				", userId=" + userId +
				", action=" + action +
				", creationTime=" + creationTime +
				", actorId=" + actorId +
				", comment='" + comment + '\'' +
				'}';
	}
}
