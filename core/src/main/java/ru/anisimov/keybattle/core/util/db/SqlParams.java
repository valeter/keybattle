package ru.anisimov.keybattle.core.util.db;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         03.05.15
 */
public class SqlParams {
	private Map<String, Object> whereParams;

	public SqlParams() {
		whereParams = new HashMap<>();
	}

	private SqlParams(String key, Object value) {
		this.whereParams = new HashMap<>();
		this.put(key, value);
	}

	public SqlParams(Map<String, Object> whereParams) {
		this.whereParams = whereParams;
	}

	public static SqlParams empty() {
		return new SqlParams();
	}
	
	public static SqlParams putFirst(String key, Object value) {
		return new SqlParams(key, value);
	}
	
	public int size() {
		return whereParams.size();
	}

	public Object get(Object key) {
		return whereParams.get(key);
	}

	public Set<String> keySet() {
		return whereParams.keySet();
	}
	
	public Object[] values() {
		return whereParams.values().toArray();
	}

	public SqlParams put(String key, Object value) {
		whereParams.put(key, value);
		return this;
	}

	public SqlParams putAll(Map<? extends String, ?> m) {
		whereParams.putAll(m);
		return this;
	}
}
