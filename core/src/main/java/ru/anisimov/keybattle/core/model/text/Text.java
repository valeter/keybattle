package ru.anisimov.keybattle.core.model.text;

import ru.anisimov.keybattle.core.exception.WordCheckException;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         21.04.15
 */
public class Text extends TextInfo {
	private String[] words;
	private TextComplexityClass complexityClass;
	
	private Text(TextInfo textInfo, String text) {
		super(textInfo);
		this.words = text.split("\\s+");
	}
	
	public static Text fromTextInfo(TextInfo textInfo, String text) {
		Text result = new Text(textInfo, text);
		result.complexityClass = TextComplexityClass.getComplexity(result);
		return result;
	}

	public String[] getWords() {
		return words;
	}

	public void setWords(String[] words) {
		this.words = words;
	}

	public TextComplexityClass getComplexityClass() {
		return complexityClass;
	}

	public int wordCount() {
		return words.length;
	}
	
	public int checkWord(int i, String word) throws WordCheckException {
		if (i < 0 || i >= words.length) {
			throw new WordCheckException();
		}
		
		return DamerauLevenstein.distance(words[i], word);
	}
}
