package ru.anisimov.keybattle.core.model.web;

import ru.anisimov.keybattle.core.HasDictionary;
import ru.anisimov.keybattle.core.model.locale.DictionaryType;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         11/2/14
 */
public enum WebActionStatus implements HasDictionary {
    SUCCESS,
    FAIL,
    ERROR;

    @Override
    public DictionaryType getDictionaryType() {
        return DictionaryType.WEB_ACTION_STATUS;
    }
}
