package ru.anisimov.keybattle.core.model.user;

import ru.anisimov.keybattle.core.util.ImageProcessor;

import javax.persistence.*;
import java.awt.image.BufferedImage;
import java.time.LocalDateTime;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         08.05.15
 */
@Entity
@Table(name = "USER_IMAGES")
public class UserImage {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private long ID;
	
	@Column(name = "USER_ID", nullable = false)
	private long userId;

	@Column(name = "TYPE_ID", nullable = false)
	@Enumerated(EnumType.ORDINAL)
	private UserImageType type;
	
	@Lob
	@Column(name = "IMAGE", nullable = false)
	private byte[] image;
	
	@Column(name = "CREATION_TIME", nullable = false, insertable = false, updatable = false)
	private LocalDateTime creationTime;

	@Column(name = "MODIFICATION_TIME", nullable = false, insertable = false, updatable = false)
	private LocalDateTime modificationTime;

	public UserImage() {
	}

	public UserImage(long userId, UserImageType type, BufferedImage image) {
		this.userId = userId;
		this.type = type;
        this.image = ImageProcessor.scaleAndCrop(
                image,
                type.getWidth(),
                type.getHeight(),
                type.getFormat()
        );
	}

	public long getID() {
		return ID;
	}

	public void setID(long ID) {
		this.ID = ID;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public UserImageType getType() {
		return type;
	}

	public void setType(UserImageType type) {
		this.type = type;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public LocalDateTime getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(LocalDateTime creationTime) {
		this.creationTime = creationTime;
	}

	public LocalDateTime getModificationTime() {
		return modificationTime;
	}

	public void setModificationTime(LocalDateTime modificationTime) {
		this.modificationTime = modificationTime;
	}
}
