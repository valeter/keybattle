package ru.anisimov.keybattle.core.model.websocket;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         03.10.15
 */
public interface WebsocketEventType {
	// common
	String ERROR = "ERROR";
	
	// user
	String USER_LEAVE = "USER_LEAVE";
	String USER_JOIN = "USER_JOIN";
	String USER_LIST = "USER_LIST";
	
	// battle
	String BATTLE_STARTED = "BATTLE_STARTED";
}
