package ru.anisimov.keybattle.core;

import ru.anisimov.keybattle.core.model.locale.DictionaryType;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         11/2/14
 */
public interface HasDictionary {
    DictionaryType getDictionaryType();
}
