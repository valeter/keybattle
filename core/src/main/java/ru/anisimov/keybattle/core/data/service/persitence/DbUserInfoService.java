package ru.anisimov.keybattle.core.data.service.persitence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import ru.anisimov.keybattle.core.data.service.interfaces.UserInfoService;
import ru.anisimov.keybattle.core.model.user.Gender;
import ru.anisimov.keybattle.core.model.user.UserInfo;
import ru.anisimov.keybattle.core.model.user.UserRoleType;

import java.sql.Date;
import java.util.List;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         11/1/14
 */
@Service("userInfoService")
public class DbUserInfoService implements UserInfoService {
	private static final RowMapper<UserInfo> USER_INFO_ROW_MAPPER = (rs, rowNum) -> {
		UserInfo result = new UserInfo();
		result.setUserId(rs.getLong("USER_ID"));
		result.setEmail(rs.getString("EMAIL"));
		result.setUserName(rs.getString("USERNAME"));
		result.setName(rs.getString("NAME"));
		Integer age = rs.getObject("AGE", Integer.class);
		result.setAge(rs.wasNull() ? null : age);
		result.setGender(Gender.values()[rs.getInt("GENDER_ID")]);
		Date date = rs.getDate("DOB");
		result.setDateOfBirth(date == null ? null : date.toLocalDate());
		Integer country = rs.getInt("COUNTRY_ID");
		result.setCountryId(rs.wasNull() ? null : country);
		result.setStatus(rs.getString("STATUS"));
		result.setHidden(rs.getInt("HIDDEN") == 1);
		result.setEmailHidden(rs.getInt("EMAIL_HIDDEN") == 1);
		result.setHasAvatar(rs.getInt("HAS_AVATAR") == 1);
		return result;
	};

	private static final String SELECT_INFOS
			= "SELECT * FROM V_USER_INFO";

	private static final String SELECT_INFOS_BY_ROLE
			= "SELECT UI.* FROM V_USER_INFO UI WHERE UI.USER_ID IN " +
			" (SELECT USER_ID FROM USER_ROLE WHERE TYPE_ID = ?)";

	private static final String CHANGE_USER_STATUS 
			= "UPDATE USER_INFO SET STATUS = ? WHERE USER_ID = ?";
	
	private static final String GET_STATUS 
			= "SELECT STATUS FROM USER_INFO WHERE USER_ID = ?";
	
	private static final String SELECT_INFO = "SELECT * FROM V_USER_INFO WHERE USER_ID = ?";

	private static final String SHOW_USER_INFO = "UPDATE USER_INFO SET " +
			" HIDDEN = ? WHERE USER_ID = ?";

	private static final String SHOW_USER_EMAIL = "UPDATE USER_INFO SET " +
			" EMAIL_HIDDEN = ? WHERE USER_ID = ?";

	private static final String CHANGE_USER_INFO = "UPDATE USER_INFO SET " +
			" NAME = ?, GENDER_ID = ?, DOB = ?, COUNTRY_ID = ?, HIDDEN = ?, EMAIL_HIDDEN = ? WHERE USER_ID = ?";

	private static final String CLEAR_USER_INFO = "UPDATE USER_INFO SET " +
			" NAME = NULL, GENDER_ID = 0, DOB = NULL, COUNTRY_ID = NULL, HIDDEN = 1, EMAIL_HIDDEN = 1 " +
			" WHERE USER_ID = ?";

	private static final String ADD_INITIAL_INFO =
			"INSERT INTO USER_INFO (USER_ID, EMAIL) VALUES (?, ?)";

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public boolean addInitialUserInfo(long userId, String email) {
		return jdbcTemplate.update(ADD_INITIAL_INFO, userId, email) != 0;
	}
	
	@Override
	public List<UserInfo> getUserInfos() {
		return jdbcTemplate.query(SELECT_INFOS, USER_INFO_ROW_MAPPER);
	}

	@Override
	public List<UserInfo> getUserInfos(UserRoleType role) {
		return jdbcTemplate.query(SELECT_INFOS_BY_ROLE, USER_INFO_ROW_MAPPER, role.ordinal());
	}

	@Override
	public String getUserStatus(long userId) {
		return jdbcTemplate.queryForObject(GET_STATUS, String.class, userId);
	}

	@Override
	public boolean changeUserStatus(long userId, String status) {
		return jdbcTemplate.update(CHANGE_USER_STATUS, status, userId) != 0;
	}

	@Override
	public boolean clearUserInfo(long userId) {
		return jdbcTemplate.update(CLEAR_USER_INFO, userId) != 0;
	}

	@Override
	public boolean changeUserInfo(long userId, UserInfo userInfo) {
		return jdbcTemplate.update(CHANGE_USER_INFO,
				nullIfEmpty(userInfo.getName()), userInfo.getGender().ordinal(), 
				userInfo.getDateOfBirth() == null ? null : Date.valueOf(userInfo.getDateOfBirth()), 
				userInfo.getCountryId(), userInfo.isHidden() ? 1 : 0,
				userInfo.isEmailHidden() ? 1 : 0, userId) != 0;
	}
	
	private String nullIfEmpty(String string) {
		return string == null 
				? null
				: (
					string.length() == 0 
						? null
						: string	
				);
	}

	@Override
	public boolean showUserInfo(long userId, boolean visible) {
		return jdbcTemplate.update(SHOW_USER_INFO, 
				visible ? 0: 1, userId) != 0;
	}

	@Override
	public UserInfo getUserInfo(long userId) {
		List<UserInfo> infos = jdbcTemplate.query(SELECT_INFO, 
				USER_INFO_ROW_MAPPER, userId);
		return infos == null || infos.size() == 0 ? null : infos.get(0);
	}
	
	@Override
	public boolean showUserEmail(long userId, boolean visible) {
		return jdbcTemplate.update(SHOW_USER_EMAIL,
				visible ? 0: 1, userId) != 0;
	}
}
