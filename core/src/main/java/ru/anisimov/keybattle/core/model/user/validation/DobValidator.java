package ru.anisimov.keybattle.core.model.user.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         31.01.15
 */
public class DobValidator implements ConstraintValidator<Dob, LocalDate> {
	private static final int MIN_AGE = 3;
	private static final int MAX_AGE = 120;
	
	@Override
	public void initialize(Dob constraintAnnotation) {
	}

	@Override
	public boolean isValid(LocalDate value, ConstraintValidatorContext context) {
		if (value == null) {
			return true;
		}
		
		LocalDate today = LocalDate.now();
		long age = ChronoUnit.YEARS.between(value, today);
		return age >= MIN_AGE && age <= MAX_AGE;
	}
}

