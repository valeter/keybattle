package ru.anisimov.keybattle.core.data.service.interfaces;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.anisimov.keybattle.core.model.user.UserImage;
import ru.anisimov.keybattle.core.model.user.UserImageType;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         08.05.15
 */
@Repository
public interface UserImageService extends CrudRepository<UserImage, Long> {
	UserImage findByUserIdAndType(long userId, UserImageType type);
	void deleteByUserId(long userId);
}
