package ru.anisimov.keybattle.core.model.history;

import ru.anisimov.keybattle.core.HasDictionary;
import ru.anisimov.keybattle.core.model.locale.DictionaryType;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         10/31/14
 */
public enum HistoryAction implements HasDictionary {
	USER_ADDED,
	USER_ROLE_ADDED,
	USER_ROLE_REMOVED,
	USER_LOCKED,
	USER_UNLOCKED,
	USER_DISABLED,
	USER_ENABLED,
	USER_INFO_ADDED,
	USER_INFO_CHANGED,
	USER_INFO_HIDDEN,
	USER_INFO_SHOWN,
	USER_PASSWORD_CHANGED,
	USER_AVATAR_CHANGED,
	USER_SETTINGS_CHANGED;

	@Override
	public DictionaryType getDictionaryType() {
		return DictionaryType.HISTORY_ACTION_TYPE;
	}
}
