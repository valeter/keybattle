package ru.anisimov.keybattle.core.model.mail;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         11.04.15
 */
public enum Sender {
	NOREPLY, 
	ADMIN,
	INFO,
	HELP
}
