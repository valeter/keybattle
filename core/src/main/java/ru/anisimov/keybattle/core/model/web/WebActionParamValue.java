package ru.anisimov.keybattle.core.model.web;

import ru.anisimov.keybattle.core.HasDictionary;
import ru.anisimov.keybattle.core.model.locale.DictionaryType;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         11/2/14
 */
public enum WebActionParamValue implements HasDictionary {
    UNKNOWN_EXCEPTION,
    SERVER_ERROR,
    EMPTY_FILE,
    NOT_AUTHENTICATED,
    UNKNOWN_ROLE,
    WRONG_FILE_TYPE;

    @Override
    public DictionaryType getDictionaryType() {
        return DictionaryType.WEB_ACTION_PARAM_VALUE;
    }
}
