package ru.anisimov.keybattle.core.exception;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         10/28/14
 */
public enum ApplicationErrorType {
	UNKNOWN,
	ALERT
}
