package ru.anisimov.keybattle.core.model.text;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         20.09.15
 */
public class DamerauLevenstein {
	public DamerauLevenstein() {
		throw new UnsupportedOperationException();
	}

	private static int min(int a, int b, int c) {
		return Math.min(Math.min(a, b), c);
	}

	public static int distance(String s1, String s2) {
		if (s1.length() == 0 || s2.length() == 0) {
			return Math.max(s1.length(), s2.length());
		}

		int[][] d = new int[s1.length()][s2.length()];
		
		if (s1.charAt(0) != s2.charAt(0)) {
			d[0][0] = 1;
		}
		
		Map<Character, Integer> indexes = new HashMap<>();
		indexes.put(s1.charAt(0), 0);
		
		for (int i = 1; i < s1.length(); i++) {
			d[i][0] = min(d[i - 1][0] + 1, i + 2, i + (s1.charAt(i) == s2.charAt(0) ? 0 : 1));
		}
		for (int j = 1; j < s2.length(); j++) {
			d[0][j] = min(j + 2, d[0][j - 1] + 1, j + (s1.charAt(0) == s2.charAt(j) ? 0 : 1));
		}
		
		for (int i = 1; i < s1.length(); i++) {
			int maxInd = s1.charAt(i) == s2.charAt(0) ? 0
					: -1;
			for (int j = 1; j < s2.length(); j++) {
				Integer tmpSwapInd = indexes.get(s2.charAt(j));
				int firstSwapInd = maxInd;
				int matchCost = d[i - 1][j - 1];
				if (s1.charAt(i) != s2.charAt(j)) {
					matchCost += 1;
				} else {
					maxInd = j;
				}
				int swapCost;
				if (tmpSwapInd != null && firstSwapInd != -1) {
					int tmpSwapCost = (tmpSwapInd == 0 && firstSwapInd == 0) 
							? 0 
							: d[Math.max(0, tmpSwapInd - 1)][Math.max(0, firstSwapInd - 1)];

					swapCost = tmpSwapCost + (i - tmpSwapInd - 1) + (j - firstSwapInd - 1) + 1;
				} else {
					swapCost = Integer.MAX_VALUE;
				}
				d[i][j] = Math.min(min(d[i - 1][j] + 1, d[i][j - 1] + 1, matchCost), swapCost);
			}
			indexes.put(s1.charAt(i), i);
		}
		
		return d[s1.length() - 1][s2.length() - 1];
	}
}
