package ru.anisimov.keybattle.core;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         11/2/14
 */
public enum SortOrder {
    KEY,
    VALUE
}
