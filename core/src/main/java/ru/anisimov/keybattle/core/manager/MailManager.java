package ru.anisimov.keybattle.core.manager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;
import ru.anisimov.keybattle.core.model.mail.Message;
import ru.anisimov.keybattle.core.model.mail.MessageBuilder;
import ru.anisimov.keybattle.core.model.mail.MessageTemplate;
import ru.anisimov.keybattle.core.model.mail.Sender;
import ru.anisimov.keybattle.core.remote.RemoteMailService;
import ru.anisimov.keybattle.core.config.constants.Params;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Locale;
import java.util.Map;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         21.03.15
 */
@Component
public class MailManager {
	private static final String CONTENT_TEMPLATE_SUFFIX = "-content";
	private static final String SUBJECT_TEMPLATE_SUFFIX = "-subject";
	
	@Autowired
	private RemoteMailService remoteMailService;
	@Autowired
	private SpringTemplateEngine springTemplateEngine; 
	
	public boolean send(Message message) {
		return remoteMailService.sendMessage(
				message.getSender(),
				getTemplateSubject(message.getTemplate(), message.getLocale(), message.getVariables()), 
				getTemplateContent(message.getTemplate(), message.getLocale(), message.getVariables()),
				message.getTo(), message.getCc());
	}
	
	public boolean sendRegistrationConfirm(String to, Long id, String sk, Locale locale) {
		try {
			return send(
					new MessageBuilder()
							.sender(Sender.NOREPLY)
							.locale(locale)
							.template(MessageTemplate.REGISTRATION_CONFIRM)
							.to(to)
							.withParam(Params.SK, URLEncoder.encode(sk, "UTF-8"))
							.withParam(Params.ENTITY_ID, id)
							.build()
			);
		} catch (UnsupportedEncodingException e) {
			// do nothing
		}
		return false;
	}

	private String getTemplateSubject(MessageTemplate template, Locale locale, Map<String, Object> variables) {
		return getTemplateString(template, SUBJECT_TEMPLATE_SUFFIX, locale, variables);
	}
	
	private String getTemplateContent(MessageTemplate template, Locale locale, Map<String, Object> variables) {
		return getTemplateString(template, CONTENT_TEMPLATE_SUFFIX, locale, variables);
	}
	
	private String getTemplateString(MessageTemplate template, String suffix, Locale locale, Map<String, Object> variables) {
		return springTemplateEngine.process(template.getPath() + suffix, new Context(locale, variables));
	}
}
