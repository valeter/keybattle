package ru.anisimov.keybattle.core.model.mail;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         11.04.15
 */
public class Message {
	private Sender sender;
	private MessageTemplate template;
	private Locale locale;
	private Map<String, Object> variables = new HashMap<>();
	private String to;
	private String[] cc = new String[0];

	public Message() {
	}

	public Message(Sender sender, MessageTemplate template, Locale locale, Map<String, Object> variables, String to, String[] cc) {
		this.sender = sender;
		this.template = template;
		this.locale = locale;
		this.variables = variables;
		this.to = to;
		this.cc = cc;
	}

	public Sender getSender() {
		return sender;
	}

	public void setSender(Sender sender) {
		this.sender = sender;
	}

	public MessageTemplate getTemplate() {
		return template;
	}

	public void setTemplate(MessageTemplate template) {
		this.template = template;
	}

	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	public Map<String, Object> getVariables() {
		return variables;
	}

	public void setVariables(Map<String, Object> variables) {
		this.variables = variables;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String[] getCc() {
		return cc;
	}

	public void setCc(String[] cc) {
		this.cc = cc;
	}
}
