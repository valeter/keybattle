package ru.anisimov.keybattle.core.data.service.persitence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;
import ru.anisimov.keybattle.core.data.service.interfaces.TextService;
import ru.anisimov.keybattle.core.model.locale.DictionaryLocale;
import ru.anisimov.keybattle.core.model.text.ModState;
import ru.anisimov.keybattle.core.model.text.Text;
import ru.anisimov.keybattle.core.model.text.TextDetails;
import ru.anisimov.keybattle.core.model.text.TextInfo;
import ru.anisimov.keybattle.core.util.db.Pagination;
import ru.anisimov.keybattle.core.util.db.SqlParams;

import javax.annotation.PostConstruct;
import java.sql.PreparedStatement;
import java.sql.Statement;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         30.04.15
 */
@Service("textService")
public class DbTextService implements TextService {
	private static final RowMapper<TextDetails> TEXT_DETAILS_ROW_MAPPER = (rs, rowNum) -> {
		return null;
	};
	
	private static final RowMapper<TextInfo> TEXT_INFO_ROW_MAPPER = (rs, rowNum) -> {
		TextInfo result = new TextInfo();
		result.setId(rs.getLong("ID"));
		result.setAuthorId(rs.getLong("AUTHOR_ID"));
		result.setLocale(DictionaryLocale.values()[rs.getInt("LOCALE_ID")]);
		result.setModState(ModState.values()[rs.getInt("MOD_STATE")]);
		result.setCreationTime(rs.getTimestamp("CREATION_TIME").toLocalDateTime());
		result.setModificationTime(rs.getTimestamp("MODIFICATION_TIME").toLocalDateTime());
		return result;
	};

	private static final RowMapper<Text> TEXT_ROW_MAPPER = (rs, rowNum) -> {
		TextInfo textInfo = TEXT_INFO_ROW_MAPPER.mapRow(rs, rowNum);
		return Text.fromTextInfo(textInfo, rs.getString("TEXT_VALUE"));
	};
	
	private static final String ADD_TEXT_INFO = "INSERT INTO TEXT_INDEX (AUTHOR_ID, LOCALE_ID) VALUES (?, ?)";
	private static final String ADD_TEXT_VALUE = "INSERT INTO TEXT (ID, TEXT_VALUE) VALUES (?, ?)";
	private static final String CHANGE_TEXT_VALUE = "UPDATE TEXT SET TEXT_VALUE = ? WHERE ID = ?";
	private static final String REMOVE_TEXT_INFO = "DELETE FROM TEXT_INDEX WHERE ID = ?";
	private static final String REMOVE_TEXT_VALUE = "DELETE FROM TEXT WHERE ID = ?";
	private static final String MODERATE_TEXT = "UPDATE TEXT_INDEX SET MOD_STATE = ? WHERE ID = ?";
	private static final String GET_TEXT_BY_ID = "SELECT TI.*, T.TEXT_VALUE FROM " +
			"TEXT_INDEX TI JOIN TEXT T ON (TI.ID = T.ID) WHERE TI.ID = ?";
	private static final String GET_TEXT_RANDOM = "SELECT TI.*, T.TEXT_VALUE FROM " +
			"(SELECT * FROM TEXT_INDEX WHERE LOCALE_ID = ? AND MOD_STATE = ? ORDER BY RAND() LIMIT 1) TI JOIN TEXT T ON (TI.ID = T.ID)";
	private static final String GET_TEXT_INFO_BY_ID = "SELECT TI.* FROM TEXT_INDEX TI WHERE TI.ID = ?";
	private static final String GET_TEXT_VALUE_BY_ID = "SELECT TEXT_VALUE FROM TEXT WHERE ID = ?";
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private Pagination<TextInfo> TEXT_INFO_PAGINATION;
	
	@PostConstruct
	private void init() {
		TEXT_INFO_PAGINATION = new Pagination.Builder<TextInfo>(jdbcTemplate)
				.table("TEXT_INDEX")
				.rowMapper(TEXT_INFO_ROW_MAPPER)
				.build();
	}
	
	@Override
	public Long addTextInfo(TextInfo textInfo) {
		KeyHolder result = new GeneratedKeyHolder();
		if (jdbcTemplate.update(con -> {
				PreparedStatement query = con.prepareStatement(ADD_TEXT_INFO, Statement.RETURN_GENERATED_KEYS);
				query.setLong(1, textInfo.getAuthorId());
				query.setLong(2, textInfo.getLocale().ordinal());
				return query;
			}, result) != 0) {
			return result.getKey().longValue();
		}
		return null;
	}

	@Override
	public boolean addTextValue(long textId, String text) {
		return jdbcTemplate.update(ADD_TEXT_VALUE, textId, text) != 0;
	}

	@Override
	public boolean changeTextValue(long oldTextId, String text) {
		return jdbcTemplate.update(CHANGE_TEXT_VALUE, text, oldTextId) != 0;
	}

	@Override
	public boolean removeTextInfo(long textId) {
		return jdbcTemplate.update(REMOVE_TEXT_INFO, textId) != 0;
	}
	
	@Override
	public boolean removeTextValue(long textId) {
		return jdbcTemplate.update(REMOVE_TEXT_VALUE, textId) != 0;
	}

	@Override
	public boolean moderateText(long textId, ModState modState) {
		return jdbcTemplate.update(MODERATE_TEXT, modState.ordinal(), textId) != 0;
	}

	@Override
	public Text getText(long textId) {
		return jdbcTemplate.queryForObject(GET_TEXT_BY_ID, TEXT_ROW_MAPPER, textId);
	}

	@Override
	public Text getRandomText(DictionaryLocale locale, ModState modState) {
		return jdbcTemplate.queryForObject(GET_TEXT_RANDOM, TEXT_ROW_MAPPER, locale.ordinal(), modState.ordinal());
	}

	@Override
	public TextInfo getTextInfo(long textId) {
		return jdbcTemplate.queryForObject(GET_TEXT_INFO_BY_ID, TEXT_INFO_ROW_MAPPER, textId);
	}

	@Override
	public Page<TextInfo> getTextInfos(ModState modState, Pageable pageable) {
		return TEXT_INFO_PAGINATION.getPage(pageable, SqlParams.putFirst("MOD_STATE", modState.ordinal()));
	}
	
	@Override
	public Page<TextInfo> getTextInfos(DictionaryLocale locale, ModState modState, Pageable pageable) {
		return TEXT_INFO_PAGINATION.getPage(pageable, SqlParams
				.putFirst("LOCALE_ID", locale.ordinal())
				.put("MOD_STATE", modState.ordinal()));
	}

	@Override
	public Page<TextInfo> getTextInfos(long authorId, Pageable pageable) {
		return TEXT_INFO_PAGINATION.getPage(pageable, SqlParams.putFirst("AUTHOR_ID", authorId));
	}

	@Override
	public String getTextValue(long textId) {
		return jdbcTemplate.queryForObject(GET_TEXT_VALUE_BY_ID, String.class, textId);
	}
}
