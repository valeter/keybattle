package ru.anisimov.keybattle.core.model.battle;

import ru.anisimov.keybattle.core.exception.WordCheckException;
import ru.anisimov.keybattle.core.model.stat.BattleStat;
import ru.anisimov.keybattle.core.model.text.Text;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         19.09.15
 */
public class BattleInfoOld {
	private UUID id;
	private Map<Long, BattleStat> userStat;
	private Text text;
	private List<BattleStat> usersLeft;
	private int usersFinished = 0;

	public BattleInfoOld(UUID id, Text text, BattleUser... users) {
		this.id = id;
		this.text = text;
		this.userStat = Arrays.stream(users).map((user) -> new BattleStat(id, user, text.getId()))
				.collect(Collectors.toMap(stat -> stat.getBattleUser().getUser().getId(), stat -> stat));
		usersLeft = new ArrayList<>();
	}
	
	public synchronized List<BattleUser> users() {
		return userStat.values().stream()
				.map(BattleStat::getBattleUser)
				.collect(Collectors.toList());
	}
	
	public synchronized boolean isFinished() {
		return userStat.values().stream().allMatch(BattleStat::isFinished);
	}
	
	public synchronized void changeUserSaveStat(long userIdOld, BattleUser userNew) {
		BattleStat stat = removeOldUser(userIdOld);
		if (stat != null) {
			stat.setBattleUser(userNew);
			userStat.put(userNew.getUser().getId(), stat);
		}

		notifyUsers();
	}
	
	public synchronized void changeUser(long userIdOld, BattleUser userNew) {
		if (removeOldUser(userIdOld) != null) {
			userStat.put(userNew.getUser().getId(), new BattleStat(id, userNew, text.getId()));
		}

		notifyUsers();
	}
	
	private BattleStat removeOldUser(long userIdOld) {
		LocalDateTime endTime = LocalDateTime.now();
		BattleStat stat = userStat.get(userIdOld);
		if (usersFinished >= userStat.size() || stat.isFinished()) {
			return null;
		}
		stat.setEndTime(endTime);
		usersLeft.add(stat);
		userStat.remove(userIdOld);
		return stat;
	}
	
	public synchronized void addWord(long userId, String word) {
		BattleStat stat;
		stat = userStat.get(userId);
		if (stat == null) {
			stat = usersLeft.stream().filter(s -> s.getBattleUser().getUser().getId() == userId).findFirst().get();
		}
		
		if (stat.isFinished()) {
			return;
		}

		int totalWords = stat.getTotalWords();
		try {
			int mistakes = text.checkWord(totalWords, word);
			stat.setTotalWords(++totalWords);
			stat.setTotalMistakes(stat.getTotalMistakes() + mistakes);
			if (totalWords == text.wordCount()) {
				finishBattle(stat);
			}
		} catch (WordCheckException e) {
			finishBattle(stat);
		}

		notifyUsers();
	}
	
	private void finishBattle(BattleStat stat) {
		LocalDateTime endTime = LocalDateTime.now();
		stat.setEndTime(endTime);
		stat.setPosition(++usersFinished);
		stat.setIsFinished(true);
	}
	
	private void notifyUsers(BattleEvent event) {
		for (BattleStat battleStat : usersLeft) {
			battleStat.getBattleUser().notify(event);
		}
	}

	public int getUsersFinished() {
		return usersFinished;
	}

	public void setUsersFinished(int usersFinished) {
		this.usersFinished = usersFinished;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public Map<Long, BattleStat> getUserStat() {
		return userStat;
	}

	public void setUserStat(Map<Long, BattleStat> userStat) {
		this.userStat = userStat;
	}

	public Text getText() {
		return text;
	}

	public void setText(Text text) {
		this.text = text;
	}

	public List<BattleStat> getUsersLeft() {
		return usersLeft;
	}

	public void setUsersLeft(List<BattleStat> usersLeft) {
		this.usersLeft = usersLeft;
	}
	
	private Object[] keyArray() {
		return new Object[]{id};
	}
	
	@Override
	public int hashCode() {
		return Arrays.hashCode(keyArray());
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof BattleInfoOld)) {
			return false;
		}
	
		BattleInfoOld that = (BattleInfoOld) obj;
		return Arrays.equals(this.keyArray(), that.keyArray());
	}
	
	@Override
	public String toString() {
		return "BattleInfo: " + Arrays.toString(keyArray());
	}
}
