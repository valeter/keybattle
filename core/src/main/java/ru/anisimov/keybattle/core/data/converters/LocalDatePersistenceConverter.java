package ru.anisimov.keybattle.core.data.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.sql.Date;
import java.time.LocalDate;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         19.03.15
 */
@Converter(autoApply = true)
public class LocalDatePersistenceConverter implements AttributeConverter<LocalDate, Date> {
	@Override
	public Date convertToDatabaseColumn(LocalDate entityValue) {
		return entityValue == null ? null : Date.valueOf(entityValue);
	}

	@Override
	public LocalDate convertToEntityAttribute(Date databaseValue) {
		return databaseValue == null ? null : databaseValue.toLocalDate();
	}
}
