package ru.anisimov.keybattle.core.util.db;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.RowMapper;

import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         03.05.15
 */
public abstract class Pagination<T> {
    JdbcOperations jdbcOperations;

    RowMapper<T> rowMapper;

    String[] columns;
    String table;

	protected Pagination(JdbcOperations jdbcOperations) {
		this.jdbcOperations = jdbcOperations;
	}
	
	public static <T> Builder<T> onMySQL(JdbcOperations jdbcOperations) {
        return new Builder<>(() -> new PaginationMySQL<>(jdbcOperations));
    } 

	public Page<T> getPage(Pageable pageable, SqlParams params) {
		List<T> resultList = jdbcOperations.query(buildSql(pageable, params), rowMapper, params.values());
		Long totalCount = jdbcOperations.queryForObject(buildCountSql(params), Long.class, params.values());
		return new PageImpl<>(resultList, pageable, totalCount);
	}

    public abstract String buildSql(Pageable pageable, SqlParams params);

	protected abstract String buildSortAndPaging(Pageable pageable);
	
	public static class Builder<T> {
		private Pagination<T> pagination;

		private Builder(Supplier<Pagination<T>> source) {
			this.pagination = source.get();
		}

		public Builder columns(String... columns) {
			pagination.columns = columns;
			return this;
		}

		public Builder table(String table) {
			pagination.table = table;
			return this;
		}
		
		public Builder rowMapper(RowMapper<T> rowMapper) {
			pagination.rowMapper = rowMapper;
			return this;
		}
		
		public Pagination<T> build() {
			return pagination;
		}
	}
}
