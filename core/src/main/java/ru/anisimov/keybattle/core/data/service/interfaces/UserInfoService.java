package ru.anisimov.keybattle.core.data.service.interfaces;

import ru.anisimov.keybattle.core.model.user.UserInfo;
import ru.anisimov.keybattle.core.model.user.UserRoleType;

import java.util.List;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         10/31/14
 */
public interface UserInfoService {
	boolean addInitialUserInfo(long userId, String email);

	boolean clearUserInfo(long userId);

	boolean changeUserInfo(long userId, UserInfo userInfo);

	boolean changeUserStatus(long userId, String status);

	boolean showUserInfo(long userId, boolean visible);

	boolean showUserEmail(long userId, boolean visible);

	List<UserInfo> getUserInfos();

	List<UserInfo> getUserInfos(UserRoleType role);

	UserInfo getUserInfo(long userId);

	String getUserStatus(long userId);
}
