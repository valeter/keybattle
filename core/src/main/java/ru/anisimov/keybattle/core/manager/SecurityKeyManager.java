package ru.anisimov.keybattle.core.manager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionTemplate;
import ru.anisimov.keybattle.core.HasTransactions;
import ru.anisimov.keybattle.core.security.SecurityKeyGenerator;
import ru.anisimov.keybattle.core.security.SecurityKeyTypeContext;
import ru.anisimov.keybattle.core.Pair;
import ru.anisimov.keybattle.core.data.service.interfaces.SecurityKeyService;
import ru.anisimov.keybattle.core.security.SecurityKeyType;

import java.util.function.LongPredicate;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         24.03.15
 */
@Component
public class SecurityKeyManager {
	private static final Logger log = LogManager.getLogger(SecurityKeyManager.class);

	@Autowired
	private TransactionTemplate transactionTemplate;
	@Autowired
	private SecurityKeyService securityKeyService;
	@Autowired
	private SecurityKeyGenerator securityKeyGenerator;
	@Autowired
	private SecurityKeyTypeContext securityKeyTypeContext;
	

	public Pair<Long, String> create(long sourceId, SecurityKeyType keyType) {
		return getInTransactionWithoutException(() -> {
					String key = securityKeyGenerator.generate();
					Long id = securityKeyService.storeKey(sourceId, keyType, key);
					return new Pair<>(id, key);
				},
				String.format("Error while creating sk for sourceId %d of type %s", sourceId, keyType.name())
		);
	}
	
	public boolean check(SecurityKeyType keyType, long id, String key, LongPredicate callback) {
		return doInTransactionWithoutException(() -> {
					Long sourceId = securityKeyService.checkKey(id, keyType, key);
					return sourceId != null 
							&& keyType.process(sourceId, securityKeyTypeContext) 
							&& callback.test(sourceId)
							&& securityKeyService.markDeleted(sourceId, keyType);
				},
				String.format("Error while checkAndDelete sk with id %d", id)
		);
	}
	
	public void clear() {
		doInTransactionWithoutException(() -> { 
			securityKeyService.clear(); 
			return true; 
		}, "Could not clear security keys");
	}
}
