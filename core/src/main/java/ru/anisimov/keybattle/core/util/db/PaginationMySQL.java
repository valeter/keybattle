package ru.anisimov.keybattle.core.util.db;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.Arrays;
import java.util.StringJoiner;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         29.12.16
 */
class PaginationPartsFactory {
    public static <T> String buildSqlMySQL(Pagination<T> pagination, Pageable pageable, SqlParams params) {
        String columnsString = pagination.columns == null || pagination.columns.length == 0
                ? "*"
                : Arrays.stream(pagination.columns).collect(Collectors.joining(", "));
        return "SELECT " + columnsString + " FROM " +
                table + buildWhere(params) + buildSortAndPaging(pageable);
    }

    public static String buildCountSqlMySQL(SqlParams params) {
        String columnsString = columns == null || columns.length == 0
                ? "*"
                : Arrays.stream(columns).collect(Collectors.joining(", "));
        return "SELECT COUNT(*) FROM (" + "SELECT " + columnsString + " FROM " +
                table + buildWhere(params) + ") T";
    }

    public static String buildWhereMySQL(SqlParams params) {
        if (params.size() > 0) {
            StringJoiner result = new StringJoiner(" = ? , ", " WHERE ", " = ?");
            params.keySet().forEach(result::add);
            return result.toString();
        }
        return "";
    }
    
    public static String buildSortAndPagingMySQL(Pageable pageable) {
        StringBuilder result = new StringBuilder();
        StringJoiner sort = new StringJoiner(", ", " ORDER BY ", "");
        StreamSupport
                .stream(((Iterable<Sort.Order>)() -> pageable.getSort().iterator()).spliterator(), false)
                .map((order) -> order.getProperty() + " " + order.getDirection().name())
                .forEach(sort::add);
        result.append(" LIMIT ").append(pageable.getPageSize())
                .append(" OFFSET ").append(pageable.getOffset());
        return result.toString();
    }
}
