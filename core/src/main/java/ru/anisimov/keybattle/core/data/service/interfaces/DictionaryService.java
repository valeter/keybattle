package ru.anisimov.keybattle.core.data.service.interfaces;

import ru.anisimov.keybattle.core.SortOrder;
import ru.anisimov.keybattle.core.model.locale.DictionaryLocale;
import ru.anisimov.keybattle.core.model.locale.DictionaryType;

import java.util.Map;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         11/7/14
 */
public interface DictionaryService {
	Map<Long, String> getDictionary(DictionaryLocale locale, DictionaryType type, SortOrder sortBy);
}
