package ru.anisimov.keybattle.core.model.text;

import java.util.Arrays;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         28.10.15
 */
public class TextDetails {
	private String authorFirstName;
	private String authorLastName;
	private String bookTitle;

	public TextDetails() {
	}

	public TextDetails(String authorFirstName, String authorLastName, String bookTitle) {
		this.authorFirstName = authorFirstName;
		this.authorLastName = authorLastName;
		this.bookTitle = bookTitle;
	}

	public String getAuthorFirstName() {
		return authorFirstName;
	}

	public void setAuthorFirstName(String authorFirstName) {
		this.authorFirstName = authorFirstName;
	}

	public String getAuthorLastName() {
		return authorLastName;
	}

	public void setAuthorLastName(String authorLastName) {
		this.authorLastName = authorLastName;
	}

	public String getBookTitle() {
		return bookTitle;
	}

	public void setBookTitle(String bookName) {
		this.bookTitle = bookName;
	}
	
	private Object[] keyArray() {
		return new Object[]{authorFirstName, authorLastName, bookTitle};
	}
	
	@Override
	public int hashCode() {
		return Arrays.hashCode(keyArray());
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof TextDetails)) {
			return false;
		}
	
		TextDetails that = (TextDetails) obj;
		return Arrays.equals(this.keyArray(), that.keyArray());
	}
	
	@Override
	public String toString() {
		return "TextDetails: " +
                Arrays.toString(keyArray());
	}
}
