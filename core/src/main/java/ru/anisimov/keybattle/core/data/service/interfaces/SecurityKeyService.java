package ru.anisimov.keybattle.core.data.service.interfaces;

import ru.anisimov.keybattle.core.security.SecurityKeyType;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         24.03.15
 */
public interface SecurityKeyService {
	Long storeKey(Long sourceId, SecurityKeyType keyType, String key);
	
	Long checkKey(Long id, SecurityKeyType keyType, String key);

	boolean markDeleted(Long sourceId, SecurityKeyType keyType);
	
	void clear();
}
