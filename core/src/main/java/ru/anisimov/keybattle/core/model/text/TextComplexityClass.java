package ru.anisimov.keybattle.core.model.text;

import ru.anisimov.keybattle.core.HasDictionary;
import ru.anisimov.keybattle.core.model.locale.DictionaryType;

import java.util.Arrays;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         07.11.15
 */
public enum TextComplexityClass implements HasDictionary {
	EASY,
	MEDIUM,
	HARD;
	
	private static final int MAX_EASY_TEXT_SYMBOL_COUNT = 800;
	private static final int MAX_MEDIUM_TEXT_SYMBOL_COUNT = 1600;
	
	public static TextComplexityClass getComplexity(Text text) {
		int symbolCount = Arrays.stream(text.getWords()).mapToInt(String::length).sum();
		return symbolCount < MAX_EASY_TEXT_SYMBOL_COUNT ? EASY 
				: (symbolCount < MAX_MEDIUM_TEXT_SYMBOL_COUNT ? MEDIUM : HARD);
	}
	
	@Override
	public DictionaryType getDictionaryType() {
		return DictionaryType.TEXT_COMPLEXITY_CLASS;
	}
}
