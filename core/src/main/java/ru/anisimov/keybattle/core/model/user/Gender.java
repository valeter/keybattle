package ru.anisimov.keybattle.core.model.user;

import ru.anisimov.keybattle.core.HasDictionary;
import ru.anisimov.keybattle.core.model.locale.DictionaryType;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         10/31/14
 */
public enum Gender implements HasDictionary {
	NONE,
	MALE,
	FEMALE,
	MALE_FEMALE,
	FEMALE_MALE,
	ALIEN;

	@Override
	public DictionaryType getDictionaryType() {
		return DictionaryType.GENDER;
	}
}
