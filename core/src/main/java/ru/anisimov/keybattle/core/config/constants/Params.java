package ru.anisimov.keybattle.core.config.constants;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         13.03.15
 */
public interface Params {
	/* ---------------------------------------------------- */
	/* common */
	
	String LANG = "lang";
	String ERROR = "error";
	String USER_ID = "user-id";
	String ROLE = "role";
	String ENABLED = "enabled";
	String COMMENT = "comment";
	String TYPE = "type";
	String SORT_BY = "sort-by";
	String AVATAR = "avatar";
	String STATUS = "status";
	String SAVE_ACTION = "save-action";
	String DELETE_ACTION = "delete-action";
	String ENTITY_ID = "entityid";
	String SK = "sk";
	String LOCALE_ID = "locale-id";
	
	
	/* ---------------------------------------------------- */
	/* text */

	
	String TEXT_VALUE = "text-value";
	String TEXT_MOD_STATE = "text-mod-state";
	
	
	/* ---------------------------------------------------- */
	/* security */
	
	String CAPTCHA_CHALLENGE_FIELD = "recaptcha_challenge_field";
	String CAPTCHA_RESPONSE_FIELD = "recaptcha_response_field";
	
	
	/* ---------------------------------------------------- */
	/* values */

	String LOGIN_ERROR_VALUE = "login_error";
	String RECAPTCHA_ERROR_VALUE = "recaptcha_error";
	String SUCCESS_VALUE = "success";
	String FAIL_VALUE = "fail";
	
	
	/* ---------------------------------------------------- */
	/* with values */
	
	String LOGIN_ERROR = ERROR + "=" + LOGIN_ERROR_VALUE;
	String LOGOUT_SUCCESS = "logout=" + SUCCESS_VALUE;
	String SIGNUP_SUCCESS = "signup=" + SUCCESS_VALUE;
	String SIGNUP_CONFIRM_SUCCESS = "signupconfirm=" + SUCCESS_VALUE;
	String SIGNUP_CONFIRM_FAIL = "signupconfirm=" + FAIL_VALUE;
	String RECAPTCHA_ERROR = ERROR + "=" + RECAPTCHA_ERROR_VALUE;
}
