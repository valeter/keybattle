package ru.anisimov.keybattle.core.config.constants;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         11/2/14
 */
public interface ModelAttributes {
	/* ---------------------------------------------------- */
	/* common */
	
	String STATUS = "http_status";
	String EXCEPTION = "exception";
	String URL = "url";

	String WEB_ACTION_STATUS_DICT = "webActionStatusDict";
	String WEB_ACTION_PARAM_DICT = "webActionParamDict";
	String WEB_ACTION_PARAM_VALUE_DICT = "webActionParamValueDict";
	String WEB_CAPTION_DICT = "webCaptionDict";
	String COUNTRY_DICT = "countryDict";
	String GENDER_DICT = "genderDict";

	String WEEKDAYS = "weekdays";
	String SHORT_WEEKDAYS = "shortWeekdays";
	String MONTHS = "months";
	String SHORT_MONTHS = "shortMonths";

	String EDITABLE = "editable";
	
	
	/* ---------------------------------------------------- */
	/* sing up page */
	
	String REGISTRATION = "registration";
	String INVALID_RECAPTCHA = "invalidRecaptcha";
	
	
	/* ---------------------------------------------------- */
	/* user profile */
    
    String USER_INFO = "userInfo";
	String USER_SETTINGS = "userSettings";
	
	
	/* ---------------------------------------------------- */
	/* battle */

	String BATTLE = "battle";
	String BATTLE_ID = "battleId";
}
