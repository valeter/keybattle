package ru.anisimov.keybattle.core.data.service.interfaces;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ru.anisimov.keybattle.core.model.locale.DictionaryLocale;
import ru.anisimov.keybattle.core.model.text.ModState;
import ru.anisimov.keybattle.core.model.text.Text;
import ru.anisimov.keybattle.core.model.text.TextInfo;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         18.03.15
 */
public interface TextService {
	Long addTextInfo(TextInfo textInfo);

	boolean addTextValue(long textId, String text);

	boolean changeTextValue(long oldTextId, String text);

	boolean removeTextInfo(long textId);

	boolean removeTextValue(long textId);

	boolean moderateText(long textId, ModState modState);

	Text getText(long textId);

	Text getRandomText(DictionaryLocale locale, ModState modState);
	
	TextInfo getTextInfo(long textId);

	Page<TextInfo> getTextInfos(DictionaryLocale locale, ModState modState, Pageable pageable);

	Page<TextInfo> getTextInfos(ModState modState, Pageable pageable);

	Page<TextInfo> getTextInfos(long authorId, Pageable pageable);

	String getTextValue(long textId);
}
