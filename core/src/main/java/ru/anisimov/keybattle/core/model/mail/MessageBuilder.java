package ru.anisimov.keybattle.core.model.mail;

import java.util.Locale;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         11.04.15
 */
public class MessageBuilder {
	private Message message;

	public MessageBuilder() {
		message = new Message();
	}
	
	public MessageBuilder sender(Sender sender) {
		message.setSender(sender);
		return this;
	}

	public MessageBuilder template(MessageTemplate template) {
		message.setTemplate(template);
		return this;
	}

	public MessageBuilder locale(Locale locale) {
		message.setLocale(locale);
		return this;
	}

	public MessageBuilder to(String to) {
		message.setTo(to);
		return this;
	}

	public MessageBuilder cc(String... cc) {
		message.setCc(cc);
		return this;
	}
	
	public MessageBuilder withParam(String name, Object value) {
		message.getVariables().put(name, value);
		return this;
	}
	
	public Message build() {
		return message;
	}
}
