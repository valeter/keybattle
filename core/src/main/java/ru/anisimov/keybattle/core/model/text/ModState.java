package ru.anisimov.keybattle.core.model.text;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         18.03.15
 */
public enum ModState {
	NOT_MODERATED,
	APPROVED,
	REJECTED
}
