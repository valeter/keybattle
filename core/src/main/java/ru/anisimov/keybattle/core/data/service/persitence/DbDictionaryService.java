package ru.anisimov.keybattle.core.data.service.persitence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import ru.anisimov.keybattle.core.SortOrder;
import ru.anisimov.keybattle.core.config.constants.Caches;
import ru.anisimov.keybattle.core.data.service.interfaces.DictionaryService;
import ru.anisimov.keybattle.core.model.locale.DictionaryLocale;
import ru.anisimov.keybattle.core.model.locale.DictionaryType;

import java.util.*;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         11/2/14
 */
@Service("dictionaryService")
public class DbDictionaryService implements DictionaryService {
    private static final String SELECT_BY_TYPE_AND_LOCALE = "SELECT TERM_ID, VALUE FROM DICT WHERE TYPE_ID = ? AND LOCALE_ID = ?";

    @Autowired
    private JdbcTemplate jdbcTemplate;

	@Cacheable(value = Caches.DICTIONARY, key = "#locale.name + #type.name + #sortBy.name")
    @Override
    public Map<Long, String> getDictionary(DictionaryLocale locale, DictionaryType type, SortOrder sortBy) {
		Map<Long, String> result = new HashMap<>();
		jdbcTemplate.query(SELECT_BY_TYPE_AND_LOCALE,
				resultSet -> {
					result.put(resultSet.getLong("TERM_ID"), resultSet.getString("VALUE"));
				},
				type.ordinal(), locale.ordinal());
		return sort(result, sortBy);
    }

    private Map<Long, String> sort(Map<Long, String> dictionary, SortOrder sortBy) {
        if (sortBy == null) {
            return dictionary;
        }
        switch (sortBy) {
            case KEY:
                return sortByKey(dictionary);
            case VALUE:
                return sortByValue(dictionary);
            default:
                return dictionary;
        }
    }
    

    private static <K, V extends Comparable<? super V>> Map<K, V> sortByKey(Map<K, V> map) {
        return new TreeMap<>(map);
    }

    private static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
        Map<K,V> result = new LinkedHashMap<>();
        map.entrySet().stream()
                .sorted(Comparator.comparing(Map.Entry::getValue))
                .forEach(e -> result.put(e.getKey(), e.getValue()));
        return result;
    }
}
