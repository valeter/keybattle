package ru.anisimov.keybattle.core.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.anisimov.keybattle.core.data.service.interfaces.UserService;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         14.04.15
 */
@Component
public class SecurityKeyTypeContext {
	@Autowired
	private UserService userService;

	public UserService getUserService() {
		return userService;
	}
}
