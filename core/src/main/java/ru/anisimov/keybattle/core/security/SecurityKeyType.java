package ru.anisimov.keybattle.core.security;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         24.03.15
 */
public enum SecurityKeyType {
	REGISTRATION((sourceId, context) -> context.getUserService().enableUser(sourceId, true));
	
	private SourceIdProcessor sourceIdProcessor;

	SecurityKeyType(SourceIdProcessor sourceIdProcessor) {
		this.sourceIdProcessor = sourceIdProcessor;
	}
	
	public boolean process(long sourceId, SecurityKeyTypeContext context) {
		return this.sourceIdProcessor.process(sourceId, context);
	}

	private interface SourceIdProcessor {
		boolean process(long sourceId, SecurityKeyTypeContext context);
	}
}
