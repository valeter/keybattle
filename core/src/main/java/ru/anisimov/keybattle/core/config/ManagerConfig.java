package ru.anisimov.keybattle.core.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         06.05.15
 */
@Configuration
@ComponentScan(basePackages = {"ru.anisimov.keybattle.core.manager"})
public class ManagerConfig {
}
