package ru.anisimov.keybattle.core.model.locale;


import java.util.*;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         11/7/14
 */
public enum DictionaryLocale {
	RU("ru"),
	EN ("en");

	public static final DictionaryLocale DEFAULT = EN;

	private String language;

	DictionaryLocale(String language) {
		this.language = language;
	}

	public static DictionaryLocale getDictLocale(Locale locale) {
		return Arrays.stream(DictionaryLocale.values())
				.filter(dictLocale -> Objects.equals(dictLocale.getLanguage(), locale.getLanguage()))
				.findAny().orElse(DEFAULT);
	}

	public String getLanguage() {
		return new Locale(language).getLanguage();
	}
}
