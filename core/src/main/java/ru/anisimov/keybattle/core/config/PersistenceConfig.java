package ru.anisimov.keybattle.core.config;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.support.TransactionTemplate;

import javax.sql.DataSource;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         10/10/14
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = {"ru.anisimov.keybattle.core.data"})
@ComponentScan(basePackages = {"ru.anisimov.keybattle.core.data"})
public class PersistenceConfig {
	// battle db
	
	@Bean(name = "battleJdbcTemplateNamed")
	public NamedParameterJdbcTemplate battleJdbcTemplateNamed(
			@Qualifier("battleDataSource") DataSource battleDataSource) {
		return new NamedParameterJdbcTemplate(battleDataSource);
	}

	@Bean(name = "battleJdbcTemplate")
	public JdbcTemplate battleJdbcTemplate(
			@Qualifier("battleDataSource") DataSource battleDataSource) {
		return new JdbcTemplate(battleDataSource);
	}

	@Bean(name = "battleDataSource")
	public DataSource battleDataSource(
			@Value("${battle.driver}") String driver,
			@Value("${battle.url}") String url,
			@Value("${battle.username}") String userName,
			@Value("${battle.password}") String password
	) {
        return newBasicDataSource(driver,url,userName,password);
	}
	
	
	// tms db
	
	@Bean(name = "tmsJdbcTemplateNamed")
	public NamedParameterJdbcTemplate tmsJdbcTemplateNamed(
			@Qualifier("tmsDataSource") DataSource tmsDataSource) {
		return new NamedParameterJdbcTemplate(tmsDataSource);
	}

	@Bean(name = "tmsJdbcTemplate")
	public JdbcTemplate tmsJdbcTemplate(
			@Qualifier("tmsDataSource") DataSource tmsDataSource) {
		return new JdbcTemplate(tmsDataSource);
	}

	@Bean(name = "tmsDataSource")
	public DataSource tmsDataSource(
			@Value("${tms.driver}") String driver,
			@Value("${tms.url}") String url,
			@Value("${tms.username}") String userName,
			@Value("${tms.password}") String password
	) {
        return newBasicDataSource(driver,url,userName,password);
	}


	// keybattle db

	@Bean(name = "jdbcTemplateNamed")
	public NamedParameterJdbcTemplate jdbcTemplateNamed(
			@Qualifier("dataSource") DataSource dataSource) {
		return new NamedParameterJdbcTemplate(dataSource);
	}

	@Bean(name = "jdbcTemplate")
	public JdbcTemplate jdbcTemplate(
			@Qualifier("dataSource") DataSource dataSource) {
		return new JdbcTemplate(dataSource);
	}

	@Bean(name = "dataSource")
	public DataSource dataSource(
			@Value("${keybattle.driver}") String driver,
			@Value("${keybattle.url}") String url,
			@Value("${keybattle.username}") String userName,
			@Value("${keybattle.password}") String password
	) {
		return newBasicDataSource(driver,url,userName,password);
	}
	
	
	private BasicDataSource newBasicDataSource(String driver, String url, String userName, String password) {
        BasicDataSource result = new BasicDataSource();
        result.setDriverClassName(driver);
        result.setUrl(url);
        result.setUsername(userName);
        result.setPassword(password);
        return result;
    }
    

	@Bean(name = "transactionTemplate")
	public TransactionTemplate transactionTemplate(
			@Qualifier("transactionManager") PlatformTransactionManager transactionManager) {
		return new TransactionTemplate(transactionManager);
	}

	// supports both JPA and JDBC transactions
	@Bean(name = "transactionManager")
	public PlatformTransactionManager transactionManager(
			LocalContainerEntityManagerFactoryBean entityManagerFactory) {
		return new JpaTransactionManager(entityManagerFactory.getObject());
	}

	
	// JPA for keybattle db
	
	@Bean 
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(
			@Qualifier("dataSource") DataSource dataSource) {
		HibernateJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
		adapter.setDatabase(Database.MYSQL);
		adapter.setShowSql(true);

		LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
		factoryBean.setPackagesToScan("ru.anisimov.keybattle.core.data", "ru.anisimov.keybattle.core.model");
		factoryBean.setJpaVendorAdapter(adapter);
		factoryBean.setDataSource(dataSource);

		return factoryBean;
	}
}
