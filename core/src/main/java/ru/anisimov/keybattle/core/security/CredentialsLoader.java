package ru.anisimov.keybattle.core.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import ru.anisimov.keybattle.core.Pair;
import ru.anisimov.keybattle.core.model.mail.Sender;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         21.03.15
 */
@Component
public class CredentialsLoader {
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	public Pair<String, String> mail(Sender sender) {
		String[] result = jdbcTemplate.queryForObject(
				"SELECT VALUE FROM CREDENTIALS WHERE NAME = 'mail-" + sender.name().toLowerCase() + "'",
				String.class).split(" ");
		return new Pair<>(result[0], result[1]);
	}
	
	public String recaptcha() {
		return jdbcTemplate.queryForObject(
				"SELECT VALUE FROM CREDENTIALS WHERE NAME = 'recaptcha'",
				String.class);
	}

	public String rememberMe() {
		return jdbcTemplate.queryForObject(
				"SELECT VALUE FROM CREDENTIALS WHERE NAME = 'remember-me'",
				String.class);
	}
}
