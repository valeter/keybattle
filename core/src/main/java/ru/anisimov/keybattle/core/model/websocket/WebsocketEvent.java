package ru.anisimov.keybattle.core.model.websocket;

import java.util.Arrays;
import java.util.List;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         03.10.15
 */
public class WebsocketEvent {
	private String type;
	private List<?> objects;

	public WebsocketEvent(String type, List<?> objects) {
		this.type = type;
		this.objects = objects;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<?> getObjects() {
		return objects;
	}

	public void setObjects(List<Object> objects) {
		this.objects = objects;
	}
	
	private Object[] keyArray() {
		return new Object[]{type, objects == null ? null : objects.toArray()};
	}
	
	@Override
	public int hashCode() {
		return Arrays.deepHashCode(keyArray());
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof WebsocketEvent)) {
			return false;
		}
	
		WebsocketEvent that = (WebsocketEvent) obj;
		return Arrays.deepEquals(this.keyArray(), that.keyArray());
	}
	
	@Override
	public String toString() {
		return new StringBuilder()
				.append("WebsocketEvent: ")
				.append(Arrays.deepToString(keyArray()))
				.toString();
	}
}
