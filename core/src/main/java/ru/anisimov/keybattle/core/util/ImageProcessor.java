package ru.anisimov.keybattle.core.util;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         26.01.15
 */
public class ImageProcessor {
	private static final Logger log = LogManager.getLogger(ImageProcessor.class);
	
	public static byte[] scaleAndCrop(BufferedImage image, int newWidth, int newHeight, String format) {
		try {
			int newScaledWidth = (double)image.getHeight() / image.getWidth() > (double)newHeight / newWidth
					? newWidth
					: Math.round((float)image.getWidth() * newHeight / (float)image.getHeight());
			int newScaledHeight = (double)image.getWidth() / image.getHeight() > (double)newWidth / newHeight
					? newHeight
					: Math.round((float)image.getHeight() * newWidth / (float)image.getWidth());

			int width = newScaledWidth == newWidth ? image.getWidth() : image.getHeight() * newWidth / newHeight;
			int height = newScaledHeight == newHeight ? image.getHeight() : image.getWidth() * newHeight / newWidth;
			int x = (image.getWidth() - width) / 2;
			int y = (image.getHeight() - height) / 2;

			Image resizedImage = image
					.getSubimage(x, y, width, height)
					.getScaledInstance(newWidth, newHeight, Image.SCALE_SMOOTH);

			BufferedImage imageBuff = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_RGB);
			Graphics g = imageBuff.createGraphics();
			g.drawImage(resizedImage, 0, 0, newWidth, newHeight, null);
			g.dispose();

			ByteArrayOutputStream result = new ByteArrayOutputStream();
			ImageIO.write(imageBuff, format, result);
			return result.toByteArray();
		} catch (Exception e) {
			log.error("Could not resize image", e);
			return null;
		}
	}
}
