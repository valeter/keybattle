package ru.anisimov.keybattle.core.remote;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.stereotype.Component;
import ru.anisimov.keybattle.core.Pair;
import ru.anisimov.keybattle.core.model.mail.Sender;
import ru.anisimov.keybattle.core.security.CredentialsLoader;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         21.03.15
 */
@Component
public class RemoteMailService {
	private static final Logger log = LogManager.getLogger(RemoteMailService.class);
	private static final String CONTENT_TYPE = "text/html; charset=utf-8";
	private static final String KEYBATTLE_NET = "@keybattle.net";

	private Map<Sender, Session> sessions = new HashMap<>();
	
	@Autowired
	public RemoteMailService(CredentialsLoader credentialsLoader) throws IOException {
		Resource resource = new ClassPathResource("mail.properties");
		Properties properties = PropertiesLoaderUtils.loadProperties(resource);
		
		for (Sender sender : Sender.values()) {
			Pair<String, String> mailCredentials = credentialsLoader.mail(sender);
			
			sessions.put(sender, 
					Session.getInstance(
						properties,
						new Authenticator() {
							protected PasswordAuthentication getPasswordAuthentication() {
								return new PasswordAuthentication(
										mailCredentials.getFirst(),
										mailCredentials.getSecond()
								);
							}
						}));
		}
	}
	
	public boolean sendMessage(Sender sender, String subject, String content, String to, String... cc) {
		try {
			Message message = new MimeMessage(sessions.get(sender));
			message.setFrom(new InternetAddress(sender.name().toLowerCase() + KEYBATTLE_NET));
			message.addRecipient(Message.RecipientType.TO, InternetAddress.parse(to)[0]);
			for (String mail : cc) {
				message.addRecipient(Message.RecipientType.CC, InternetAddress.parse(mail)[0]);
			}
	
			message.setSubject(subject);
			message.setContent(content, CONTENT_TYPE);
	
			Transport.send(message);
			log.debug("Message with subject " + subject + " sent to: " + to + " cc: " + Arrays.toString(cc));
			return true;
		} catch (MessagingException e) {
			log.error(e);
			return false;
		}
	}
}
