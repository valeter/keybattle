package ru.anisimov.keybattle.core.model.mail;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         11.04.15
 */
public enum MessageTemplate {
	REGISTRATION_CONFIRM("liquibase/mail/registration-confirm");

	private String path;

	private MessageTemplate(String path) {
		this.path = path;
	}

	public String getPath() {
		return path;
	}
}
