package ru.anisimov.keybattle.core.exception;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         19.09.15
 */
public class WordCheckException extends Exception {
	public WordCheckException() {
	}

	public WordCheckException(String message) {
		super(message);
	}

	public WordCheckException(String message, Throwable cause) {
		super(message, cause);
	}

	public WordCheckException(Throwable cause) {
		super(cause);
	}

	public WordCheckException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
