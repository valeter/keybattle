package ru.anisimov.keybattle.core.model.user;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         10.05.15
 */
public enum UserImageType {
	BIG(150, 150, "png"),
	MEDIUM(100, 100, "png"),
	SMALL(50, 50, "png");
	
	private int width;
	private int height;
	private String format;

	UserImageType(int width, int height, String format) {
		this.width = width;
		this.height = height;
		this.format = format;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}
}
