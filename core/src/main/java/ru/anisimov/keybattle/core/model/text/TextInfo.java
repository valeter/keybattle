package ru.anisimov.keybattle.core.model.text;

import ru.anisimov.keybattle.core.model.locale.DictionaryLocale;

import java.time.LocalDateTime;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         18.03.15
 */
public class TextInfo {
	private long id;
	private long authorId;
	private DictionaryLocale locale;
	private ModState modState;
	private LocalDateTime creationTime;
	private LocalDateTime modificationTime;

	public TextInfo() {
	}

	public TextInfo(long authorId, DictionaryLocale locale) {
		this.authorId = authorId;
		this.locale = locale;
	}

	protected TextInfo(TextInfo textInfo) {
		this.id = textInfo.id;
		this.authorId = textInfo.authorId;
		this.locale = textInfo.locale;
		this.modState = textInfo.modState;
		this.creationTime = textInfo.creationTime;
		this.modificationTime = textInfo.modificationTime;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(long authorId) {
		this.authorId = authorId;
	}

	public DictionaryLocale getLocale() {
		return locale;
	}

	public void setLocale(DictionaryLocale locale) {
		this.locale = locale;
	}

	public ModState getModState() {
		return modState;
	}

	public void setModState(ModState modState) {
		this.modState = modState;
	}

	public LocalDateTime getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(LocalDateTime creationTime) {
		this.creationTime = creationTime;
	}

	public LocalDateTime getModificationTime() {
		return modificationTime;
	}

	public void setModificationTime(LocalDateTime modificationTime) {
		this.modificationTime = modificationTime;
	}
}
