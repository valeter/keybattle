package ru.anisimov.keybattle.core.data.service.interfaces;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import ru.anisimov.keybattle.core.model.history.HistoryAction;
import ru.anisimov.keybattle.core.model.history.UserHistoryRecord;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         10/31/14
 */
@Repository
public interface UserHistoryService extends PagingAndSortingRepository<UserHistoryRecord, Long> {
	default boolean addRecord(long userId, HistoryAction action, long actorId, String comment) {
		save(new UserHistoryRecord(userId, action, LocalDateTime.now(), actorId, comment));
		return true;
	}
	
	List<UserHistoryRecord> findRecordsByUserId(long userId);
	
	List<UserHistoryRecord> findRecordsByActorId(long actorId);
}
