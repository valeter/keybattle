package ru.anisimov.keybattle.core.data.service.interfaces.impl;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import ru.anisimov.keybattle.core.model.user.UserRole;
import ru.anisimov.keybattle.core.model.user.Registration;
import ru.anisimov.keybattle.core.model.user.User;
import ru.anisimov.keybattle.core.model.user.UserRoleType;
import ru.anisimov.keybattle.core.data.service.interfaces.UserService;

import java.util.List;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         10/31/14
 */
public class AbstractUserService implements UserService {
	@Override
	public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
		throw new UnsupportedOperationException();
	}

	@Override
	public Long addUser(Registration user) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean addUser(User newUser) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean changeUserPassword(long userId, String password) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean removeUser(long userId) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean lockUser(long userId, boolean locked) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean enableUser(long userId, boolean enabled) {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<User> getUsers(Boolean withActiveRoles, Boolean locked, Boolean enabled) {
		throw new UnsupportedOperationException();
	}

	@Override
	public User getUserById(long userId) {
		throw new UnsupportedOperationException();
	}

	@Override
	public User getUserByUserName(String userName) {
		throw new UnsupportedOperationException();
	}

	@Override
	public User getUserByEmail(String email) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean addRole(long userId, UserRoleType role) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean removeRole(long userId, UserRoleType role) {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<User> getUsers() {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<User> getUsers(UserRoleType role) {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<UserRole> getUserRoles(long userId, Boolean enabled) {
		throw new UnsupportedOperationException();
	}
}
