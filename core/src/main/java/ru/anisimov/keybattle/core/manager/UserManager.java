package ru.anisimov.keybattle.core.manager;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionTemplate;
import ru.anisimov.keybattle.core.HasTransactions;
import ru.anisimov.keybattle.core.data.service.interfaces.UserHistoryService;
import ru.anisimov.keybattle.core.data.service.interfaces.UserImageService;
import ru.anisimov.keybattle.core.data.service.interfaces.UserInfoService;
import ru.anisimov.keybattle.core.data.service.interfaces.UserService;
import ru.anisimov.keybattle.core.model.history.HistoryAction;
import ru.anisimov.keybattle.core.model.history.UserHistoryRecord;
import ru.anisimov.keybattle.core.model.user.UserImage;
import ru.anisimov.keybattle.core.model.user.UserImageType;
import ru.anisimov.keybattle.core.model.user.UserInfo;

import javax.annotation.PostConstruct;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         10/31/14
 */
@Component
public class UserManager implements HasTransactions {
	private static final Logger log = LogManager.getLogger(UserManager.class);
	
	@Autowired
	private TransactionTemplate transactionTemplate;
	@Autowired
	private UserService userService;
	@Autowired
	private UserInfoService userInfoService;
	@Autowired
	private UserHistoryService userHistoryService;
	@Autowired
	private UserImageService userImageService;

	@Value("${anonymous.avatar.path}")
	private String anonymousAvatarPath;
	private byte[] defaultAvatar;

	public UserInfo getUserInfo(Long userId) {
		return getWithoutException(
				() -> userInfoService.getUserInfo(userId),
				String.format("Could not get user info for user %d", userId)
		);
	}

	public UserSettings getUserSettings(Long userId) {
		return getWithoutException(
				() -> userInfoService.getUserSettings(userId),
				String.format("Could not get user settings for user %d", userId)
		);
	}

	public List<UserInfo> getUserInfos() {
		return getWithoutException(
				userInfoService::getUserInfos,
				"Could not get user info list"
		);
	}

	public List<UserInfo> getUserInfos(UserRoleType role) {
		return getWithoutException(
				() -> userInfoService.getUserInfos(role),
				String.format("Could not get user info list for role %s", role)
		);
	}

	public byte[] getUserAvatar(Long userId, UserImageType type) {
		return getWithoutException(
				() -> {
					UserImage image = userImageService.findByUserIdAndType(userId, type);
					return image == null ? defaultAvatar: image.getImage();
				},
				String.format("Could not get user image for user %d", userId)
		);
	}

	public String getUserStatus(Long userId) {
		return getWithoutException(
				() -> userInfoService.getUserStatus(userId),
				String.format("Could not get user status for user %d", userId)
		);
	}

	public List<UserRole> getUserRoles(Long userId, Boolean enabled) {
		return getWithoutException(
				() -> userService.getUserRoles(userId, enabled),
				String.format("Could not get user role list for user %d", userId)
		);
	}

	public Long addUser(Registration user, long actorId) {
		return getInTransactionWithoutException(
				() -> {
					Long userId = userService.addUser(user);
					if (userId != null
							&& userHistoryService.addRecord(userId, HistoryAction.USER_ADDED, actorId, "registration")
							&& userService.addRole(userId, UserRoleType.USER)
							&& userHistoryService.addRecord(userId, HistoryAction.USER_ROLE_ADDED, actorId, "registration")
							&& userInfoService.addInitialUserInfo(userId, user.getEmail())
							&& userHistoryService.addRecord(userId, HistoryAction.USER_INFO_ADDED, actorId, "registration")) {
						return userId;
					}
					return null;
				},
				String.format("Could not add user %s by actor %d", user, actorId)
		);
	}

	public boolean clearUserInfo(long userId, long actorId) {
		return doInTransactionWithoutException(
				() -> userInfoService.clearUserInfo(userId)
						&& userHistoryService.addRecord(userId, HistoryAction.USER_INFO_CHANGED, actorId, "clear"),
				String.format("Could not clear user info for user %d by actor %d", userId, actorId)
		);
	}

	public boolean clearUserSettings(long userId, long actorId) {
		return doInTransactionWithoutException(
				() -> userInfoService.clearUserSettings(userId)
						&& userHistoryService.addRecord(userId, HistoryAction.USER_SETTINGS_CHANGED, actorId, "clear"),
				String.format("Could not clear user settings for user %d by actor %d", userId, actorId)
		);
	}

	public boolean addUserRole(Long userId, UserRoleType role, long actorId, String comment) {
		return doInTransactionWithoutException(
				() -> userService.addRole(userId, role)
						&& userHistoryService.addRecord(userId, HistoryAction.USER_ROLE_ADDED, actorId, comment),
				String.format("Could not add for user %d role %s by actor %d", userId, role, actorId)
		);
	}

	public boolean removeUserRole(long userId, UserRoleType role, long actorId, String comment) {
		return doInTransactionWithoutException(
				() -> userService.removeRole(userId, role)
						&& userHistoryService.addRecord(userId, HistoryAction.USER_ROLE_REMOVED, actorId, comment),
				String.format("Could not remove for user %d role %s by actor %d", userId, role, actorId)
		);
	}

	public boolean lockUser(long userId, long actorId, String comment) {
		return doInTransactionWithoutException(
				() -> userService.lockUser(userId, true)
						&& userHistoryService.addRecord(userId, HistoryAction.USER_LOCKED, actorId, comment),
				String.format("Could not lock user %d by actor %d", userId, actorId)
		);
	}

	public boolean unlockUser(long userId, long actorId, String comment) {
		return doInTransactionWithoutException(
				() -> userService.lockUser(userId, false)
						&& userHistoryService.addRecord(userId, HistoryAction.USER_UNLOCKED, actorId, comment),
				String.format("Could not lock user %d by actor %d", userId, actorId)
		);
	}

	public boolean disableUser(long userId, long actorId, String comment) {
		return doInTransactionWithoutException(
				() -> userService.enableUser(userId, false)
						&& userHistoryService.addRecord(userId, HistoryAction.USER_DISABLED, actorId, comment),
				String.format("Could not disable user %d by actor %d", userId, actorId)
		);
	}

	public boolean enableUser(long userId, long actorId, String comment) {
		return doInTransactionWithoutException(
				() -> userService.enableUser(userId, true)
						&& userHistoryService.addRecord(userId, HistoryAction.USER_ENABLED, actorId, comment),
				String.format("Could not disable user %d by actor %d", userId, actorId)
		);
	}

	public List<User> getUsers(Boolean withActiveRoles, Boolean locked, Boolean enabled) {
		return getWithoutException(
				() -> userService.getUsers(withActiveRoles, locked, enabled),
				"Could not get user list"
		);
	}

	public List<User> getUsers(UserRoleType role) {
		return getWithoutException(
				() -> userService.getUsers(role),
				String.format("Could not get user list for role %s", role)  
		);
	}

	public User getUserById(long userId) {
		return getWithoutException(
				() -> userService.getUserById(userId),
				String.format("Could not get user with id %d", userId)
		);
	}

	public User getUserByUserName(String userName) {
		return getWithoutException(
				() -> userService.getUserByUserName(userName),
				String.format("Could not get user with name %s", userName)
		);
	}

	public User getUserByEmail(String email) {
		return getWithoutException(
				() -> userService.getUserByEmail(email),
				String.format("Could not get user with email %s", email)
		);
	}

	public boolean changeUserInfo(Long userId, UserInfo userInfo, long actorId) {
		return doInTransactionWithoutException(
				() -> userInfoService.changeUserInfo(userId, userInfo)
						&& userHistoryService.addRecord(userId, HistoryAction.USER_INFO_CHANGED, actorId, null),
				String.format("Could not change user info to %s for user %d by actor %d", String.valueOf(userInfo), userId, actorId)
		);
	}

	public boolean changeUserSettings(Long userId, UserSettings userSettings, long actorId) {
		return doInTransactionWithoutException(
				() -> userInfoService.changeUserSettings(userId, userSettings)
						&& userHistoryService.addRecord(userId, HistoryAction.USER_SETTINGS_CHANGED, actorId, null),
				String.format("Could not change user settings to %s for user %d by actor %d", String.valueOf(userSettings), userId, actorId)
		);
	}

	public boolean changeUserStatus(Long userId, String status, long actorId) {
		return doInTransactionWithoutException(
				() -> userInfoService.changeUserStatus(userId, status),
				String.format("Could not change user status to %s for user %d by actor %d", String.valueOf(status), userId, actorId)
		);
	}

	public boolean changeUserAvatar(Long userId, BufferedImage avatar, long actorId) {
		return doInTransactionWithoutException(
				() -> {
					userImageService.deleteByUserId(userId);
					for (UserImageType imageType : UserImageType.values()) {
						userImageService.save(new UserImage(userId, imageType, avatar));
					}
					return userHistoryService.addRecord(userId, HistoryAction.USER_AVATAR_CHANGED, actorId, null);
				},
				String.format("Could not change user avatar for user %d by actor %d", userId, actorId)
		);
	}

	public boolean clearUserAvatar(Long userId, long actorId) {
		return doInTransactionWithoutException(
				() -> {
					userImageService.deleteByUserId(userId);
					return userHistoryService.addRecord(userId, HistoryAction.USER_AVATAR_CHANGED, actorId, null);
				},
				String.format("Could not clear user images for user %d by actor %d", userId, actorId)
		);
	}

	public boolean showUserInfo(long userId, long actorId) {
		return doInTransactionWithoutException(
				() -> userInfoService.showUserInfo(userId, true)
						&& userHistoryService.addRecord(userId, HistoryAction.USER_INFO_SHOWN, actorId, null),
				String.format("Could not show user info for user %d by actor %d", userId, actorId)
		);
	}

	public boolean hideUserInfo(long userId, long actorId) {
		return doInTransactionWithoutException(
				() -> userInfoService.showUserInfo(userId, false)
						&& userHistoryService.addRecord(userId, HistoryAction.USER_INFO_HIDDEN, actorId, null),
				String.format("Could not hide user info for user %d by actor %d", userId, actorId)
		);
	}

	public List<UserHistoryRecord> getRecordsByUser(Long userId) {
		return getWithoutException(
				() -> userHistoryService.findRecordsByUserId(userId),
				String.format("Could not get user history for user %d", userId)
		);
	}

	public List<UserHistoryRecord> getRecordsByActor(long actorId) {
		return getWithoutException(
				() -> userHistoryService.findRecordsByActorId(actorId),
				String.format("Could not get user history for user %d", actorId)
		);
	}

	@PostConstruct
	public void init() throws IOException {
		defaultAvatar = IOUtils.toByteArray(this.getClass().getClassLoader().getResourceAsStream(anonymousAvatarPath));
	}

	@Override
	public TransactionTemplate getTransactionTemplate() {
		return transactionTemplate;
	}

	@Override
	public Logger getLogger() {
		return log;
	}
}
