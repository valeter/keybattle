package ru.anisimov.keybattle.core.exception;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         01.02.15
 */
public class AjaxException extends RuntimeException {
	public AjaxException() {
	}

	public AjaxException(String message) {
		super(message);
	}

	public AjaxException(String message, Throwable cause) {
		super(message, cause);
	}

	public AjaxException(Throwable cause) {
		super(cause);
	}

	public AjaxException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
