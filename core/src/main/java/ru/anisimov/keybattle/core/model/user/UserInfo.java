package ru.anisimov.keybattle.core.model.user;

import org.springframework.format.annotation.DateTimeFormat;
import ru.anisimov.keybattle.core.model.user.validation.Dob;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.Arrays;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         10/31/14
 */
public class UserInfo {
	private static final int MIN_COUNTRY_ID = 0;
	private static final int MAX_COUNTRY_ID = 195;
	
	private long userId;
	private String email;
	private String userName;
	
	@Size(min = 0, max = 100)
	private String name;
	private Integer age;

	private Gender gender;

	@Dob
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private LocalDate dateOfBirth;

	@Max(MAX_COUNTRY_ID)
	@Min(MIN_COUNTRY_ID)
	private Integer countryId;
	private String status;
	private boolean hidden;
	private boolean emailHidden;
	private boolean hasAvatar;

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Gender getGender() {
		return gender;
	}
	
	public Integer getGenderId() {
		return gender.ordinal();
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(LocalDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public Integer getCountryId() {
		return countryId;
	}

	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isHidden() {
		return hidden;
	}

	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}

	public boolean isEmailHidden() {
		return emailHidden;
	}

	public void setEmailHidden(boolean emailHidden) {
		this.emailHidden = emailHidden;
	}

	public boolean isHasAvatar() {
		return hasAvatar;
	}

	public void setHasAvatar(boolean hasAvatar) {
		this.hasAvatar = hasAvatar;
	}

	private Object[] keyArray() {
		return new Object[]{userId, userName};
	}
	
	@Override
	public int hashCode() {
		return Arrays.hashCode(keyArray());
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof UserInfo)) {
			return false;
		}
	
		UserInfo that = (UserInfo) obj;
		return Arrays.deepEquals(this.keyArray(), that.keyArray());
	}
	
	@Override
	public String toString() {
		return new StringBuilder()
				.append("UserInfo: ")
				.append(Arrays.toString(keyArray()))
				.toString();
	}
}
