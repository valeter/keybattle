package ru.anisimov.keybattle.core.data.service.persitence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;
import ru.anisimov.keybattle.core.Pair;
import ru.anisimov.keybattle.core.data.service.interfaces.SecurityKeyService;
import ru.anisimov.keybattle.core.security.PasswordEncoding;
import ru.anisimov.keybattle.core.security.SecurityKeyType;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         24.03.15
 */
@Service("securityKeyService")
public class DbSecurityKeyService implements SecurityKeyService {
	private static final HashMap<SecurityKeyType, Integer> DAYS_BEFORE_DEPRECATION = new HashMap<SecurityKeyType, Integer>() {{
		put(SecurityKeyType.REGISTRATION, 30);
	}};
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public Long storeKey(Long sourceId, SecurityKeyType keyType, String key) {
		KeyHolder result = new GeneratedKeyHolder();
		if (jdbcTemplate.update(con -> {
			PreparedStatement query = con.prepareStatement("INSERT INTO SECURITY_KEY (SOURCE_ID, TYPE_ID, SK) VALUES (?, ?, ?)",
					Statement.RETURN_GENERATED_KEYS);
			query.setLong(1, sourceId);
			query.setInt(2, keyType.ordinal());
			query.setString(3, PasswordEncoding.encodePassword(key));
			return query;
		}, result) != 0) {
			return result.getKey().longValue();
		}
		return null;
	}

	@Override
	public Long checkKey(Long id, SecurityKeyType keyType, String key) {
		List<Pair<String, Long>> results = jdbcTemplate.query(
				"SELECT SK, SOURCE_ID FROM SECURITY_KEY WHERE ID = ? AND TYPE_ID = ? AND DELETED = 0 AND TIMESTAMPDIFF(DAY, CREATION_TIME, NOW()) <= ?",
				(rs, rowNum) -> new Pair<>(rs.getString("SK"), rs.getLong("SOURCE_ID")), 
				id, keyType.ordinal(), DAYS_BEFORE_DEPRECATION.get(keyType)); 
		return results != null && results.size() == 1 && PasswordEncoding.checkPassword(key, results.get(0).getFirst())
				? results.get(0).getSecond()
				: null;
	}

	@Override
	public boolean markDeleted(Long sourceId, SecurityKeyType keyType) {
		return jdbcTemplate.update("UPDATE SECURITY_KEY SET DELETED = 1 WHERE SOURCE_ID = ? AND TYPE_ID = ?", 
				sourceId, keyType.ordinal()) != 0;
	}

	@Override
	public void clear() {
		for (SecurityKeyType keyType : DAYS_BEFORE_DEPRECATION.keySet()) {
			jdbcTemplate.update("DELETE FROM SECURITY_KEY WHERE DELETED = 1 OR TIMESTAMPDIFF(DAY, CREATION_TIME, NOW()) > ?",
					DAYS_BEFORE_DEPRECATION.get(keyType));	
		}
	}
}
