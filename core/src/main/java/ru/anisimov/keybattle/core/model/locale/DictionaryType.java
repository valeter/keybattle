package ru.anisimov.keybattle.core.model.locale;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         11/7/14
 */
public enum DictionaryType {
	HISTORY_ACTION_TYPE,			// 1
	GENDER,							// 2
	COUNTRY,						// 3
	
	WEB_ACTION_STATUS,				// 4
	WEB_ACTION_PARAM,				// 5
	WEB_ACTION_PARAM_VALUE,			// 6
	WEB_CAPTION,					// 7
	
	TEXT_COMPLEXITY_CLASS			// 8
}
