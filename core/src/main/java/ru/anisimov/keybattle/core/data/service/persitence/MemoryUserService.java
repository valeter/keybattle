package ru.anisimov.keybattle.core.data.service.persitence;

import ru.anisimov.keybattle.core.data.service.interfaces.impl.AbstractUserService;
import ru.anisimov.keybattle.core.model.user.User;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         10/22/14
 */
public class MemoryUserService extends AbstractUserService {
	private List<User> users = new ArrayList<>();

	@Override
	public boolean addUser(User newUser) {
		if (newUser != null && getUserByUserName(newUser.getUsername()) == null) {
			users.add(newUser);
			return true;
		}
		return false;
	}

	@Override
	public boolean removeUser(long userId) {
		return users.removeIf(user -> Objects.equals(userId, user.getId()));
	}
	
	@Override
	public List<User> getUsers() {
		return Collections.unmodifiableList(users);
	}

	@Override
	public User getUserById(long userId) {
		return users.stream()
				.filter(user -> Objects.equals(userId, user.getId()))
				.findFirst().orElse(null);
	}

	@Override
	public User getUserByUserName(String userName) {
		return users.stream()
				.filter(user -> Objects.equals(userName, user.getUsername()))
				.findFirst().orElse(null);
	}
}
