package ru.anisimov.keybattle.core.model.stat;

import ru.anisimov.keybattle.core.model.battle.BattleUser;

import java.time.LocalDateTime;
import java.util.UUID;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         19.09.15
 */
public class BattleStat {
	private UUID battleId;
	private BattleUser battleUser;
	private long textId;
	
	private int position;
	private boolean isFinished;
	private int totalWords;
	private int totalMistakes;
	private LocalDateTime startTime;
	private LocalDateTime endTime;

	public BattleStat(UUID battleId, BattleUser battleUser, long textId) {
		this.battleId = battleId;
		this.battleUser = battleUser;
		this.textId = textId;
		this.startTime = LocalDateTime.now();
	}

	public UUID getBattleId() {
		return battleId;
	}

	public void setBattleId(UUID battleId) {
		this.battleId = battleId;
	}

	public BattleUser getBattleUser() {
		return battleUser;
	}

	public void setBattleUser(BattleUser battleUser) {
		this.battleUser = battleUser;
	}

	public long getTextId() {
		return textId;
	}

	public void setTextId(long textId) {
		this.textId = textId;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public boolean isFinished() {
		return isFinished;
	}

	public void setIsFinished(boolean isFinished) {
		this.isFinished = isFinished;
	}

	public int getTotalWords() {
		return totalWords;
	}

	public void setTotalWords(int totalWords) {
		this.totalWords = totalWords;
	}

	public int getTotalMistakes() {
		return totalMistakes;
	}

	public void setTotalMistakes(int totalMistakes) {
		this.totalMistakes = totalMistakes;
	}

	public LocalDateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}

	public LocalDateTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalDateTime endTime) {
		this.endTime = endTime;
	}
}
