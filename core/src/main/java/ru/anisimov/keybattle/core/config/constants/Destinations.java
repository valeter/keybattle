package ru.anisimov.keybattle.core.config.constants;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         10/22/14
 */
public interface Destinations {
	/* ---------------------------------------------------- */
	/* common */
	
	String RESOURCES = "/resources";
	
	
	/* ---------------------------------------------------- */
	/* synchronous controllers */
	
	String INDEX = "/";
	String ERROR = "/error";
	String SIGN_IN = "/sign-in";
	String SIGN_IN_ERROR = SIGN_IN + "?" + Params.LOGIN_ERROR;
	String LOGOUT_SUCCESS = SIGN_IN + "?" + Params.LOGOUT_SUCCESS;
	String SIGN_UP_SUCCESS = SIGN_IN + "?" + Params.SIGNUP_SUCCESS;
	String SIGN_UP_CONFIRM_SUCCESS = SIGN_IN + "?" + Params.SIGNUP_CONFIRM_SUCCESS;
	String SIGN_UP_CONFIRM_FAIL = SIGN_IN + "?" + Params.SIGNUP_CONFIRM_FAIL;
	
	String SIGN_UP = "/sign-up";
	String SIGN_UP_CONFIRM = "/confirm";
	String PROFILE = "/profile";
	String PROFILE_UPDATE_INFO = PROFILE + "/update-info";
	String PROFILE_UPDATE_SETTINGS = PROFILE + "/update-settings";
	String PROFILE_SET_AVATAR = PROFILE + "/set-avatar";
	String PROFILE_CLEAR_AVATAR = PROFILE + "/clear-avatar";
	String PROFILE_SET_STATUS = PROFILE + "/set-status";

	String BATTLE = "/battle";
	String BATTLE_JOIN = "/battle/join";
	String BATTLE_ID = "/battle/{id}";
	String BATTLE_QUEUE = "/battle/queue";

	
	/* ---------------------------------------------------- */
	/* ajax api */
	
	String DICT = "/dict";

	String USER = "/user";
	String USER_ME = USER + "/me";
	String USER_INFO = USER + "/info";
	String USER_INFOS = USER + "/infos";
	String USER_AVATAR = USER + "/avatar";
	String USER_STATUS = USER + "/status";
	String USER_ROLES = USER + "/roles";
	String USER_HISTORY = USER + "/history";
	String USER_ADD_ROLE = USER + "/add-role";
	
	String TEXT = "/text";
	String TEXT_RANDOM = "/random";
	String TEXT_ID_PATH_VARIABLE = "textId";
	String TEXT_BY_ID = "/{" + TEXT_ID_PATH_VARIABLE + "}";
	String TEXT_VALUE = TEXT_BY_ID + "/value";
	String TEXT_INFO = TEXT_BY_ID + "/info";

	
	/* ---------------------------------------------------- */
	/* websocket api */

	String BATTLE_ID_STATE = "/battle/{id}/state";
	String BATTLE_ID_WORDS_SEND = "/battle/{id}/words/send";
	String BATTLE_ID_WORDS = "/battle/{id}/words";
	String BATTLE_ID_ERRORS = "/battle/{id}/errors";
	
	String BATTLE_QUEUE_USERS = "/battle/queue/users";
	String BATTLE_QUEUE_USER_LIST = "/battle/queue/userList";
	String BATTLE_QUEUE_SYSTEM = "/battle/queue/system";
	String BATTLE_QUEUE_SYSTEM_STATE = "/battle/queue/systemState";
	
	String BATTLE_ROOM = "/battle/room";
	String BATTLE_ROOM_CHAT = "/battle/room/chat";
	String BATTLE_ROOM_USERS = "/battle/room/users";
	String BATTLE_ROOM_USER_LIST = "/battle/room/userList";
	String BATTLE_ROOM_MESSAGES = "/battle/room/messages";
	String BATTLE_ROOM_MESSAGE_LIST = "/battle/room/messageList";
}
