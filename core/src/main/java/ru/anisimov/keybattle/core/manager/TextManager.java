package ru.anisimov.keybattle.core.manager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionTemplate;
import ru.anisimov.keybattle.core.HasTransactions;
import ru.anisimov.keybattle.core.data.service.interfaces.TextService;
import ru.anisimov.keybattle.core.model.locale.DictionaryLocale;
import ru.anisimov.keybattle.core.model.text.ModState;
import ru.anisimov.keybattle.core.model.text.Text;
import ru.anisimov.keybattle.core.model.text.TextInfo;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         04.05.15
 */
@Component
public class TextManager implements HasTransactions {
	private static final Logger log = LogManager.getLogger(TextManager.class);
	
	@Autowired
	private TransactionTemplate transactionTemplate;
	
	@Autowired
	private TextService textService;

	public Long addText(long authorId, DictionaryLocale locale, String textValue) {
		return getInTransactionWithoutException(
				() -> {
					Long textId = textService.addTextInfo(new TextInfo(authorId, locale));
					if (textId != null
						&& textService.addTextValue(textId, textValue)) {
						return textId;
					}
					return null;
				}, 
				String.format("Could not add text with params: authorId %d, locale %s, text length %s", 
						authorId, locale.name(), textValue == null ? "null" : String.valueOf(textValue.length()))
		);
	}

	public boolean moderateText(long textId, ModState modState) {
		return doInTransactionWithoutException(
				() -> textService.moderateText(textId, modState),
				String.format("Could not set mod_state of text %d to %s", textId, modState.name())
		);
	}

	public boolean removeText(long textId) {
		return doInTransactionWithoutException(
				() -> textService.removeTextValue(textId)
						&& textService.removeTextInfo(textId),
				String.format("Could not remove text %d", textId)
		);
	}

	public boolean changeText(long textId, String textValue) {
		return doInTransactionWithoutException(
				() -> textService.changeTextValue(textId, textValue),
				String.format("Could not change text %d, new text length %s",
						textId, textValue == null ? "null" : String.valueOf(textValue.length()))
		);
	}

	public Page<TextInfo> getTextInfos(ModState modState, Pageable pageable) {
		return getInTransactionWithoutException(
				() -> textService.getTextInfos(modState, pageable),
				String.format("Could not get text infos: mod state %s", modState.name())
		);
	}

	public Page<TextInfo> getTextInfos(DictionaryLocale locale, ModState modState, Pageable pageable) {
		return getInTransactionWithoutException(
				() -> textService.getTextInfos(locale, modState, pageable),
				String.format("Could not get text infos: mod state %s, locale %s", modState.name(), locale.name())
		);
	}

	public TextInfo getTextInfo(long textId) {
		return getInTransactionWithoutException(
				() -> textService.getTextInfo(textId),
				String.format("Could not get text info %d", textId)
		);
	}

	public Text getText(long textId) {
		return getInTransactionWithoutException(
				() -> textService.getText(textId),
				String.format("Could not get text %d", textId)
		);
	}

	public String getTextValue(long textId) {
		return getInTransactionWithoutException(
				() -> textService.getTextValue(textId),
				String.format("Could not get text value %d", textId)
		);
	}

	public Text getRandomText(DictionaryLocale locale) {
		ModState modState = ModState.APPROVED;
		return getInTransactionWithoutException(
				() -> textService.getRandomText(locale, modState),
				String.format("Could not get random text: locale %s, mod state %s", locale.name(), modState.name())
		);
	}

	public Page<TextInfo> getTextInfos(long authorId, Pageable pageable) {
		return getInTransactionWithoutException(
				() -> textService.getTextInfos(authorId, pageable),
				String.format("Could not get text infos: author id %d", authorId)
		);
	}

	@Override
	public TransactionTemplate getTransactionTemplate() {
		return transactionTemplate;
	}

	@Override
	public Logger getLogger() {
		return log;
	}
}
