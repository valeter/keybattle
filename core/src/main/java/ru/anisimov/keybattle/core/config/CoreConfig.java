package ru.anisimov.keybattle.core.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         10/7/14
 */
@Configuration
@ComponentScan(basePackages = {
		"ru.anisimov.keybattle.core.model", 
		"ru.anisimov.keybattle.core.remote",
		"ru.anisimov.keybattle.core.security"
})
public class CoreConfig {
	
}
