package ru.anisimov.keybattle.core.model.battle;

import ru.anisimov.keybattle.core.model.text.Text;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         19.09.15
 */
public class BattleInfo {
	private UUID id;
	private Text text;
	private Map<Long, BattleUser> battleUsers;
	private List<BattleUser> battleUsersLeft;
	private int usersFinished = 0;

	public BattleInfo(UUID id, Text text, List<BattleUser> users) {
		this.id = id;
		this.text = text;
		this.battleUsers = users.stream()
				.collect(Collectors.toMap(user -> user.getUser().getId(), user -> user));
		battleUsersLeft = new ArrayList<>(users);
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public Text getText() {
		return text;
	}

	public void setText(Text text) {
		this.text = text;
	}

	public Map<Long, BattleUser> getBattleUsers() {
		return battleUsers;
	}

	public void setBattleUsers(Map<Long, BattleUser> battleUsers) {
		this.battleUsers = battleUsers;
	}

	public List<BattleUser> getBattleUsersLeft() {
		return battleUsersLeft;
	}

	public void setBattleUsersLeft(List<BattleUser> battleUsersLeft) {
		this.battleUsersLeft = battleUsersLeft;
	}

	public int getUsersFinished() {
		return usersFinished;
	}

	public void setUsersFinished(int usersFinished) {
		this.usersFinished = usersFinished;
	}
	
	private Object[] keyArray() {
		return new Object[]{id};
	}
	
	@Override
	public int hashCode() {
		return Arrays.hashCode(keyArray());
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof BattleInfo)) {
			return false;
		}
	
		BattleInfo that = (BattleInfo) obj;
		return Arrays.equals(this.keyArray(), that.keyArray());
	}
	
	@Override
	public String toString() {
		return new StringBuilder()
				.append("BattleInfo: ")
				.append(Arrays.toString(keyArray()))
				.toString();
	}
}
