package ru.anisimov.keybattle.core.model.user.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         31.01.15
 */
@Documented
@Constraint(validatedBy = DobValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface Dob {
	String message() default "{ru.anisimov.keybattle.core.security.validation.Dob.message}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
