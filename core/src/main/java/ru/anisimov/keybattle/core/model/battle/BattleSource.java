package ru.anisimov.keybattle.core.model.battle;

import ru.anisimov.keybattle.core.model.locale.DictionaryLocale;
import ru.anisimov.keybattle.core.model.text.Text;
import ru.anisimov.keybattle.core.model.user.User;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         24.02.16
 */
public class BattleSource {
	private User user;
	private Integer size;
	private Text text;
	private DictionaryLocale locale;
	private Boolean needBots;

	public BattleSource() {
	}

	public BattleSource(User user, Integer size, Text text, DictionaryLocale locale, Boolean needBots) {
		this.user = user;
		this.size = size;
		this.text = text;
		this.locale = locale;
		this.needBots = needBots;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public Text getText() {
		return text;
	}

	public void setText(Text text) {
		this.text = text;
	}

	public DictionaryLocale getLocale() {
		return locale;
	}

	public void setLocale(DictionaryLocale locale) {
		this.locale = locale;
	}

	public Boolean getNeedBots() {
		return needBots;
	}

	public void setNeedBots(Boolean needBots) {
		this.needBots = needBots;
	}
}
