package ru.anisimov.keybattle.core.model.web;

import ru.anisimov.keybattle.core.HasDictionary;
import ru.anisimov.keybattle.core.model.locale.DictionaryType;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         13.03.15
 */
public enum WebCaption implements HasDictionary {
	OK,
	CANCEL;

	@Override
	public DictionaryType getDictionaryType() {
		return DictionaryType.WEB_CAPTION;
	}
}
