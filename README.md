# README #

***

## Setting up environment  

**environment variables**  
`export KEYBATTLE_ENVIRONMENT=local`  

**mysql**  
my.cnf file:  

```
#!properties

[client]
default-character-set=utf8  
[mysqld]  
character-set-server=utf8  
collation-server=utf8_general_ci

```