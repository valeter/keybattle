package ru.anisimov.keybattle.main.security.filter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.GenericFilterBean;
import ru.anisimov.keybattle.core.remote.AntiRobotService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.IOException;

import static ru.anisimov.keybattle.core.config.constants.Params.*;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         10/15/14
 */
@Component
public class AntiRobotFilter extends GenericFilterBean {
	@Autowired
	private AntiRobotService antiRobotService;

	@Override
	public void doFilter(ServletRequest request, 
						 ServletResponse response, 
						 FilterChain filterChain) throws IOException, ServletException {
		String challengeParam = request.getParameter(CAPTCHA_CHALLENGE_FIELD);
		
		if (!StringUtils.isEmpty(challengeParam)) {
			String remoteAddress = request.getRemoteAddr();
			String responseParam = request.getParameter(CAPTCHA_RESPONSE_FIELD);
			
			if (!StringUtils.isEmpty(responseParam)) {
				AntiRobotService.ValidationParams params = 
						new AntiRobotService.ValidationParams(remoteAddress, challengeParam, responseParam);
				if (antiRobotService.valid(params)) {
					filterChain.doFilter(request, response);
					return;
				}
			}
			
			ServletRequest modifiedRequest = new HttpServletRequestWrapper((HttpServletRequest)request) {
				@Override
				public String getParameter(String name) {
					if (CAPTCHA_CHALLENGE_FIELD.equals(name)) {
						return null;
					}
					return super.getParameter(name);
				}
			};
			modifiedRequest
					.getRequestDispatcher(getErrorUrl((HttpServletRequest) modifiedRequest))
					.forward(modifiedRequest, response);
			return;
		}
		filterChain.doFilter(request, response);
	}

	private String getErrorUrl(HttpServletRequest request) {
		String servletPath = request.getServletPath();
		String queryString = request.getQueryString();
		return servletPath + (StringUtils.isEmpty(queryString) ? "?" : "?" + queryString + "&") + RECAPTCHA_ERROR;
	}
}
