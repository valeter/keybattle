package ru.anisimov.keybattle.main.config;

import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import ru.anisimov.keybattle.core.config.CorePropertyPlaceholderConfig;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         11/12/14
 */
@Configuration
@PropertySource({
		"classpath:jdbc.properties",
		"classpath:application.properties"
})
@Import(CorePropertyPlaceholderConfig.class)
public class PropertyPlaceholderConfig {
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}
	
	@Bean
	public static PropertiesFactoryBean propertiesFactoryBean() {
		return new PropertiesFactoryBean();
	}
}
