package ru.anisimov.keybattle.main.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.anisimov.keybattle.core.Pair;
import ru.anisimov.keybattle.core.config.constants.ModelAttributes;
import ru.anisimov.keybattle.core.config.constants.Params;
import ru.anisimov.keybattle.core.manager.MailManager;
import ru.anisimov.keybattle.core.manager.SecurityKeyManager;
import ru.anisimov.keybattle.core.manager.UserManager;
import ru.anisimov.keybattle.core.model.user.FakeUsers;
import ru.anisimov.keybattle.core.model.user.Registration;
import ru.anisimov.keybattle.core.security.PasswordEncoding;
import ru.anisimov.keybattle.core.security.SecurityKeyType;

import javax.validation.Valid;
import java.util.Locale;

import static ru.anisimov.keybattle.core.config.constants.Destinations.*;
import static ru.anisimov.keybattle.core.config.constants.Params.RECAPTCHA_ERROR_VALUE;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         10/14/14
 */
@Controller
@RequestMapping(SIGN_UP)
public class RegistrationController {
	
}
