package ru.anisimov.keybattle.main.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.anisimov.keybattle.core.SortOrder;
import ru.anisimov.keybattle.core.config.constants.ModelAttributes;
import ru.anisimov.keybattle.core.config.constants.Params;
import ru.anisimov.keybattle.core.data.service.interfaces.DictionaryService;
import ru.anisimov.keybattle.core.manager.UserManager;
import ru.anisimov.keybattle.core.model.locale.DictionaryLocale;
import ru.anisimov.keybattle.core.model.locale.DictionaryType;
import ru.anisimov.keybattle.core.model.user.User;
import ru.anisimov.keybattle.core.model.user.UserInfo;
import ru.anisimov.keybattle.core.model.user.UserSettings;
import ru.anisimov.keybattle.core.model.web.WebActionParam;
import ru.anisimov.keybattle.core.model.web.WebActionParamValue;
import ru.anisimov.keybattle.core.model.web.WebActionResult;
import ru.anisimov.keybattle.core.model.web.WebActionStatus;
import ru.anisimov.keybattle.main.controller.interfaces.HasAjaxActions;
import ru.anisimov.keybattle.main.controller.interfaces.HasCalendar;

import javax.imageio.ImageIO;
import javax.validation.Valid;
import java.awt.image.BufferedImage;
import java.security.Principal;
import java.util.Locale;
import java.util.Map;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static ru.anisimov.keybattle.core.config.constants.Destinations.*;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         11/2/14
 */
@Controller
public class ProfileController implements HasAjaxActions, HasCalendar {
	private static final Logger log = LogManager.getLogger(ProfileController.class);
    
    private static final int MAX_STATUS_LENGTH = 140;

    @Autowired
    private UserManager userManager;
    @Autowired
    private DictionaryService dictionaryService;
    @Autowired
    private MessageSource messageSource;

    @RequestMapping(value = PROFILE, method = RequestMethod.GET)
    public String getMyProfile(Model model, Principal principal) {
        UserInfo userInfo = userManager.getUserInfo(((User) principal).getId());
		UserSettings userSettings = userManager.getUserSettings(((User) principal).getId());

		model.addAttribute(ModelAttributes.EDITABLE, true);
        model.addAttribute(ModelAttributes.USER_INFO, userInfo);
		model.addAttribute(ModelAttributes.USER_SETTINGS, userSettings);
        
        return PROFILE;
    }

	@RequestMapping(value = PROFILE + "/{userName}", method = RequestMethod.GET)
	public String getProfile(@PathVariable String userName, Model model) {
		User user = userManager.getUserByUserName(userName);
		if (user != null) {
			UserInfo userInfo = userManager.getUserInfo(user.getId());
	
			if (!userInfo.isHidden()) {
				model.addAttribute(ModelAttributes.EDITABLE, false);
				if (userInfo.isEmailHidden()) {
					userInfo.setEmail(null);
				}
				model.addAttribute(ModelAttributes.USER_INFO, userInfo);
			}
		}
		return PROFILE;
	}

    @RequestMapping(value = PROFILE_UPDATE_INFO, method = RequestMethod.POST, params = { Params.SAVE_ACTION })
    public String updateMyInfo(
			@Valid UserInfo userInfo, 
            BindingResult result, 
            Principal principal) {
        if (result.hasErrors()) {
            return PROFILE;
        }
        
        long userId = ((User)principal).getId();
        if (!userManager.changeUserInfo(userId, userInfo, userId)) {
            throw new RuntimeException("Could not save user info");
        }
        return PROFILE;
    }
    
    @RequestMapping(value = PROFILE_UPDATE_INFO, method = RequestMethod.POST, params = { Params.DELETE_ACTION })
    public String deleteMyInfo(Principal principal) {
        long userId = ((User)principal).getId();
        if (!userManager.clearUserInfo(userId, userId)) {
            throw new RuntimeException("Could not clear user info");
        }
        
        return "redirect:" + PROFILE;
    }

	@RequestMapping(value = PROFILE_UPDATE_SETTINGS, method = RequestMethod.POST, params = { Params.SAVE_ACTION })
	public String updateMySettings(
			@Valid UserSettings userSettings,
			BindingResult result,
			Principal principal) {
		if (result.hasErrors()) {
			return PROFILE;
		}

		long userId = ((User)principal).getId();
		if (!userManager.changeUserSettings(userId, userSettings, userId)) {
			throw new RuntimeException("Could not save user settings");
		}
		return PROFILE;
	}

	@RequestMapping(value = PROFILE_UPDATE_SETTINGS, method = RequestMethod.POST, params = { Params.DELETE_ACTION })
	public String deleteMySettings(Principal principal) {
		long userId = ((User)principal).getId();
		if (!userManager.clearUserSettings(userId, userId)) {
			throw new RuntimeException("Could not clear user settings");
		}

		return "redirect:" + PROFILE;
	}
    
    @RequestMapping(value = PROFILE_SET_STATUS, method = RequestMethod.POST, produces = APPLICATION_JSON_VALUE)
    public @ResponseBody
    WebActionResult updateMyStatus(
			@RequestParam(value = Params.STATUS, required = false) String status, 
            Authentication auth,
            @ModelAttribute UserInfo userInfo,
            java.util.Locale locale) {
        return makeAjaxAction(() -> {
            if (status != null && status.length() > MAX_STATUS_LENGTH) {
                return new WebActionResult(WebActionStatus.ERROR)
                        .addParam(WebActionParam.REASON, 
                                messageSource.getMessage("status.size", new Object[0], locale));
            }
            long userId = ((User)auth.getPrincipal()).getId();
            if (!userManager.changeUserStatus(userId, status, userId)) {
                userInfo.setStatus(status);
                return new WebActionResult(WebActionStatus.ERROR)
                        .addParam(WebActionParam.REASON, WebActionParamValue.SERVER_ERROR);
            }
            return new WebActionResult(WebActionStatus.SUCCESS);
        });
    }

    @RequestMapping(value = PROFILE_SET_AVATAR, method = RequestMethod.POST, produces = APPLICATION_JSON_VALUE)
    public @ResponseBody WebActionResult updateMyAvatar(@RequestParam(Params.AVATAR) MultipartFile avatar, 
														Authentication authentication) {
        return makeAjaxAction(() -> {
            if (avatar.isEmpty()) {
                return new WebActionResult(WebActionStatus.ERROR)
                        .addParam(WebActionParam.REASON, WebActionParamValue.EMPTY_FILE);
            }
            
            Long userId = ((User)authentication.getPrincipal()).getId();
			BufferedImage image = ImageIO.read(avatar.getInputStream());
			
            if (!userManager.changeUserAvatar(userId, image, userId)) {
                return new WebActionResult(WebActionStatus.ERROR)
                        .addParam(WebActionParam.REASON, WebActionParamValue.SERVER_ERROR)
                        .addParam(WebActionParam.HINT, WebActionParamValue.WRONG_FILE_TYPE);
            }
            return new WebActionResult(WebActionStatus.SUCCESS);
        });
    }

	@RequestMapping(value = PROFILE_CLEAR_AVATAR, method = RequestMethod.POST, produces = APPLICATION_JSON_VALUE)
	public @ResponseBody WebActionResult clearMyAvatar(Authentication authentication) {
		return makeAjaxAction(() -> {
			Long userId = ((User)authentication.getPrincipal()).getId();

			if (!userManager.clearUserAvatar(userId, userId)) {
				return new WebActionResult(WebActionStatus.ERROR)
						.addParam(WebActionParam.REASON, WebActionParamValue.SERVER_ERROR);
			}
			return new WebActionResult(WebActionStatus.SUCCESS);
		});
	}
	
    @ModelAttribute(ModelAttributes.GENDER_DICT)
    public Map<Long, String> genderDictionary(Locale locale) {
        return dictionaryService.getDictionary(DictionaryLocale.getDictLocale(locale),
                DictionaryType.GENDER, SortOrder.KEY);
    }

    @ModelAttribute(ModelAttributes.COUNTRY_DICT)
    public Map<Long, String> countryDictionary(Locale locale) {
        return dictionaryService.getDictionary(DictionaryLocale.getDictLocale(locale),
                DictionaryType.COUNTRY, SortOrder.VALUE);
    }

    @Override
    public DictionaryService getDictionaryService() {
        return dictionaryService;
    }
}
