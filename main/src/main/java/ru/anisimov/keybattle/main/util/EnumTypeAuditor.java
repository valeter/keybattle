package ru.anisimov.keybattle.main.util;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import ru.anisimov.keybattle.core.exception.ConsistencyException;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         11/2/14
 */
public class EnumTypeAuditor<T extends Enum> {
    private static final String VALUES_METHOD_NAME = "values";
    private static final String DEFAULT_ID_COLUMN_NAME = "ID";
    private static final String DEFAULT_NAME_COLUMN_NAME = "NAME";

    private JdbcTemplate jdbcTemplate;

    private T enumType;
    private String tableName;
    private String idColumnName;
    private String nameColumnName;
    private List<String> otherColumnNames;

    public EnumTypeAuditor(JdbcTemplate jdbcTemplate, T enumType, String tableName) {
        this(jdbcTemplate, enumType, tableName, DEFAULT_ID_COLUMN_NAME, DEFAULT_NAME_COLUMN_NAME);
    }

    public EnumTypeAuditor(JdbcTemplate jdbcTemplate, T enumType,
                           String tableName, String idColumnName, String nameColumnName, String... otherColumnNames) {
        this.jdbcTemplate = jdbcTemplate;
        this.enumType = enumType;
        this.tableName = tableName;
        this.idColumnName = idColumnName;
        this.nameColumnName = nameColumnName;
        this.otherColumnNames = Arrays.asList(otherColumnNames);
        checkConsistency();
    }

    private RowMapper<T> getRowMapper() {
        return (resultSet, i) -> {
            int id = resultSet.getInt(idColumnName);
            String name = resultSet.getString(nameColumnName);
            T result = getEnumMembers()[id];
            return Objects.equals(result.name(), name) ? result : null;
        };
    }

    public void checkConsistency() {
        T[] enumMembers = getEnumMembers();
        T[] tableRows = jdbcTemplate.query(constructSelect(), getRowMapper())
                .toArray(enumMembers);
        Arrays.sort(enumMembers);
        Arrays.sort(tableRows);
        if (!Arrays.equals(enumMembers, tableRows)) {
            throw new ConsistencyException(String.format("Error in table %s mapped to enum %s: wrong rows %s",
                    tableName, enumType.getClass().getName(), Arrays.toString(tableRows)));
        }
    }

    private T[] getEnumMembers() {
        Method valuesMethod = Arrays
                .stream(enumType.getClass().getMethods())
                .filter(method -> VALUES_METHOD_NAME.equals(method.getName()))
                .findFirst().orElse(null);

        try {
            return (T[]) valuesMethod.invoke(null);
        } catch (InvocationTargetException | IllegalAccessException e) {
            // never happens
        }
        return null;
    }

    private String constructSelect() {
        StringBuilder result = new StringBuilder("SELECT ")
                .append(idColumnName).append(", ")
                .append(nameColumnName);
        otherColumnNames.forEach(columnName -> result.append(", ").append(columnName));
        return result.append(" FROM ").append(tableName).toString();
    }
}
