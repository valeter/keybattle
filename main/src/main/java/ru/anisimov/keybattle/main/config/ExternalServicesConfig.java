package ru.anisimov.keybattle.main.config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         03.10.15
 */
@Configuration
public class ExternalServicesConfig {
	private static final Logger log = LogManager.getLogger(ExternalServicesConfig.class);
	
	@Bean
	public RestTemplate battleRestTemplate() {
		RestTemplate result = new RestTemplate();
		result.setInterceptors(Collections.singletonList(
				(request, body, execution) -> {
					ClientHttpResponse response = execution.execute(request, body);
					log.debug(new String(body));
					return response;
				}
		));
		return result;
	}
}
