package ru.anisimov.keybattle.main.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;
import ru.anisimov.keybattle.core.config.constants.Destinations;
import ru.anisimov.keybattle.core.config.constants.ModelAttributes;
import ru.anisimov.keybattle.core.model.user.User;

import static ru.anisimov.keybattle.core.config.constants.Destinations.BATTLE_QUEUE;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         16.06.15
 */
@Controller
public class BattleQueueController {
	@Autowired
	private RestTemplate battleRestTemplate;
	@Value("${battle.server.host}")
	private String battleServerHost;
	@Value("${battle.server.port}")
	private String battleServerPort;
	
	@RequestMapping(value = BATTLE_QUEUE, method = RequestMethod.GET)
	public String queue(Authentication authentication, Model model) {
		User user = (User)authentication.getPrincipal();
		ResponseEntity<String> battleIdEntity = battleRestTemplate.postForEntity("http://" + 
				battleServerHost + ":" + battleServerPort + Destinations.BATTLE_JOIN, user.getId(), String.class);
		if (battleIdEntity.getStatusCode() != HttpStatus.OK) {
			throw new RuntimeException("Battle server returned response status code " + battleIdEntity.getStatusCode());
		}
		model.addAttribute(ModelAttributes.BATTLE_ID, battleIdEntity.getBody());
		return "battle-queue";
	}
}
