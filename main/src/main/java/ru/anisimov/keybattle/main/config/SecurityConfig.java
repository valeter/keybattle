package ru.anisimov.keybattle.main.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.header.writers.frameoptions.XFrameOptionsHeaderWriter;
import ru.anisimov.keybattle.core.data.service.interfaces.UserService;
import ru.anisimov.keybattle.core.security.CredentialsLoader;
import ru.anisimov.keybattle.core.security.PasswordEncoding;
import ru.anisimov.keybattle.main.security.filter.AntiRobotFilter;

import javax.sql.DataSource;

import static ru.anisimov.keybattle.core.config.constants.Destinations.*;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         10/10/14
 */
@Configuration
@EnableWebSecurity
@ComponentScan(basePackages = {"ru.anisimov.keybattle.core.security", "ru.anisimov.keybattle.main.security"})
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	// two weeks
	private static final int REMEMBER_ME_TOKEN_VALIDITY_SECONDS = 3600 * 24 * 14;
	
	@Autowired
	private AntiRobotFilter antiRobotFilter;
	@Autowired
	private UserService userService;
	@Autowired
	private CredentialsLoader credentialsLoader;
	@Autowired
	private DataSource dataSource;

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth
			.userDetailsService(userService)
			.passwordEncoder(PasswordEncoding.passwordEncoder());
	}

	/* чтобы spring boot не добавлял автоматически его в цепочку фильтров */
	@Bean
	public FilterRegistrationBean antiRobotFilterRegistrationBean () {
		FilterRegistrationBean registrationBean = new FilterRegistrationBean();
	
		registrationBean.setFilter(antiRobotFilter);
		registrationBean.setEnabled(false);

		return registrationBean;
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.headers()
				.addHeaderWriter(new XFrameOptionsHeaderWriter(
						XFrameOptionsHeaderWriter.XFrameOptionsMode.SAMEORIGIN
				))
				.and()
			.authorizeRequests()
				.antMatchers(RESOURCES + "**").permitAll()
				.antMatchers(SIGN_IN + "*").permitAll()
				.antMatchers(SIGN_UP + "*").permitAll()
				.and()
			.formLogin()
				.loginPage(SIGN_IN).permitAll()
				.loginProcessingUrl("/auth/login_check")
				.failureUrl(SIGN_IN_ERROR).permitAll()
				.defaultSuccessUrl(INDEX)
				.and()
			.rememberMe()
				.key(credentialsLoader.rememberMe())
				.tokenRepository(persistentTokenRepository())
				.tokenValiditySeconds(REMEMBER_ME_TOKEN_VALIDITY_SECONDS)
				.and()
			.logout().permitAll()
				.logoutSuccessUrl(LOGOUT_SUCCESS)
				.deleteCookies("JSESSIONID")
				.and()
			.addFilterAfter(antiRobotFilter, CsrfFilter.class);
	}

	@Bean
	public PersistentTokenRepository persistentTokenRepository() {
		JdbcTokenRepositoryImpl tokenRepository = new JdbcTokenRepositoryImpl();
		tokenRepository.setDataSource(dataSource);
		return tokenRepository;
	}

	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}
}
