package ru.anisimov.keybattle.main.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import ru.anisimov.keybattle.core.config.constants.ModelAttributes;
import ru.anisimov.keybattle.core.exception.AjaxException;
import ru.anisimov.keybattle.core.model.web.WebActionParam;
import ru.anisimov.keybattle.core.model.web.WebActionParamValue;
import ru.anisimov.keybattle.core.model.web.WebActionResult;
import ru.anisimov.keybattle.core.model.web.WebActionStatus;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         03.10.15
 */
@ControllerAdvice
public class ExceptionController {
	private static final Logger log = LogManager.getLogger(ExceptionController.class);
	
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Ajax error")
	@ExceptionHandler(AjaxException.class)
	@ResponseBody 
	public WebActionResult handleAjaxException(HttpServletRequest request, Exception e) {
		log.debug(String.format("Ajax request %s raised exception",
				request.getRequestURL().toString()), e);
		return new WebActionResult(WebActionStatus.ERROR)
				.addParam(WebActionParam.REASON, WebActionParamValue.UNKNOWN_EXCEPTION)
				.addParam(WebActionParam.EXCEPTION, e.getLocalizedMessage());
	}

	@ExceptionHandler(Exception.class)
	public ModelAndView handleException(HttpServletRequest request, Exception e) {
		log.debug(String.format("Request %s raised exception", request.getRequestURL()), e);
		String exception = null;
		try (ByteArrayOutputStream out = new ByteArrayOutputStream(); 
			PrintWriter pw = new PrintWriter(out)) {
			e.printStackTrace(pw);
			pw.flush(); 
			out.flush();
			exception = new String(out.toByteArray());
		} catch (IOException ignore) {
		}
		ModelAndView result = new ModelAndView("error");
		result.addObject(ModelAttributes.EXCEPTION, exception);
		result.addObject(ModelAttributes.URL, request.getRequestURL());
		result.addObject(ModelAttributes.STATUS, "500");
		return result;
	}
}
