package ru.anisimov.keybattle.main.controller.interfaces;

import com.google.common.base.Strings;
import org.springframework.web.bind.annotation.ModelAttribute;
import ru.anisimov.keybattle.core.config.constants.ModelAttributes;

import java.text.DateFormatSymbols;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         16.04.15
 */
public interface HasCalendar {
	@ModelAttribute(ModelAttributes.WEEKDAYS)
	default List<String> weekdays(Locale locale) {
		return Arrays.asList(new DateFormatSymbols(locale).getWeekdays()).stream()
				.filter((string) -> !Strings.isNullOrEmpty(string)).collect(Collectors.toList());
	}
	
	@ModelAttribute(ModelAttributes.SHORT_WEEKDAYS)
	default List<String> shortWeekdays(Locale locale) {
		return Arrays.asList(new DateFormatSymbols(locale).getShortWeekdays()).stream()
				.filter((string) -> !Strings.isNullOrEmpty(string)).collect(Collectors.toList());
	}

	@ModelAttribute(ModelAttributes.MONTHS)
	default List<String> months(Locale locale) {
		return Arrays.asList(new DateFormatSymbols(locale).getMonths()).stream()
				.filter((string) -> !Strings.isNullOrEmpty(string)).collect(Collectors.toList());
	}
	
	@ModelAttribute(ModelAttributes.SHORT_MONTHS)
	default List<String> shortMonths(Locale locale) {
		return Arrays.asList(new DateFormatSymbols(locale).getShortMonths()).stream()
				.filter((string) -> !Strings.isNullOrEmpty(string)).collect(Collectors.toList());
	}
}
