package ru.anisimov.keybattle.main.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.ChannelRegistration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import ru.anisimov.keybattle.core.websocket.interceptor.UserOnlineInterceptor;

import static ru.anisimov.keybattle.core.config.constants.Destinations.BATTLE_ROOM;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         10/20/14
 */
@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig extends AbstractWebSocketMessageBrokerConfigurer {
	private static final String DESTINATION_PREFIX = "/app";
	
	private UserOnlineInterceptor userOnlineInterceptor;

	@Override
	public void configureMessageBroker(MessageBrokerRegistry config) {
		config.enableSimpleBroker(
				BATTLE_ROOM + "/"
		);
		config.setApplicationDestinationPrefixes(DESTINATION_PREFIX);
	}

	@Override
	public void registerStompEndpoints(StompEndpointRegistry registry) {
		registry.addEndpoint(BATTLE_ROOM).withSockJS();
	}

	@Override
	public void configureClientInboundChannel(ChannelRegistration registration) {
		registration.setInterceptors(userOnlineInterceptor());
	}

	@Bean
	public UserOnlineInterceptor userOnlineInterceptor() {
		userOnlineInterceptor = new UserOnlineInterceptor();
		return userOnlineInterceptor;
	}
}
