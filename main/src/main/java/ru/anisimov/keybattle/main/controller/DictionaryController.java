package ru.anisimov.keybattle.main.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.anisimov.keybattle.core.SortOrder;
import ru.anisimov.keybattle.core.data.service.interfaces.DictionaryService;
import ru.anisimov.keybattle.core.model.locale.DictionaryLocale;
import ru.anisimov.keybattle.core.model.locale.DictionaryType;

import java.util.Locale;
import java.util.Map;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static ru.anisimov.keybattle.core.config.constants.Destinations.DICT;
import static ru.anisimov.keybattle.core.config.constants.Params.SORT_BY;
import static ru.anisimov.keybattle.core.config.constants.Params.TYPE;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         11/2/14
 */
@Controller
public class DictionaryController {
    @Autowired
    public DictionaryService dictionaryService;

    @RequestMapping(value = DICT, method = GET, produces = APPLICATION_JSON_VALUE)
    public @ResponseBody Map<Long, String> getDict(
            @RequestParam(TYPE) Integer typeId,
            @RequestParam(value = SORT_BY, required = false) String sortBy,
            Locale locale) {
        try {
            SortOrder order = sortBy == null ? null : SortOrder.valueOf(sortBy);
            return dictionaryService.getDictionary(DictionaryLocale.getDictLocale(locale),
                    DictionaryType.values()[typeId], order);
        } catch (ArrayIndexOutOfBoundsException | IllegalArgumentException e) {
            return null;
        }
    }
}
