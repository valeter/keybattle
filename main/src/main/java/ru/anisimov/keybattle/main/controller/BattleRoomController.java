package ru.anisimov.keybattle.main.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.anisimov.keybattle.core.manager.ChatLogManager;
import ru.anisimov.keybattle.core.exception.ApplicationError;
import ru.anisimov.keybattle.core.exception.ApplicationErrorType;
import ru.anisimov.keybattle.core.model.chat.*;
import ru.anisimov.keybattle.core.websocket.action.SendToUserWebsocketMessage;
import ru.anisimov.keybattle.core.websocket.action.SendWebsocketMessage;

import java.security.Principal;
import java.util.List;

import static ru.anisimov.keybattle.core.config.constants.Destinations.*;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         10/20/14
 */
@Controller
public class BattleRoomController {
	@Autowired
	private SimpMessagingTemplate messagingTemplate;

	@Autowired
	private ChatLogManager chatLogManager;
/*
	@Autowired
	private UsersOnlineManager usersOnlineManager;
*/

	@RequestMapping(value = BATTLE_ROOM, method = RequestMethod.GET)
	public String showRoom() {
		return BATTLE_ROOM;
	}

	@MessageMapping(BATTLE_ROOM_USER_LIST)
	@SendTo(BATTLE_ROOM_USERS)
	public UserEvent getUsers() throws Exception {
		return null;/*new UserEvent(new Date(), USER_LIST, 
				usersOnlineManager.getUsers(UsersOnlineManager.PersistentConnection.BATTLE_ROOM));*/
	}

	@MessageMapping(BATTLE_ROOM_MESSAGE_LIST)
	@SendToUser(value = BATTLE_ROOM_CHAT, broadcast = false)
	public MultiChatEvent getLog(UserFilter userFilter, Principal principal) throws Exception {
		List<ChatEvent> log = chatLogManager.getLog(principal.getName(), userFilter);
		return null; /*new MultiChatEvent(new Date(), CHAT_LIST,
				log.stream()
						.sorted(Comparator.comparing(ChatEvent::getEventtime))
						.collect(Collectors.toList())
		);*/
	}

	@MessageMapping(BATTLE_ROOM_MESSAGES)
	public void userSay(ChatMessage message, Principal principal) throws Exception {
		UserFilter recipients = message.getRecipients();
		/*ChatEvent event = new ChatEvent(new Date(), CHAT_MESSAGE,
				Arrays.asList(new ChatMessage(principal.getName(), message.getText(), recipients))
		);
		sendMessage(event, BATTLE_ROOM_CHAT, recipients.getUsers());
		chatLogManager.add(event);*/
	}

	@MessageExceptionHandler
	@SendToUser(value= BATTLE_ROOM_CHAT, broadcast=false)
	public ApplicationError handleException(Exception exception) {
		return new ApplicationError("", ApplicationErrorType.ALERT);
	}

	private void sendMessage(Object event, String destination, List<String> userNames) {
		if (userNames == null || userNames.size() == 0) {
			new SendWebsocketMessage(messagingTemplate, destination, () -> event).make();
		} else {
			for (String userName : userNames) {
				new SendToUserWebsocketMessage(messagingTemplate, destination, userName, () -> event).make();
			}
		}
	}
}
