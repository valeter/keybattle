package ru.anisimov.keybattle.main.controller.interfaces;

import org.springframework.web.bind.annotation.ModelAttribute;
import ru.anisimov.keybattle.core.SortOrder;
import ru.anisimov.keybattle.core.data.service.interfaces.DictionaryService;
import ru.anisimov.keybattle.core.exception.AjaxException;
import ru.anisimov.keybattle.core.model.locale.DictionaryLocale;
import ru.anisimov.keybattle.core.model.locale.DictionaryType;
import ru.anisimov.keybattle.core.config.constants.ModelAttributes;
import ru.anisimov.keybattle.core.model.web.WebActionResult;

import java.util.Locale;
import java.util.Map;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         11/2/14
 */
public interface HasAjaxActions {
    default WebActionResult makeAjaxAction(AjaxAction action) {
        try {
            return action.make();
        } catch (Throwable e) {
            throw new AjaxException(e);
        }
    }
    
    @ModelAttribute(ModelAttributes.WEB_ACTION_STATUS_DICT)
    default Map<Long, String> webActionStatusDict(Locale locale) {
        return getDictionaryService().getDictionary(DictionaryLocale.getDictLocale(locale),
                DictionaryType.WEB_ACTION_STATUS, null);
    }

    @ModelAttribute(ModelAttributes.WEB_ACTION_PARAM_DICT)
    default Map<Long, String> webActionParamDict(Locale locale) {
        return getDictionaryService().getDictionary(DictionaryLocale.getDictLocale(locale),
                DictionaryType.WEB_ACTION_PARAM, null);
    }

    @ModelAttribute(ModelAttributes.WEB_ACTION_PARAM_VALUE_DICT)
    default Map<Long, String> webActionParamValueDict(Locale locale) {
        return getDictionaryService().getDictionary(DictionaryLocale.getDictLocale(locale),
                DictionaryType.WEB_ACTION_PARAM_VALUE, null);
    }

	@ModelAttribute(ModelAttributes.WEB_CAPTION_DICT)
	default Map<Long, String> webCaptionDictionary(Locale locale) {
		return getDictionaryService().getDictionary(DictionaryLocale.getDictLocale(locale),
				DictionaryType.WEB_CAPTION, SortOrder.KEY);
	}

    DictionaryService getDictionaryService();
    
    interface AjaxAction {
        WebActionResult make() throws Throwable;
    }
}
