package ru.anisimov.keybattle.main.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import ru.anisimov.keybattle.core.config.constants.Destinations;
import ru.anisimov.keybattle.core.config.constants.Params;
import ru.anisimov.keybattle.core.data.service.interfaces.DictionaryService;
import ru.anisimov.keybattle.core.manager.TextManager;
import ru.anisimov.keybattle.core.model.locale.DictionaryLocale;
import ru.anisimov.keybattle.core.model.text.ModState;
import ru.anisimov.keybattle.core.model.text.Text;
import ru.anisimov.keybattle.core.model.text.TextInfo;
import ru.anisimov.keybattle.core.model.user.User;
import ru.anisimov.keybattle.core.model.web.WebActionResult;
import ru.anisimov.keybattle.core.model.web.WebActionStatus;
import ru.anisimov.keybattle.main.controller.interfaces.HasAjaxActions;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         04.05.15
 */
@RestController
@RequestMapping(
		value = Destinations.TEXT,
		consumes = MediaType.TEXT_PLAIN_VALUE,
		produces = MediaType.APPLICATION_JSON_VALUE
)
public class TextController implements HasAjaxActions {
	private static final Logger log = LogManager.getLogger(TextController.class);
	
	@Autowired
	private DictionaryService dictionaryService;
	
	@Autowired
	private TextManager textManager;

	@RequestMapping(method = RequestMethod.POST,
			params = {Params.LOCALE_ID, Params.TEXT_VALUE})
	public Long addText(
			@RequestParam(Params.LOCALE_ID) DictionaryLocale locale,
			@RequestParam(Params.TEXT_VALUE) String textValue,
			Authentication authentication) {
		long userId = ((User)authentication.getPrincipal()).getId();
		return textManager.addText(userId, locale, textValue);
	}

	@RequestMapping(value = Destinations.TEXT_BY_ID, 
			method = RequestMethod.PATCH,
			params = {Params.TEXT_MOD_STATE})
	public boolean moderateText(
			@PathVariable(Destinations.TEXT_ID_PATH_VARIABLE) long textId, 
			@RequestParam(Params.TEXT_MOD_STATE) ModState modState) {
		return textManager.moderateText(textId, modState);
	}

	@RequestMapping(value = Destinations.TEXT_BY_ID, 
			method = RequestMethod.DELETE)
	public boolean removeText(
			@PathVariable(Destinations.TEXT_ID_PATH_VARIABLE) long textId) {
		return textManager.removeText(textId);
	}

	@RequestMapping(value = Destinations.TEXT_BY_ID, 
			method = RequestMethod.PUT,
			params = {Params.TEXT_VALUE})
	public WebActionResult changeText(
			@PathVariable(Destinations.TEXT_ID_PATH_VARIABLE) long textId,
			@RequestParam(Params.TEXT_VALUE) String textValue) {
		return makeAjaxAction(() -> {
			if (!textManager.changeText(textId, textValue)) {
				return new WebActionResult(WebActionStatus.ERROR);
			}
			
			return new WebActionResult(WebActionStatus.SUCCESS);
		});
	}

	@RequestMapping(method = RequestMethod.GET,
			params = {Params.TEXT_MOD_STATE})
	public Page<TextInfo> getTextInfos(
			@RequestParam(Params.TEXT_MOD_STATE) ModState modState, 
			Pageable pageable) {
		return textManager.getTextInfos(modState, pageable);
	}

	@RequestMapping(method = RequestMethod.GET,
			params = {Params.LOCALE_ID, Params.TEXT_MOD_STATE})
	public Page<TextInfo> getTextInfos(
			@RequestParam(Params.LOCALE_ID) DictionaryLocale locale,
			@RequestParam(Params.TEXT_MOD_STATE) ModState modState, 
			Pageable pageable) {
		return textManager.getTextInfos(locale, modState, pageable);
	}

	@RequestMapping(method = RequestMethod.GET,
			params = {Params.USER_ID})
	public Page<TextInfo> getTextInfos(
			@RequestParam(Params.USER_ID) long authorId, 
			Pageable pageable) {
		return textManager.getTextInfos(authorId, pageable);
	}

	@RequestMapping(value = Destinations.TEXT_INFO,
			method = RequestMethod.GET)
	public TextInfo getTextInfo(
			@PathVariable(Destinations.TEXT_ID_PATH_VARIABLE) long textId) {
		return textManager.getTextInfo(textId);
	}

	@RequestMapping(value = Destinations.TEXT_BY_ID,
			method = RequestMethod.GET)
	public Text getText(
			@PathVariable(Destinations.TEXT_ID_PATH_VARIABLE) long textId) {
		return textManager.getText(textId);
	}

	@RequestMapping(value = Destinations.TEXT_RANDOM,
			method = RequestMethod.GET,
			params = {Params.LOCALE_ID, Params.TEXT_MOD_STATE})
	public Text getRandomText(
			@RequestParam(Params.LOCALE_ID) DictionaryLocale locale) {
		return textManager.getRandomText(locale);
	}

	@RequestMapping(value = Destinations.TEXT_VALUE,
			method = RequestMethod.GET,
			produces = MediaType.TEXT_PLAIN_VALUE)
	public String getTextValue(
			@PathVariable(Destinations.TEXT_ID_PATH_VARIABLE) long textId) {
		return textManager.getTextValue(textId);
	}

	@Override
	public DictionaryService getDictionaryService() {
		return dictionaryService;
	}
}
