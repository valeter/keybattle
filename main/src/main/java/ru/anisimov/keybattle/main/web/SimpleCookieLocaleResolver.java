package ru.anisimov.keybattle.main.web;

import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         14.04.15
 */
public class SimpleCookieLocaleResolver extends CookieLocaleResolver {
	private SessionLocaleResolver sessionLocaleResolver = new SessionLocaleResolver();

	@Override
	public void setLocale(HttpServletRequest request, HttpServletResponse response, Locale locale) {
		super.setLocale(request, response, locale);
		sessionLocaleResolver.setLocale(request, response, locale);
	}
	
	@Override
	protected Locale determineDefaultLocale(HttpServletRequest request) {
		return sessionLocaleResolver.resolveLocale(request);
	}

	@Override
	public void setDefaultLocale(Locale defaultLocale) {
		sessionLocaleResolver.setDefaultLocale(defaultLocale);
	}
}
