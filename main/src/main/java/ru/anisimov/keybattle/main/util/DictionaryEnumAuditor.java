package ru.anisimov.keybattle.main.util;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import ru.anisimov.keybattle.core.HasDictionary;
import ru.anisimov.keybattle.core.Pair;
import ru.anisimov.keybattle.core.exception.ConsistencyException;
import ru.anisimov.keybattle.core.model.locale.DictionaryLocale;
import ru.anisimov.keybattle.core.model.locale.DictionaryType;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         11/2/14
 */
public class DictionaryEnumAuditor<T extends Enum & HasDictionary> {
    private static final String VALUES_METHOD_NAME = "values";
    private static final String TABLE_NAME = "DICT";
    private static final String TYPE_ID_COLUMN_NAME = "TYPE_ID";
    private static final String TERM_ID_COLUMN_NAME = "TERM_ID";
    private static final String LOCALE_ID_COLUMN_NAME = "LOCALE_ID";
    private static final String TEXT_COLUMN_NAME = "VALUE";

    private JdbcTemplate jdbcTemplate;

    private T enumType;

    public DictionaryEnumAuditor(JdbcTemplate jdbcTemplate, T enumType) {
        this.jdbcTemplate = jdbcTemplate;
        this.enumType = enumType;
        checkConsistency();
    }

    private RowMapper<Pair<T, DictionaryLocale>> getRowMapper() {
        return (resultSet, i) -> {
            int id = resultSet.getInt(TERM_ID_COLUMN_NAME);
            int localeId = resultSet.getInt(LOCALE_ID_COLUMN_NAME);
            return nvl(resultSet.getString(TEXT_COLUMN_NAME)).trim().length() == 0
                    ? null
                    : new Pair<>(getEnumMembers()[id], DictionaryLocale.values()[localeId]);
        };
    }

    private String nvl(String string) {
        return string == null ? "" : string;
    }

    public void checkConsistency() {
        T[] enumMembers = getEnumMembers();
        List<Pair<T, DictionaryLocale>> tableRowsList = jdbcTemplate.query(constructSelect(enumType.getDictionaryType()), getRowMapper());
        int localizedEnumMembersCount = enumMembers.length * DictionaryLocale.values().length;
        if (tableRowsList.size() != localizedEnumMembersCount) {
            throw new ConsistencyException(String.format("Error in enum dictionary mapped to enum %s: wrong row count %d, need %d",
                    enumType.getClass().getName(), tableRowsList.size(), localizedEnumMembersCount));
        }
        if (tableRowsList.stream().anyMatch(item -> item == null)) {
            throw new ConsistencyException(String.format("Error in enum dictionary mapped to enum %s: one of the rows is empty",
                    enumType.getClass().getName()));
        }
        T[] tableRows = tableRowsList.stream()
                .filter(tableRow -> tableRow != null ? tableRow.getSecond() == DictionaryLocale.DEFAULT : false)
                .map(Pair::getFirst)
                .collect(Collectors.toList()).toArray(enumMembers);

        Arrays.sort(enumMembers);
        Arrays.sort(tableRows);
        if (!Arrays.equals(enumMembers, tableRows)) {
            throw new ConsistencyException(String.format("Error in table %s mapped to enum %s: wrong rows %s",
                    TABLE_NAME, enumType.getClass().getName(), Arrays.toString(tableRows)));
        }
    }

    private T[] getEnumMembers() {
        Method valuesMethod = Arrays
                .stream(enumType.getClass().getMethods())
                .filter(method -> VALUES_METHOD_NAME.equals(method.getName()))
                .findFirst().orElse(null);

        try {
            return (T[]) valuesMethod.invoke(null);
        } catch (InvocationTargetException | IllegalAccessException e) {
            // never happens
        }
        return null;
    }

    private String constructSelect(DictionaryType dictionaryType) {
        StringBuilder result = new StringBuilder("SELECT ")
                .append(TERM_ID_COLUMN_NAME).append(", ")
                .append(LOCALE_ID_COLUMN_NAME).append(", ")
                .append(TEXT_COLUMN_NAME);
        return result
                .append(" FROM ")
                .append(TABLE_NAME)
                .append(" WHERE ")
                .append(TYPE_ID_COLUMN_NAME)
                .append(" = ")
                .append(dictionaryType.ordinal()).toString();
    }
}
