package ru.anisimov.keybattle.main.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.Ordered;
import org.springframework.format.FormatterRegistry;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import ru.anisimov.keybattle.core.config.CoreConfig;
import ru.anisimov.keybattle.core.config.ManagerConfig;
import ru.anisimov.keybattle.core.config.PersistenceConfig;
import ru.anisimov.keybattle.core.config.constants.Params;
import ru.anisimov.keybattle.core.data.formatters.EnumOrdinalFormatterFactory;
import ru.anisimov.keybattle.main.web.SimpleCookieLocaleResolver;

import javax.servlet.Filter;
import java.nio.charset.Charset;

import static ru.anisimov.keybattle.core.config.constants.Destinations.RESOURCES;
import static ru.anisimov.keybattle.core.config.constants.Destinations.SIGN_IN;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         10/9/14
 */
@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages = {"ru.anisimov.keybattle.tms.controller"})
@Import({PersistenceConfig.class, ManagerConfig.class, CoreConfig.class, PropertyPlaceholderConfig.class})
public class WebConfig extends WebMvcConfigurerAdapter {
	private static final int MAX_FILE_UPLOAD_SIZE = 1024 * 1024 * 5;
	
	private static final String DEFAULT_ENCODING = "UTF-8";

	private static final String[] MESSAGES_PLACES = {
			"classpath:messages/messages",
			"classpath:messages/validation",
			"classpath:messages/mail"
	};
	
	@Bean
	public HttpMessageConverter<String> responseBodyConverter() {
		return new StringHttpMessageConverter(Charset.forName(DEFAULT_ENCODING));
	}

	@Bean
	public Filter characterEncodingFilter() {
		CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
		characterEncodingFilter.setEncoding(DEFAULT_ENCODING);
		characterEncodingFilter.setForceEncoding(true);
		return characterEncodingFilter;
	}

	@Bean
	public MultipartResolver multipartResolver() {
		CommonsMultipartResolver result = new CommonsMultipartResolver();
		result.setMaxUploadSize(MAX_FILE_UPLOAD_SIZE);
		result.setMaxInMemorySize(MAX_FILE_UPLOAD_SIZE);
		return result;
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		LocaleChangeInterceptor localeChangeInterceptor = new LocaleChangeInterceptor();
		localeChangeInterceptor.setParamName(Params.LANG);
		registry.addInterceptor(localeChangeInterceptor);
	}

	@Bean
	public LocaleResolver localeResolver() {
		return new SimpleCookieLocaleResolver();
	}

	@Bean
	public MessageSource messageSource() {
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasenames(MESSAGES_PLACES);
		messageSource.setUseCodeAsDefaultMessage(true);
		messageSource.setDefaultEncoding(DEFAULT_ENCODING);
		messageSource.setCacheSeconds(0);
		return messageSource;
	}

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController(SIGN_IN).setViewName(SIGN_IN);
		registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler(RESOURCES + "**").addResourceLocations(RESOURCES);
	}

	@Override
	public void addFormatters(FormatterRegistry registry) {
		registry.addFormatterForFieldAnnotation(new EnumOrdinalFormatterFactory());
	}
	
	/*
	private static final String TEMPLATES_FOLDER = "/";
	private static final String TEMPLATES_EXTENSION = ".html";
	private static final String TEMPLATES_MODE = "HTML5";
	

	@Override
	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
		configurer.enable();
	}

	
	@Bean
	public TemplateResolver templateResolver() {
		ServletContextTemplateResolver resolver = new ServletContextTemplateResolver();
		resolver.setPrefix(TEMPLATES_FOLDER);
		resolver.setSuffix(TEMPLATES_EXTENSION);

		//NB, selecting HTML5 as the template mode.
		resolver.setCharacterEncoding(DEFAULT_ENCODING);
		resolver.setTemplateMode(TEMPLATES_MODE);
		resolver.setCacheable(false);
		return resolver;
	}

	@Bean
	public SpringTemplateEngine templateEngine(MessageSource messageSource, ServletContextTemplateResolver templateResolver) {
		SpringTemplateEngine engine = new SpringTemplateEngine();
		engine.setTemplateResolver(templateResolver);
		engine.setMessageSource(messageSource);
		engine.addDialect(new SpringSecurityDialect());
		return engine;
	}

	@Bean
	public ViewResolver viewResolver(SpringTemplateEngine templateEngine) {
		ThymeleafViewResolver viewResolver = new ThymeleafViewResolver();
		viewResolver.setTemplateEngine(templateEngine);
		viewResolver.setOrder(1);
		viewResolver.setViewNames(new String[]{"*"});
		viewResolver.setCache(false);
		viewResolver.setCharacterEncoding(DEFAULT_ENCODING);
		return viewResolver;
	}

	*/
}
