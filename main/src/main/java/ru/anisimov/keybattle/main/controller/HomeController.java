package ru.anisimov.keybattle.main.controller;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import static ru.anisimov.keybattle.core.config.constants.Destinations.*;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         10/7/14
 */
@Controller
public class HomeController {
	@RequestMapping(value = INDEX, method = RequestMethod.GET)
	public String getHomepage(Authentication authentication) {
		return authentication == null  
				? "redirect:" + SIGN_IN
				: "redirect:" + PROFILE;
	}
}
