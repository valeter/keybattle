package ru.anisimov.keybattle.main.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.anisimov.keybattle.core.data.formatters.EnumOrdinalFormat;
import ru.anisimov.keybattle.core.model.user.UserImageType;
import ru.anisimov.keybattle.core.model.user.UserInfo;
import ru.anisimov.keybattle.main.controller.interfaces.HasAjaxActions;
import ru.anisimov.keybattle.core.manager.UserManager;
import ru.anisimov.keybattle.core.data.service.interfaces.DictionaryService;
import ru.anisimov.keybattle.core.model.history.UserHistoryRecord;
import ru.anisimov.keybattle.core.model.web.WebActionParam;
import ru.anisimov.keybattle.core.model.web.WebActionParamValue;
import ru.anisimov.keybattle.core.model.web.WebActionResult;
import ru.anisimov.keybattle.core.model.web.WebActionStatus;
import ru.anisimov.keybattle.core.config.constants.Destinations;
import ru.anisimov.keybattle.core.config.constants.Params;

import java.util.Collections;
import java.util.List;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         11/2/14
 */
@RestController
public class UserController implements HasAjaxActions {
	private static final Logger log = LogManager.getLogger(UserController.class);
	
	@Autowired
	private DictionaryService dictionaryService;
	
    @Autowired
    private UserManager userManager;

    @RequestMapping(value = Destinations.USER_ME, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public UserInfo getCurrentUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return auth == null || auth.getName() == null ? null : userManager.getUserInfo(((User)auth.getPrincipal()).getId());
    }

    @RequestMapping(value = Destinations.USER_INFO, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public UserInfo getUserInfo(@RequestParam(Params.USER_ID) Long userId) {
        return userManager.getUserInfo(userId);
    }

    @RequestMapping(value = Destinations.USER_INFOS, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<UserInfo> getUserInfos(@RequestParam(value = Params.ROLE, required = false) Integer role) {
        try {
            List<UserInfo> infos = role == null
                    ? userManager.getUserInfos()
                    : userManager.getUserInfos(UserRoleType.values()[role]);
            return infos == null ? Collections.EMPTY_LIST : infos;
        } catch (ArrayIndexOutOfBoundsException e) {
            return null;
        }
    }

    @RequestMapping(value = Destinations.USER_AVATAR, method = RequestMethod.GET, produces = MediaType.IMAGE_PNG_VALUE)
    public byte[] getUserAvatar(
			@RequestParam(value = Params.USER_ID) Long userId,
			@EnumOrdinalFormat @RequestParam(value = Params.TYPE) UserImageType typeId) {
        return userManager.getUserAvatar(userId, typeId);
    }

    @RequestMapping(value = Destinations.USER_STATUS, method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE)
    public String getUserStatus(@RequestParam(value = Params.USER_ID) Long userId) {
        return userManager.getUserStatus(userId);
    }

    @RequestMapping(value = Destinations.USER_ROLES, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<UserRole> getUserRoles(
            @RequestParam(Params.USER_ID) Long userId,
            @RequestParam(value = Params.ENABLED, required = false) Boolean enabled) {
        return userManager.getUserRoles(userId, enabled);
    }

    @RequestMapping(value = Destinations.USER_HISTORY, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<UserHistoryRecord> getUserHistory(@RequestParam(Params.USER_ID) Long userId) {
        return userManager.getRecordsByUser(userId);
    }

    @RequestMapping(value = Destinations.USER_ADD_ROLE, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public WebActionResult addUserRole(
            @RequestParam(Params.USER_ID) Long userId,
            @RequestParam(Params.ROLE) Integer role,
            @RequestParam(value = Params.COMMENT, required = false) String comment) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Long actorId = auth == null || auth.getPrincipal() == null ? null : ((User)auth.getPrincipal()).getId();
        if (actorId == null) {
            return new WebActionResult(WebActionStatus.ERROR).addParam(WebActionParam.REASON, WebActionParamValue.NOT_AUTHENTICATED);
        } else {
            try {
                UserRoleType roleType = UserRoleType.values()[role];
                return userManager.addUserRole(userId, roleType, actorId, comment)
                        ? new WebActionResult(WebActionStatus.SUCCESS)
                        : new WebActionResult(WebActionStatus.ERROR).addParam(WebActionParam.REASON, WebActionParamValue.SERVER_ERROR);
            } catch (ArrayIndexOutOfBoundsException e) {
                return new WebActionResult(WebActionStatus.ERROR).addParam(WebActionParam.REASON, WebActionParamValue.UNKNOWN_ROLE);
            }
        }
    }

	@Override
	public DictionaryService getDictionaryService() {
		return dictionaryService;
	}
}
