package ru.anisimov.keybattle.main.config;

import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.JdbcTemplate;
import ru.anisimov.keybattle.core.HasDictionary;
import ru.anisimov.keybattle.core.model.history.HistoryAction;
import ru.anisimov.keybattle.core.model.locale.DictionaryLocale;
import ru.anisimov.keybattle.core.model.locale.DictionaryType;
import ru.anisimov.keybattle.core.model.user.UserRoleType;
import ru.anisimov.keybattle.core.security.SecurityKeyType;
import ru.anisimov.keybattle.main.util.DictionaryEnumAuditor;
import ru.anisimov.keybattle.main.util.EnumTypeAuditor;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         11/2/14
 */
@Configuration
@Lazy(false)
public class AuditConfig {
    private static final String ROOT_PACKAGE = "ru.anisimov.keybattle";

    private static final String TABLE_DICT_TYPE = "DICT_TYPE";
    private static final String TABLE_DICT_LOCALE = "DICT_LOCALE";
    private static final String TABLE_USER_ROLE = "USER_ROLE_TYPE";
    private static final String TABLE_HISTORY_ACTION = "HISTORY_ACTION";
	private static final String TABLE_SECURITY_KEY_TYPE = "SECURITY_KEY_TYPE";

    @Bean
    @Lazy(false)
    public EnumTypeAuditor<DictionaryType> dictionaryTypeAuditor(JdbcTemplate jdbcTemplate) {
        return new EnumTypeAuditor<>(jdbcTemplate, DictionaryType.values()[0], TABLE_DICT_TYPE);
    }

    @Bean
    @Lazy(false)
    public EnumTypeAuditor<DictionaryLocale> dictionaryLocaleAuditor(JdbcTemplate jdbcTemplate) {
        return new EnumTypeAuditor<>(jdbcTemplate, DictionaryLocale.values()[0], TABLE_DICT_LOCALE);
    }

    @Bean
    @Lazy(false)
    public EnumTypeAuditor<UserRoleType> userRoleAuditor(JdbcTemplate jdbcTemplate) {
        return new EnumTypeAuditor<>(jdbcTemplate, UserRoleType.values()[0], TABLE_USER_ROLE);
    }

    @Bean
    @Lazy(false)
    public EnumTypeAuditor<HistoryAction> historyActionAuditor(JdbcTemplate jdbcTemplate) {
        return new EnumTypeAuditor<>(jdbcTemplate, HistoryAction.values()[0], TABLE_HISTORY_ACTION);
    }

	@Bean
	@Lazy(false)
	public EnumTypeAuditor<SecurityKeyType> securityKeyTypeAuditor(JdbcTemplate jdbcTemplate) {
		return new EnumTypeAuditor<>(jdbcTemplate, SecurityKeyType.values()[0], TABLE_SECURITY_KEY_TYPE);
	}

    @Bean
    @Lazy(false)
    public List<DictionaryEnumAuditor> dictionaryAuditors(JdbcTemplate jdbcTemplate) {
        List<DictionaryEnumAuditor> result = new ArrayList<>();
        Reflections reflections = new Reflections(
                ClasspathHelper.forPackage(ROOT_PACKAGE), new SubTypesScanner());
        Set<Class<? extends HasDictionary>> enumsWithDictionaries =
                reflections.getSubTypesOf(HasDictionary.class).stream()
                        .filter(type -> type.isEnum())
                        .collect(Collectors.toSet());
        enumsWithDictionaries.stream()
                .map(type -> type.getEnumConstants()[0])
                .forEach(enumConstant -> result.add(new DictionaryEnumAuditor(jdbcTemplate, (Enum)enumConstant)));
        return result;
    }
}
