/* ---------------------------------------------------- */
/* error messages */

function showErrorByData(data) {
    var text = '';
    for (var param in data.params) {
        if (data.params.hasOwnProperty(param)) {
            var paramName = dicts.webActionParamDict[param];
            var paramValue = dicts.webActionParamValueDict[data.params[param]];
            if (paramValue == undefined) {
                paramValue = data.params[param]
            }

            text = text + paramName + ': ' + paramValue + '\n';
        }
    }

    swal({
        title: dicts.webActionStatusDict[data.status],
        text: text,
        confirmButtonText: dicts.webCaptionDict[0],
        confirmButtonColor: "rgb(77, 166, 77)",
        allowOutsideClick: true,
        allowEscapeKey: true,
        animation: false
    });
}

function showError(errorMsg) {
    swal({
        title: dicts.webActionStatusDict[1],
        text: errorMsg,
        confirmButtonText: dicts.webCaptionDict[0],
        confirmButtonColor: "rgb(77, 166, 77)",
        allowOutsideClick: true,
        allowEscapeKey: true,
        animation: false
    });
}


/* ---------------------------------------------------- */
/* image processing */

function loadText(wrapElement, url, className) {
    var div = document.createElement('div');
    div.className = className;
    
    $.ajax({
		type: 'GET',
		url: url,
		success: function(data) {
			if (data.status && data.status != 0) {
				showErrorByData(data);
			}
			div.appendChild(document.createTextNode());
			div.innerHTML = data;
		},
		error: function(xhr, str) {
			showError(loadTextError);
		}
	});
    
    wrapElement.appendChild(div);
}

function loadImage(wrapElement, url, alt) {
    var img = new Image();
    img.onload = function(event) {
        wrapElement.appendChild(img);
        onImageLoad(event);
    };
    img.alt = alt;
    img.src = url;
    img.parentNode = wrapElement;
}

function onImageLoad(evt) {
    var img = evt.currentTarget;

    // what's the size of this image and it's parent
    var w = $(img).width();
    var h = $(img).height();
    var tw = $(img).parent().width();
    var th = $(img).parent().height();

    // compute the new size and offsets
    var result = scaleImage(w, h, tw, th, false);

    // adjust the image coordinates and size
    img.width = result.width;
    img.height = result.height;
    $(img).css("margin-left", result.targetleft);
    $(img).css("margin-top", result.targettop);
}

function scaleImage(srcwidth, srcheight, targetwidth, targetheight, fLetterBox) {
    var result = { width: 0, height: 0, fScaleToTargetWidth: true };

    if ((srcwidth <= 0) || (srcheight <= 0) || (targetwidth <= 0) || (targetheight <= 0)) {
        return result;
    }

    // scale to the target width
    var scaleX1 = targetwidth;
    var scaleY1 = (srcheight * targetwidth) / srcwidth;

    // scale to the target height
    var scaleX2 = (srcwidth * targetheight) / srcheight;
    var scaleY2 = targetheight;

    // now figure out which one we should use
    var fScaleOnWidth = (scaleX2 > targetwidth);
    if (fScaleOnWidth) {
        fScaleOnWidth = fLetterBox;
    }
    else {
        fScaleOnWidth = !fLetterBox;
    }

    if (fScaleOnWidth) {
        result.width = Math.floor(scaleX1);
        result.height = Math.floor(scaleY1);
        result.fScaleToTargetWidth = true;
    }
    else {
        result.width = Math.floor(scaleX2);
        result.height = Math.floor(scaleY2);
        result.fScaleToTargetWidth = false;
    }
    result.targetleft = Math.floor((targetwidth - result.width) / 2);
    result.targettop = Math.floor((targetheight - result.height) / 2);

    return result;
}
