var stompClient = null;
var logLoaded = 0;

function showChatEvent(chatEvent) {
    var chat = document.getElementById('chat-window');

    var date = new Date(chatEvent.eventtime);
    Date.prototype.chatFormat = function() {
        var hh = this.getHours().toString();
        var mm = this.getMinutes().toString();
        var ss  = this.getSeconds().toString();
        return hh + ':' + (mm[1]?mm:"0"+mm[0]) + ':' + (ss[1]?ss:"0"+ss[0]);
    };
    var dateString = date.chatFormat();

    var messages = chatEvent.messages;

    for (var i = 0; i < messages.length; i++) {
        var message = messages[i];

        var p = document.createElement('p');
        p.className = 'message';

        var userName = message.userName;
        var text = message.text;

        var messageString = dateString + '  ';
        switch (chatEvent.type) {
            case 'USER_JOIN':
                messageString = messageString + text;
                break;
            case 'USER_LEAVE':
                messageString = messageString + text;
                break;
            default:
                messageString = messageString + userName + ': ' + text;
        }

        p.appendChild(document.createTextNode(messageString));
        chat.appendChild(p);
    }
    chat.scrollTop = chat.scrollHeight;
}

var Command = function(execute, value) {
    this.execute = execute;
    this.value = value;
};

var ShowChatEventCommand = function(value) {
    return new Command(showChatEvent, value);
};

var ShowChatEventExecutor = function() {
    var commands = [];

    return {
        execute: function (command) {
            commands.push(command);
            if (logLoaded > 0) {
                this.executeQueue();
            }
        },

        executeQueue: function () {
            for (var i = commands.length - 1; i >= 0; i--) {
                var command = commands[i];
                command.execute(command.value);
            }
            commands = [];
        }
    }
};

var chatEventExecutor = new ShowChatEventExecutor();

function connect() {
    var url = '/battle/room';
    var socket = new SockJS(url);
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function(frame) {
        var userName = document.getElementById("cur-user-name").textContent;

        // chat broadcast
        stompClient.subscribe('/battle/room/chat', function(chatEvent) {
            var message = JSON.parse(chatEvent.body);
            chatEventExecutor.execute(new ShowChatEventCommand(message));
        });

        // chat user channel
        stompClient.subscribe('/user/battle/room/chat', function(chatEvent) {
            var message = JSON.parse(chatEvent.body);
            if (message.type == 'CHAT_LIST') {
                for (var i =  0; i <  message.events.length; i++) {
                    chatEventExecutor.execute(new ShowChatEventCommand(message.events[i]));
                }
                logLoaded = 1;
                chatEventExecutor.executeQueue();
            } else {
                chatEventExecutor.execute(new ShowChatEventCommand(message));
            }
        });

        // user list
        stompClient.subscribe('/battle/room/users', function(userEvent) {
            var message = JSON.parse(userEvent.body);
            showUserEvent(message);
        });

        getCurrentUsers();
        getChatLog();
        showUserCard(userName);
    });
}

function getCurrentUsers() {
    stompClient.send("/app/battle/room/userList",{},{});
}

function getChatLog() {
    stompClient.send("/app/battle/room/messageList", {}, JSON.stringify(getUserFilter()));
}

function disconnect() {
    stompClient.disconnect();
}

function getUserFilter() {
    var userFilter = [];
    var userName = document.getElementById("cur-user-name").textContent;
    var selectedUserCards = document.getElementsByClassName("user-card-selected");
    var selfCard = document.getElementById("card-" + userName);
    if (selectedUserCards.length > 1) {
        for (var i = 0; i < selectedUserCards.length; i++) {
            var card = selectedUserCards.item(i);
            userFilter.push(card.id.substring(5));
        }
    }
    var result = {users: userFilter};
    return result;
}

function showUserEvent(userEvent) {
    var userNames = userEvent.userNames;
    switch(userEvent.type) {
        case 'USER_LEAVE':
            hideUserCard(userNames[0]);
            break;
        case 'USER_JOIN':
        case 'USER_LIST':
            for (var i = 0; i < userNames.length; i++) {
                showUserCard(userNames[i]);
            }
            break;
        default:
    }
}

function hideUserCard(userName) {
    var card = document.getElementById('card-' + userName);
    if (card != null) {
        document.getElementById('card-' + userName).remove();
    }
}

function showUserCard(userName) {
    var card = document.getElementById('card-' + userName);
    if (card != null) {
        return;
    }

    var chatUsers = document.getElementById('chat-users');
    var div = document.createElement('div');
    div.className = 'user-card';
    div.id = 'card-' + userName;

    var avatarWrapper = document.createElement('div');
    avatarWrapper.className = 'avatar-wrapper';

    var avatar = document.createElement('img');
    avatar.src = 'resources/icons/noname.png';
    avatar.className = 'avatar';
    avatarWrapper.appendChild(avatar);
    div.appendChild(avatarWrapper);

    var cardLogin = document.createElement('div');
    cardLogin.className = 'card-login';
    cardLogin.appendChild(document.createTextNode(userName));
    div.appendChild(cardLogin);

    var curUserName = document.getElementById("cur-user-name").textContent;
    if (curUserName == userName) {
        div.classList.add("user-card-selected");
    } else {
        div.addEventListener('click', function (event) {
            if (!this.classList.contains("user-card-selected")) {
                this.classList.add("user-card-selected");
            } else {
                this.classList.remove("user-card-selected");
            }
        });
    }

    chatUsers.appendChild(div);
}

function say() {
    var message = document.getElementById("user-message").value;
    if (message.length > 0) {
        stompClient.send("/app/battle/room/messages", {}, JSON.stringify({
            'userName': null,
            'text': message,
            'recipients': getUserFilter()
        }));
        document.getElementById("user-message").value = '';
    }
}

function disconnect() {
    disconnect();
}

function sayOnEnter(event) {
    if (event.keyCode == 13) {
        say();
    }
}
