function processUserEvent(userEvent) {
    var users = userEvent.objects;
    switch(userEvent.type) {
        case 'USER_LEAVE':
			for (var i = 0; i < users.length; i++) {
				hideUserCard(users[i].id);
            }
            break;
        case 'USER_JOIN':
        case 'USER_LIST':
            for (var i = 0; i < users.length; i++) {
                showUserCard(users[i].id, users[i].username);
            }
            break;
        default:
    }
}

function processSystemEvent(systemEvent) {
	var message = userEvent.objects[0];
	if (!message) {
		return;
	}
    switch(systemEvent.type) {
    	case 'ERROR':
    		showError(message);
    		window.location.replace(urls.indexPageUrl);
            break;
        case 'BATTLE_STARTED':
        	window.location.replace(urls.battlePageUrl + message);
        	break;
        default:
    }
}

var Command = function(execute, value) {
    this.execute = execute;
    this.value = value;
};

var Executor = function() {
    var commands = [];
    var executeImmediate = false;

    return {
        execute: function (command) {
            commands.push(command);
            if (executeImmediate) {
            	this.executeQueue();
            }
        },

        executeQueue: function () {
            for (var i = commands.length - 1; i >= 0; i--) {
                var command = commands[i];
                command.execute(command.value);
            }
            commands = [];
        }
    }
};

var stompClient = null;
var eventExecutor = new Executor();

function connect() {
    var url = urls.battleServerUrl + '/battle/queue';
    var socket = new SockJS(url);
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function(frame) {
        // users queue 
        stompClient.subscribe('/user/battle/queue/users', function(userEvent) {
            var message = JSON.parse(userEvent.body);
            eventExecutor.execute(new Command(processUserEvent, message));
        });

        // chat user channel
        stompClient.subscribe('/user/battle/queue/system', function(systemEvent) {
            var message = JSON.parse(systemEvent.body);
            eventExecutor.execute(new Command(processSystemEvent, message));
        });

        getCurrentUsers();
        getSystemState();
        
        eventExecutor.executeImmediate = true;
    });
}

function getCurrentUsers() {
    stompClient.send("/app/battle/queue/userList",{},battleId);
}

function getSystemState() {
    stompClient.send("/app/battle/queue/systemState",{},battleId);
}

function disconnect() {
    stompClient.disconnect();
}

function hideUserCard(userId) {
    var card = document.getElementById(getCardId(userId));
    if (card != null) {
        card.remove();
    }
}

function showUserCard(userId, userName) {
    var card = document.getElementById(getCardId(userId));
    if (card != null) {
        return;
    }

    var userCards = document.getElementById('user-cards');
    
    var div = document.createElement('div');
    div.className = 'user-card';
    div.id = getCardId(userId);

    var avatarWrapper = document.createElement('div');
    avatarWrapper.className = 'user-card-avatar-wrapper';
    loadImage(avatarWrapper, urls.getAvatarUrl + '?type=2&user-id=' + userId, 'avatar');
    div.appendChild(avatarWrapper);

	var cardName = document.createElement('div');
    cardName.className = 'user-card-username';
    cardName.appendChild(document.createTextNode(userName));
    div.appendChild(cardName);
    
	loadText(div, urls.getStatusUrl + '?user-id=' + userId, 'user-card-status')
	
    userCards.appendChild(div);
}

function getCardId(userId) {
	return 'user-card-' + userId;
}
