var opts = {
    lines: 17, // The number of lines to draw
    length: 18, // The length of each line
    width: 3, // The line thickness
    radius: 10, // The radius of the inner circle
    corners: 0, // Corner roundness (0..1)
    rotate: 0, // The rotation offset
    direction: 1, // 1: clockwise, -1: counterclockwise
    color: 'green', // #rgb or #rrggbb or array of colors
    speed: 1.1, // Rounds per second
    trail: 10, // Afterglow percentage
    shadow: false, // Whether to render a shadow
    hwaccel: false, // Whether to use hardware acceleration
    className: 'spinner' // The CSS class to assign to the spinner
};

var selectedCountry;
var selectedGender;

function loadUserInfo(userInfo) {
    if (userInfo.genderId != null) {
        $('#gender-' + userInfo.genderId).click();
    }

    if (userInfo.countryId != null) {
        $('#country-' + userInfo.countryId).click();
    }
}

function getSelectedValue(id) {
    return $("#" + id).find("dt a span.value").html();
}

function init() {
    var wrap = document.getElementById('avatar-wrap');
    var input = document.getElementById('avatar-input');

    loadImage(wrap, getAvatarUrl + '?type=0&user-id=' + userId
        + '&' + new Date().getTime(), 'avatar', onImageLoad);

    wrap.onclick = function () {
        input.value = null;
    };

    input.onchange = function () {
        var data = new FormData();
        data.append('avatar', $('#avatar-input')[0].files[0]);

        $('#avatar-wrap').empty();

        var spinner = new Spinner(opts).spin();
        spinner.el.removeAttribute("style")
        wrap.appendChild(spinner.el);

        $.ajax({
            type: 'POST',
            url: setAvatarUrl,
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.status != 0) {
                    showErrorByData(data, dicts);
                }
                $('#avatar-wrap').empty();
                loadImage(wrap, getAvatarUrl + '?type=0&user-id=' + userId
                    + '&' + new Date().getTime(), 'avatar', onImageLoad);
            },
            error: function(xhr, str) {
                showError(imageSizeError, dicts);
                $('#avatar-wrap').empty();
                loadImage(wrap, getAvatarUrl + '?type=0&user-id=' + userId  
                    + '&' + new Date().getTime(), 'avatar', onImageLoad);
            }
        });
    };
    
    $("*").keypress(function(event){
        if ($(document.getElementById('country-list')).is(":visible")) {
            var key = String.fromCharCode(event.which).toLowerCase();

            var countryList = document.getElementById('country-list');
            var countries = countryList.children;
            var countryToFocus;
            for (var i = 0; i <= countries.length; i++) {
                var countryLi = countries[i];
                var countryA = countryLi.children[0];
                var countryName = countryA.children[1];
                if (countryName.innerHTML.toLowerCase().charAt(0) == key) {
                    countryToFocus = countryLi
                    break;
                }
            }
            countryToFocus.scrollIntoView();
        }
    });

    var inputDate = $('#dateOfBirth');
    inputDate.pickmeup({
        date: new Date,
        flat: false,
        first_day: 1,
        prev: '&#9664;',
        next: '&#9654;',
        mode: 'single',
        view: 'years',
        calendars: 1,
        format: 'd-m-Y',
        position: 'right',
        trigger_event: 'click',
        class_name: '',
        hide_on_select	: true,
        locale: { // Object, that contains localized days of week names and months
            days: calendar.weekdays,
            daysShort: calendar.shortWeekdays,
            daysMin: calendar.shortWeekdays,
            months: calendar.months,
            monthsShort: calendar.shortMonths
        }
    });

    $("#country dt a").click(function() {
        $("#country dd ul").toggle();
    });

    $("#country dd ul li a").click(function() {
        var text = $(this).html();
        $("#country dt a span").html(text);
        $("#country dd ul").hide();
        selectedCountry = getSelectedValue("country");
        $("#countryId").val(selectedCountry);
    });

    $("#gender dt a").click(function() {
        $("#gender dd ul").toggle();
    });

    $("#gender dd ul li a").click(function() {
        var text = $(this).html();
        $("#gender dt a span").html(text);
        $("#gender dd ul").hide();
        selectedGender = getSelectedValue("gender");
        $("#genderId").val(selectedGender);
    });

    $(document).bind('click', function(e) {
        var $clicked = $(e.target);
        if (!$clicked.parents("#country").length) {
            $("#country dd ul").hide();
        }

        if (!$clicked.parents("#gender").length) {
            $("#gender dd ul").hide();
        }
    });
}
