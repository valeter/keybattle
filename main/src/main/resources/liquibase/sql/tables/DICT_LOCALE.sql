CREATE TABLE DICT_LOCALE (
	ID INT(2) NOT NULL,
	NAME VARCHAR(200) NOT NULL,
	DESCRIPTION VARCHAR(400),

	PRIMARY KEY (ID),

	KEY IDX_DL_NAME (NAME),
  CONSTRAINT UK_DL_NAME UNIQUE (NAME)
)
