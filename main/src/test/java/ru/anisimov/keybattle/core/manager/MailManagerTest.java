package ru.anisimov.keybattle.core.manager;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.anisimov.keybattle.core.model.mail.MessageBuilder;
import ru.anisimov.keybattle.core.model.mail.MessageTemplate;
import ru.anisimov.keybattle.main.Launcher;
import ru.anisimov.keybattle.core.model.mail.Sender;

import java.util.Locale;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Launcher.class)
public class MailManagerTest {
	@Autowired
	private MailManager mailManager;
	
	@Test
	public void testSendImmediately() throws Exception {
		mailManager.send(
				new MessageBuilder()
						.sender(Sender.NOREPLY)
						.locale(Locale.ENGLISH)
						.template(MessageTemplate.REGISTRATION_CONFIRM)
						.to("valetr@yandex.ru")
						.withParam("sk", "fuck")
						.build()
		);
	}
}
