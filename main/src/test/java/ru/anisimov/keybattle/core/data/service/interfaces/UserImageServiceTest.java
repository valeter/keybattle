package ru.anisimov.keybattle.core.data.service.interfaces;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.anisimov.keybattle.core.config.CoreConfig;
import ru.anisimov.keybattle.core.config.PersistenceConfig;
import ru.anisimov.keybattle.main.config.PropertyPlaceholderConfig;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         08.05.15
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {PersistenceConfig.class, CoreConfig.class, PropertyPlaceholderConfig.class})
public class UserImageServiceTest {
	@Autowired
	private UserService userService;
	@Autowired 
	private UserImageService userImageService;
	
	@Test
	@Transactional
	public void testAll() {
		/*User user = userService.getUserByUserName("valter");
		byte[] expectedThumbnail = new byte[] {1, 2, 0, 13};
		UserImage result = userThumbnailService.save(new UserImage(user.getId(), UserImageType.MEDIUM, expectedThumbnail));
		assertTrue(userThumbnailService.count() == 1);
		UserImage userImage = userThumbnailService.findByUserId(user.getId());
		assertArrayEquals(expectedThumbnail, userImage.getImage());*/
	}
}
