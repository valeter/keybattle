package ru.anisimov.keybattle.core.data.service.persitence;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.anisimov.keybattle.core.config.CoreConfig;
import ru.anisimov.keybattle.core.data.service.interfaces.UserInfoService;
import ru.anisimov.keybattle.core.config.PersistenceConfig;
import ru.anisimov.keybattle.core.model.user.Gender;
import ru.anisimov.keybattle.core.model.user.User;
import ru.anisimov.keybattle.core.model.user.UserInfo;
import ru.anisimov.keybattle.core.security.AuthoritiesPolicy;
import ru.anisimov.keybattle.core.data.service.interfaces.UserService;

import java.time.LocalDate;
import java.util.Arrays;

import static org.junit.Assert.*;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CoreConfig.class, PersistenceConfig.class})
@TransactionConfiguration(defaultRollback = true)
@Ignore
public class DbUserInfoServiceTest {
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private UserService userService;
	@Autowired
	private UserInfoService userInfoService;
	
	@Before
	public void setUp() throws Exception {
		jdbcTemplate.update("DELETE FROM USER_INFO");
	}

	@Test
	public void testAddInitialUserInfo() throws Exception {
		long userId = 532;
		assertTrue(userService.addUser(new User(userId, "valter", "no_pass", false, true, 
				Arrays.asList(AuthoritiesPolicy.getAuthority("USER")))));
		assertTrue(userInfoService.addInitialUserInfo(userId, "valter@mail.ru"));
		User user = userService.getUserByUserName("valter");
		UserInfo userInfo = userInfoService.getUserInfo(userId);
		assertEquals(user.getId(), userInfo.getUserId());
		assertNull(userInfo.getAge());
		assertNull(userInfo.getCountryId());
		assertNull(userInfo.getDateOfBirth());
		assertEquals(Gender.ALIEN, userInfo.getGender());
		assertNull(userInfo.getName());
		assertNull(userInfo.getStatus());
		assertEquals(true, userInfo.isHidden());
		assertEquals(userInfo.getEmail(), "valter@mail.ru");
	}

	@Test
	public void testShowUserInfo() throws Exception {
		long userId = 532;
		assertTrue(userService.addUser(new User(userId, "valter", "no_pass", false, true,
				Arrays.asList(AuthoritiesPolicy.getAuthority("USER")))));
		assertTrue(userInfoService.addInitialUserInfo(userId, "valter@mail.ru"));
		User user = userService.getUserByUserName("valter");
		UserInfo userInfo = userInfoService.getUserInfo(userId);
		assertEquals(user.getId(), userInfo.getUserId());
		assertNull(userInfo.getAge());
		assertNull(userInfo.getCountryId());
		assertNull(userInfo.getDateOfBirth());
		assertEquals(Gender.ALIEN, userInfo.getGender());
		assertNull(userInfo.getName());
		assertNull(userInfo.getStatus());
		assertEquals(true, userInfo.isHidden());
		
		assertTrue(userInfoService.showUserInfo(userId, true));
		userInfo = userInfoService.getUserInfo(userId);
		assertFalse(userInfo.isHidden());
	}

	@Test
	public void testGetUserInfo() throws Exception {
		long userId = 532;
		assertTrue(userService.addUser(new User(userId, "valter", "no_pass", false, true,
				Arrays.asList(AuthoritiesPolicy.getAuthority("USER")))));
		assertTrue(userInfoService.addInitialUserInfo(userId, "valter@mail.ru"));
		User user = userService.getUserByUserName("valter");
		
		byte[] avatar = new byte[]{0, 0 , 1, 0, 1, 0, 2, 5, 1, 4};
		UserInfo expectedInfo = new UserInfo();
		expectedInfo.setAge(3);
		expectedInfo.setCountryId(147);
		expectedInfo.setDateOfBirth(LocalDate.of(2010, 12, 24));
		expectedInfo.setGender(Gender.FEMALE);
		expectedInfo.setHidden(false);
		expectedInfo.setName("Vasya");
		expectedInfo.setStatus("come and c me");
		expectedInfo.setUserId(user.getId());
		
		assertTrue(userInfoService.changeUserInfo(userId, expectedInfo));
		UserInfo userInfo = userInfoService.getUserInfo(userId);
		assertEquals(expectedInfo, userInfo);
		userInfo = userInfoService.getUserInfo(userId);
		assertEquals(expectedInfo, userInfo);
	}

	@Test
	public void testGetUserInfoWithoutAvatar() throws Exception {
		long userId = 532;
		assertTrue(userService.addUser(new User(userId, "valter", "no_pass", false, true,
				Arrays.asList(AuthoritiesPolicy.getAuthority("USER")))));
		assertTrue(userInfoService.addInitialUserInfo(userId, "valter@mail.ru"));
		User user = userService.getUserByUserName("valter");

		byte[] avatar = new byte[]{0, 0 , 1, 0, 1, 0, 2, 5, 1, 4};
		UserInfo expectedInfo = new UserInfo();
		expectedInfo.setAge(3);
		expectedInfo.setCountryId(147);
		expectedInfo.setDateOfBirth(LocalDate.of(2010, 12, 24));
		expectedInfo.setGender(Gender.FEMALE);
		expectedInfo.setHidden(false);
		expectedInfo.setName("Vasya");
		expectedInfo.setStatus("come and c me");
		expectedInfo.setUserId(user.getId());

		assertTrue(userInfoService.changeUserInfo(userId, expectedInfo));
		UserInfo userInfo;
		userInfo = userInfoService.getUserInfo(userId);
		assertEquals(expectedInfo, userInfo);
	}

	@Test
	public void testGetUserAvatar() throws Exception {
		long userId = 532;
		assertTrue(userService.addUser(new User(532, "valter", "no_pass", false, true,
				Arrays.asList(AuthoritiesPolicy.getAuthority("USER")))));
		assertTrue(userInfoService.addInitialUserInfo(userId, "valter@mail.ru"));
		User user = userService.getUserByUserName("valter");

		byte[] avatar = new byte[]{0, 0 , 1, 0, 1, 0, 2, 5, 1, 4};
		UserInfo expectedInfo = new UserInfo();
		expectedInfo.setAge(3);
		expectedInfo.setCountryId(147);
		expectedInfo.setDateOfBirth(LocalDate.of(2010, 12, 24));
		expectedInfo.setGender(Gender.FEMALE);
		expectedInfo.setHidden(false);
		expectedInfo.setName("Vasya");
		expectedInfo.setStatus("come and c me");
		expectedInfo.setUserId(user.getId());

		assertTrue(userInfoService.changeUserInfo(userId, expectedInfo));
	}
}
