package ru.anisimov.keybattle.core.manager;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.anisimov.keybattle.core.config.CoreConfig;
import ru.anisimov.keybattle.core.config.PersistenceConfig;

import static org.junit.Assert.assertTrue;

@Transactional
@TransactionConfiguration(defaultRollback = true)
@ContextConfiguration(classes = {PersistenceConfig.class, CoreConfig.class})
@RunWith(SpringJUnit4ClassRunner.class)
public class UserManagerTest {
    @Autowired
    private UserManager userManager;

    @Test
    public void testInit() throws Exception {
        assertTrue(true);
    }
}
