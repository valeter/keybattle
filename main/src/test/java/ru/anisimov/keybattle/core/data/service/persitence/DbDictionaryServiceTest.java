package ru.anisimov.keybattle.core.data.service.persitence;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.anisimov.keybattle.core.config.CoreConfig;
import ru.anisimov.keybattle.core.model.locale.DictionaryLocale;
import ru.anisimov.keybattle.main.config.AuditConfig;
import ru.anisimov.keybattle.core.config.PersistenceConfig;
import ru.anisimov.keybattle.core.data.service.interfaces.DictionaryService;
import ru.anisimov.keybattle.core.model.locale.DictionaryType;

import java.util.Map;

@Transactional
@TransactionConfiguration(defaultRollback = true)
@ContextConfiguration(classes = {AuditConfig.class, PersistenceConfig.class, CoreConfig.class})
@RunWith(SpringJUnit4ClassRunner.class)
public class DbDictionaryServiceTest {
    @Autowired
    private DictionaryService dictionaryService;

    @Test
    public void testGetDictionary() throws Exception {
        Map<Long, String> userRoleDict = dictionaryService.getDictionary(DictionaryLocale.RU, DictionaryType.USER_ROLE_TYPE, null);
        Map<Long, String> historyActionDict = dictionaryService.getDictionary(DictionaryLocale.EN, DictionaryType.HISTORY_ACTION_TYPE, null);
        System.out.println(userRoleDict);
        System.out.println(historyActionDict);
        System.out.println(dictionaryService.getDictionary(DictionaryLocale.EN, DictionaryType.COUNTRY, null));
        System.out.println(dictionaryService.getDictionary(DictionaryLocale.RU, DictionaryType.GENDER, null));
    }
}
