package ru.anisimov.keybattle.core.data.service.interfaces;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.anisimov.keybattle.core.config.CoreConfig;
import ru.anisimov.keybattle.main.config.WebConfig;
import ru.anisimov.keybattle.main.config.WebSocketConfig;

@ContextConfiguration(classes = {CoreConfig.class, WebConfig.class, WebSocketConfig.class})
@RunWith(SpringJUnit4ClassRunner.class)
public class SecurityKeyServiceTest {
	@Autowired
	private SecurityKeyService securityKeyService;
	
	@Test
	public void testAll() throws Exception {
		securityKeyService.clear();
		
		/*assertTrue(securityKeyService.storeKey(1l, SecurityKeyType.REGISTRATION, "suck"));
		assertTrue(securityKeyService.checkKey(1l, SecurityKeyType.REGISTRATION, "suck"));
		
		assertTrue(securityKeyService.markDeleted(1l, SecurityKeyType.REGISTRATION));
		assertFalse(securityKeyService.checkKey(1l, SecurityKeyType.REGISTRATION, "suck"));*/
		
		securityKeyService.clear();
	}
}
