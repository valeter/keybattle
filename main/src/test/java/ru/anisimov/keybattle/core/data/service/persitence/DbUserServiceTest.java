package ru.anisimov.keybattle.core.data.service.persitence;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.anisimov.keybattle.core.config.CoreConfig;
import ru.anisimov.keybattle.core.config.PersistenceConfig;
import ru.anisimov.keybattle.core.data.service.interfaces.UserInfoService;
import ru.anisimov.keybattle.core.data.service.interfaces.UserService;
import ru.anisimov.keybattle.core.model.user.Registration;
import ru.anisimov.keybattle.core.model.user.User;
import ru.anisimov.keybattle.core.model.user.UserRoleType;
import ru.anisimov.keybattle.core.security.PasswordEncoding;

import static org.junit.Assert.*;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CoreConfig.class, PersistenceConfig.class})
@TransactionConfiguration(defaultRollback = true)
public class DbUserServiceTest {
	@Autowired
	UserService userService;
	@Autowired
	UserInfoService userInfoService;
	@Autowired
	JdbcTemplate jdbcTemplate;

	@Test
	public void testAddUser() throws Exception {
		jdbcTemplate.update("DELETE FROM USER_ROLE");
		jdbcTemplate.update("DELETE FROM USER_INFO");
		jdbcTemplate.update("DELETE FROM USER_HISTORY");
		jdbcTemplate.update("DELETE FROM USER");

		String userName = "vasya";
		String password = "vasya.vasyechkin";
		String email = "vasya.vasyechkin@mail.ru";

		assertNull(userService.getUserByUserName(userName));
		assertNull(userService.getUserByEmail(email));
	
		Long userId = userService.addUser(new Registration(userName, PasswordEncoding.encodePassword(password), null, null));
		assertNotNull(userId);
		assertTrue(userInfoService.addInitialUserInfo(userId, email));
		assertTrue(userService.addRole(userId, UserRoleType.USER));
		assertTrue(userService.addRole(userId, UserRoleType.ADMINISTRATOR));

		assertNotNull(userService.getUserByUserName(userName));
		assertNotNull(userService.getUserByEmail(email));

		User user = userService.getUserByUserName(userName);

		assertEquals(userName, user.getUsername());
		assertTrue(PasswordEncoding.passwordEncoder().matches(password, jdbcTemplate.queryForObject("SELECT PASSWORD FROM USER WHERE USERNAME = ?", String.class, userName)));
		assertTrue(user.getAuthorities().stream()
				.map(GrantedAuthority::getAuthority)
				.anyMatch(role -> "USER".equals(role)));
		assertTrue(user.getAuthorities().stream()
				.map(GrantedAuthority::getAuthority)
				.anyMatch(role -> "ADMINISTRATOR".equals(role)));
		assertTrue(user.isAccountNonLocked());
		assertTrue(user.isEnabled());
		assertTrue(user.isAccountNonExpired());
		assertTrue(user.isCredentialsNonExpired());

		jdbcTemplate.update("DELETE FROM USER_ROLE");
		user = userService.getUserByUserName(userName);
		assertEquals(userName, user.getUsername());
		assertTrue(PasswordEncoding.passwordEncoder().matches(password, jdbcTemplate.queryForObject("SELECT PASSWORD FROM USER WHERE USERNAME = ?", String.class, userName)));
		assertTrue(user.isAccountNonLocked());
		assertTrue(user.isEnabled());
		assertTrue(user.isAccountNonExpired());
		assertTrue(user.isCredentialsNonExpired());
	}

	@Test(expected = DuplicateKeyException.class)
	public void testAddUserDuplicate() throws Exception {
		jdbcTemplate.update("DELETE FROM USER_ROLE");
		jdbcTemplate.update("DELETE FROM USER_INFO");
		jdbcTemplate.update("DELETE FROM USER_HISTORY");
		jdbcTemplate.update("DELETE FROM USER");

		String userName = "vasya";
		String password = "vasya.vasyechkin";
		String email = "vasya.vasyechkin@mail.ru";

		assertNull(userService.getUserByUserName(userName));
		assertNull(userService.getUserByEmail(email));

		Long userId = userService.addUser(new Registration(userName, PasswordEncoding.encodePassword(password), null, null));
		assertNotNull(userId);
		assertTrue(userInfoService.addInitialUserInfo(userId, email));
		assertTrue(userService.addRole(userId, UserRoleType.USER));
		assertTrue(userService.addRole(userId, UserRoleType.ADMINISTRATOR));

		assertNotNull(userService.getUserByUserName(userName));
		assertNotNull(userService.getUserByEmail(email));

		User user = userService.getUserByUserName(userName);

		assertEquals(userName, user.getUsername());
		assertTrue(PasswordEncoding.passwordEncoder().matches(password, jdbcTemplate.queryForObject("SELECT PASSWORD FROM USER WHERE USERNAME = ?", String.class, userName)));
		assertTrue(user.getAuthorities().stream()
				.map(GrantedAuthority::getAuthority)
				.anyMatch(role -> "USER".equals(role)));
		assertTrue(user.getAuthorities().stream()
				.map(GrantedAuthority::getAuthority)
				.anyMatch(role -> "ADMINISTRATOR".equals(role)));
		assertTrue(user.isAccountNonLocked());
		assertTrue(user.isEnabled());
		assertTrue(user.isAccountNonExpired());
		assertTrue(user.isCredentialsNonExpired());

		assertNotNull(userService.addUser(new Registration(userName, PasswordEncoding.encodePassword(password), null, null)));

		assertNull(userService.addUser(new Registration(userName, PasswordEncoding.encodePassword("fuck"), null, null)));
	}

	@Test
	public void testRemoveRole() throws Exception {
		jdbcTemplate.update("DELETE FROM USER_ROLE");
		jdbcTemplate.update("DELETE FROM USER_INFO");
		jdbcTemplate.update("DELETE FROM USER_HISTORY");
		jdbcTemplate.update("DELETE FROM USER");

		String userName = "vasya";
		String password = "vasya.vasyechkin";
		String email = "vasya.vasyechkin@mail.ru";

		assertNull(userService.getUserByUserName(userName));
		assertNull(userService.getUserByEmail(email));

		Long userId = userService.addUser(new Registration(userName, PasswordEncoding.encodePassword(password), null, null));
		assertNotNull(userId);
		assertTrue(userInfoService.addInitialUserInfo(userId, email));
		assertTrue(userService.addRole(userId, UserRoleType.USER));
		assertTrue(userService.addRole(userId, UserRoleType.ADMINISTRATOR));

		assertNotNull(userService.getUserByUserName(userName));
		assertNotNull(userService.getUserByEmail(email));

		User user = userService.getUserByUserName(userName);

		assertEquals(userName, user.getUsername());
		assertTrue(user.getAuthorities().stream()
				.map(GrantedAuthority::getAuthority)
				.anyMatch(role -> "USER".equals(role)));
		assertTrue(user.getAuthorities().stream()
				.map(GrantedAuthority::getAuthority)
				.anyMatch(role -> "ADMINISTRATOR".equals(role)));
		assertTrue(user.isAccountNonLocked());
		assertTrue(user.isEnabled());
		assertTrue(user.isAccountNonExpired());
		assertTrue(user.isCredentialsNonExpired());

		assertTrue(userService.removeRole(userId, UserRoleType.ADMINISTRATOR));
		user = userService.getUserByUserName(userName);
		assertFalse(user.getAuthorities().stream()
				.map(GrantedAuthority::getAuthority)
				.anyMatch(role -> "ADMINISTRATOR".equals(role)));
		assertTrue(user.getAuthorities().stream()
				.map(GrantedAuthority::getAuthority)
				.anyMatch(role -> "USER".equals(role)));

		assertTrue(userService.removeRole(userId, UserRoleType.USER));
		user = userService.getUserByUserName(userName);
		assertFalse(user.getAuthorities().stream()
				.map(GrantedAuthority::getAuthority)
				.anyMatch(role -> "ADMINISTRATOR".equals(role)));
		assertFalse(user.getAuthorities().stream()
				.map(GrantedAuthority::getAuthority)
				.anyMatch(role -> "USER".equals(role)));
	}

	@Test
	public void testLockUser() throws Exception {
		jdbcTemplate.update("DELETE FROM USER_ROLE");
		jdbcTemplate.update("DELETE FROM USER_INFO");
		jdbcTemplate.update("DELETE FROM USER_HISTORY");
		jdbcTemplate.update("DELETE FROM USER");

		String userName = "vasya";
		String password = "vasya.vasyechkin";
		String email = "vasya.vasyechkin@mail.ru";

		assertNull(userService.getUserByUserName(userName));
		assertNull(userService.getUserByEmail(email));

		Long userId = userService.addUser(new Registration(userName, PasswordEncoding.encodePassword(password), null, null));
		assertNotNull(userId);
		assertTrue(userInfoService.addInitialUserInfo(userId, email));
		assertTrue(userService.addRole(userId, UserRoleType.USER));

		assertNotNull(userService.getUserByUserName(userName));
		assertNotNull(userService.getUserByEmail(email));

		User user = userService.getUserByUserName(userName);

		assertEquals(userName, user.getUsername());
		assertTrue(user.getAuthorities().stream()
				.map(GrantedAuthority::getAuthority)
				.anyMatch(role -> "USER".equals(role)));
		assertFalse(user.getAuthorities().stream()
				.map(GrantedAuthority::getAuthority)
				.anyMatch(role -> "ADMINISTRATOR".equals(role)));
		assertTrue(user.isAccountNonLocked());
		assertTrue(user.isEnabled());
		assertTrue(user.isAccountNonExpired());
		assertTrue(user.isCredentialsNonExpired());

		assertTrue(userService.lockUser(userId, true));
		user = userService.getUserByUserName(userName);
		assertFalse(user.isAccountNonLocked());
		assertTrue(userService.lockUser(userId, false));
		user = userService.getUserByUserName(userName);
		assertTrue(user.isAccountNonLocked());
	}

	@Test
	public void testEnableUser() throws Exception {
		jdbcTemplate.update("DELETE FROM USER_ROLE");
		jdbcTemplate.update("DELETE FROM USER_INFO");
		jdbcTemplate.update("DELETE FROM USER_HISTORY");
		jdbcTemplate.update("DELETE FROM USER");

		String userName = "vasya";
		String password = "vasya.vasyechkin";
		String email = "vasya.vasyechkin@mail.ru";

		assertNull(userService.getUserByUserName(userName));
		assertNull(userService.getUserByEmail(email));

		Long userId = userService.addUser(new Registration(userName, PasswordEncoding.encodePassword(password), null, null));
		assertNotNull(userId);
		assertTrue(userInfoService.addInitialUserInfo(userId, email));
		assertTrue(userService.addRole(userId, UserRoleType.USER));

		assertNotNull(userService.getUserByUserName(userName));
		assertNotNull(userService.getUserByEmail(email));

		User user = userService.getUserByUserName(userName);

		assertEquals(userName, user.getUsername());
		assertTrue(user.getAuthorities().stream()
				.map(GrantedAuthority::getAuthority)
				.anyMatch(role -> "USER".equals(role)));
		assertFalse(user.getAuthorities().stream()
				.map(GrantedAuthority::getAuthority)
				.anyMatch(role -> "ADMINISTRATOR".equals(role)));
		assertTrue(user.isAccountNonLocked());
		assertTrue(user.isEnabled());
		assertTrue(user.isAccountNonExpired());
		assertTrue(user.isCredentialsNonExpired());

		assertTrue(userService.enableUser(userId, false));
		user = userService.getUserByUserName(userName);
		assertFalse(user.isEnabled());
		assertTrue(userService.enableUser(userId, true));
		user = userService.getUserByUserName(userName);
		assertTrue(user.isEnabled());
	}

	@Test
	public void testGetUsers() throws Exception {
		jdbcTemplate.update("DELETE FROM USER_ROLE");
		jdbcTemplate.update("DELETE FROM USER_INFO");
		jdbcTemplate.update("DELETE FROM USER_HISTORY");
		jdbcTemplate.update("DELETE FROM USER");

		String userName = "vasya";
		String password = "vasya.vasyechkin";
		String email = "vasya.vasyechkin@mail.ru";

		assertNull(userService.getUserByUserName(userName));
		assertNull(userService.getUserByEmail(email));

		Long userId = userService.addUser(new Registration(userName, PasswordEncoding.encodePassword(password), null, null));
		assertNotNull(userId);
		assertTrue(userInfoService.addInitialUserInfo(userId, email));
		assertTrue(userService.addRole(userId, UserRoleType.USER));

		assertNotNull(userService.getUserByUserName(userName));
		assertNotNull(userService.getUserByEmail(email));

		String userName2 = "vasya2";
		String password2 = "vasya2.vasyechkin";
		String email2 = "vasya2.vasyechkin@mail.ru";

		assertNull(userService.getUserByUserName(userName2));
		assertNull(userService.getUserByEmail(email2));

		Long userId2 = userService.addUser(new Registration(userName, PasswordEncoding.encodePassword(password), null, null));
		assertNotNull(userId);
		assertTrue(userInfoService.addInitialUserInfo(userId2, email2));
		assertTrue(userService.addRole(userId2, UserRoleType.USER));

		assertNotNull(userService.getUserByUserName(userName2));
		assertNotNull(userService.getUserByEmail(email2));

		User user = userService.getUserByUserName(userName);

		assertEquals(userName, user.getUsername());
		assertTrue(user.getAuthorities().stream()
				.map(GrantedAuthority::getAuthority)
				.anyMatch(role -> "USER".equals(role)));
		assertFalse(user.getAuthorities().stream()
				.map(GrantedAuthority::getAuthority)
				.anyMatch(role -> "ADMINISTRATOR".equals(role)));
		assertTrue(user.isAccountNonLocked());
		assertTrue(user.isEnabled());
		assertTrue(user.isAccountNonExpired());
		assertTrue(user.isCredentialsNonExpired());

		user = userService.getUserByUserName(userName2);

		assertEquals(userName2, user.getUsername());
		assertTrue(user.getAuthorities().stream()
				.map(GrantedAuthority::getAuthority)
				.anyMatch(role -> "USER".equals(role)));
		assertFalse(user.getAuthorities().stream()
				.map(GrantedAuthority::getAuthority)
				.anyMatch(role -> "ADMINISTRATOR".equals(role)));
		assertTrue(user.isAccountNonLocked());
		assertTrue(user.isEnabled());
		assertTrue(user.isAccountNonExpired());
		assertTrue(user.isCredentialsNonExpired());
	}

	@Test
	public void testGetUsersByRole() throws Exception {
		jdbcTemplate.update("DELETE FROM USER_ROLE");
		jdbcTemplate.update("DELETE FROM USER_INFO");
		jdbcTemplate.update("DELETE FROM USER_HISTORY");
		jdbcTemplate.update("DELETE FROM USER");

		String userName = "vasya";
		String password = "vasya.vasyechkin";
		String email = "vasya.vasyechkin@mail.ru";

		assertNull(userService.getUserByUserName(userName));
		assertNull(userService.getUserByEmail(email));

		Long userId = userService.addUser(new Registration(userName, PasswordEncoding.encodePassword(password), null, null));
		assertNotNull(userId);
		assertTrue(userInfoService.addInitialUserInfo(userId, email));
		assertTrue(userService.addRole(userId, UserRoleType.USER));

		assertNotNull(userService.getUserByUserName(userName));
		assertNotNull(userService.getUserByEmail(email));

		String userName2 = "vasya2";
		String password2 = "vasya2.vasyechkin";
		String email2 = "vasya2.vasyechkin@mail.ru";

		assertNull(userService.getUserByUserName(userName2));
		assertNull(userService.getUserByEmail(email2));

		Long userId2 = userService.addUser(new Registration(userName, PasswordEncoding.encodePassword(password), null, null));
		assertNotNull(userId);
		assertTrue(userInfoService.addInitialUserInfo(userId2, email2));
		assertTrue(userService.addRole(userId2, UserRoleType.USER));
		assertTrue(userService.addRole(userId2, UserRoleType.ADMINISTRATOR));

		assertNotNull(userService.getUserByUserName(userName2));
		assertNotNull(userService.getUserByEmail(email2));


		String userName3 = "vasya3";
		String password3 = "vasya3.vasyechkin";
		String email3 = "vasya3.vasyechkin@mail.ru";

		assertNull(userService.getUserByUserName(userName3));
		assertNull(userService.getUserByEmail(email3));

		Long userId3 = userService.addUser(new Registration(userName, PasswordEncoding.encodePassword(password), null, null));
		assertNotNull(userId3);
		assertTrue(userInfoService.addInitialUserInfo(userId3, email3));
		assertTrue(userService.addRole(userId3, UserRoleType.USER));
		assertTrue(userService.addRole(userId3, UserRoleType.ADMINISTRATOR));

		assertNotNull(userService.getUserByUserName(userName3));
		assertNotNull(userService.getUserByEmail(email3));

		assertEquals(3, userService.getUsers(UserRoleType.USER).size());
		assertTrue(userService.getUsers(UserRoleType.USER).stream().allMatch(
						user -> user.getUsername().equals("vasya")
								|| user.getUsername().equals("vasya2")
								|| user.getUsername().equals("vasya3"))
		);
		assertTrue(userService.getUsers(UserRoleType.USER).stream().anyMatch(
						user -> user.getUsername().equals("vasya"))
		);
		assertTrue(userService.getUsers(UserRoleType.USER).stream().anyMatch(
						user -> user.getUsername().equals("vasya2"))
		);
		assertTrue(userService.getUsers(UserRoleType.USER).stream().anyMatch(
						user -> user.getUsername().equals("vasya3"))
		);

		assertEquals(2, userService.getUsers(UserRoleType.ADMINISTRATOR).size());
		assertTrue(userService.getUsers(UserRoleType.ADMINISTRATOR).stream().allMatch(
						user -> user.getUsername().equals("vasya2")
								|| user.getUsername().equals("vasya3"))
		);
		assertTrue(userService.getUsers(UserRoleType.ADMINISTRATOR).stream().anyMatch(
						user -> user.getUsername().equals("vasya3"))
		);
		assertTrue(userService.getUsers(UserRoleType.ADMINISTRATOR).stream().anyMatch(
						user -> user.getUsername().equals("vasya2"))
		);

		userService.removeRole(userId3, UserRoleType.ADMINISTRATOR);
		assertEquals(1, userService.getUsers(UserRoleType.ADMINISTRATOR).size());
		assertTrue(userService.getUsers(UserRoleType.ADMINISTRATOR).stream().anyMatch(
						user -> user.getUsername().equals("vasya2"))
		);
	}

	@Test
	public void testGetUserById() throws Exception {
		jdbcTemplate.update("DELETE FROM USER_ROLE");
		jdbcTemplate.update("DELETE FROM USER_INFO");
		jdbcTemplate.update("DELETE FROM USER_HISTORY");
		jdbcTemplate.update("DELETE FROM USER");

		String userName = "vasya";
		String password = "vasya.vasyechkin";
		String email = "vasya.vasyechkin@mail.ru";

		assertNull(userService.getUserByUserName(userName));
		assertNull(userService.getUserByEmail(email));

		Long userId = userService.addUser(new Registration(userName, PasswordEncoding.encodePassword(password), null, null));
		assertNotNull(userId);
		assertTrue(userInfoService.addInitialUserInfo(userId, email));
		assertTrue(userService.addRole(userId, UserRoleType.USER));
		assertTrue(userService.addRole(userId, UserRoleType.ADMINISTRATOR));

		assertNotNull(userService.getUserByUserName(userName));
		assertNotNull(userService.getUserByEmail(email));

		User user = userService.getUserByUserName(userName);

		assertEquals(user, userService.getUserById(user.getId()));
	}

	@Test
	public void testGetUserIdByUserName() throws Exception {
		jdbcTemplate.update("DELETE FROM USER_ROLE");
		jdbcTemplate.update("DELETE FROM USER_INFO");
		jdbcTemplate.update("DELETE FROM USER_HISTORY");
		jdbcTemplate.update("DELETE FROM USER");

		String userName = "vasya";
		String password = "vasya.vasyechkin";
		String email = "vasya.vasyechkin@mail.ru";

		assertNull(userService.getUserByUserName(userName));
		assertNull(userService.getUserByEmail(email));

		Long userId = userService.addUser(new Registration(userName, PasswordEncoding.encodePassword(password), null, null));
		assertNotNull(userId);
		assertTrue(userInfoService.addInitialUserInfo(userId, email));
		assertTrue(userService.addRole(userId, UserRoleType.USER));
		assertTrue(userService.addRole(userId, UserRoleType.ADMINISTRATOR));

		assertNotNull(userService.getUserByUserName(userName));
		assertNotNull(userService.getUserByEmail(email));

		User user = userService.getUserByUserName(userName);
	}

	@Test
	public void testGetUserByEmail() throws Exception {
		jdbcTemplate.update("DELETE FROM USER_ROLE");
		jdbcTemplate.update("DELETE FROM USER_INFO");
		jdbcTemplate.update("DELETE FROM USER_HISTORY");
		jdbcTemplate.update("DELETE FROM USER");

		String userName = "vasya";
		String password = "vasya.vasyechkin";
		String email = "vasya.vasyechkin@mail.ru";

		assertNull(userService.getUserByUserName(userName));
		assertNull(userService.getUserByEmail(email));

		Long userId = userService.addUser(new Registration(userName, PasswordEncoding.encodePassword(password), null, null));
		assertNotNull(userId);
		assertTrue(userInfoService.addInitialUserInfo(userId, email));
		assertTrue(userService.addRole(userId, UserRoleType.USER));
		assertTrue(userService.addRole(userId, UserRoleType.ADMINISTRATOR));

		assertNotNull(userService.getUserByUserName(userName));
		assertNotNull(userService.getUserByEmail(email));

		User user = userService.getUserByUserName(userName);

		assertEquals(user, userService.getUserByEmail(email));
	}
}
