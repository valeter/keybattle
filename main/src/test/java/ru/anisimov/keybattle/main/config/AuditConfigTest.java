package ru.anisimov.keybattle.main.config;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.anisimov.keybattle.core.config.PersistenceConfig;

import static org.junit.Assert.*;

@Transactional
@TransactionConfiguration(defaultRollback = true)
@ContextConfiguration(classes = {AuditConfig.class, PersistenceConfig.class})
@RunWith(SpringJUnit4ClassRunner.class)
public class AuditConfigTest {
    @Test
    public void test() throws Exception {
        assertTrue(true);
    }
}
